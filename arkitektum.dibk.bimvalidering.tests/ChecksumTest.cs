﻿using System.IO;
using FluentAssertions;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class ChecksumTest
    {
        [Fact]
        public void Sha256Method()
        {
            var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
            string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
            string sha256HashStream;
            string sha256HashBytes;
            var bytes = File.ReadAllBytes(ifcDataFile);
            using (var stream = File.Open(ifcDataFile, FileMode.Open))
            {
                sha256HashBytes = GetChecksum.Sha256HashFile(bytes);
                sha256HashStream = GetChecksum.Sha256HashFile(stream);
            }
            sha256HashBytes.Should().BeEquivalentTo(sha256HashStream);
        }

        [Fact]
        public void Md5Method()
        {
            var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
            string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
            string md5HasStream;
            string md5HashBytes;
            var bytes = File.ReadAllBytes(ifcDataFile);
            using (var stream = File.Open(ifcDataFile, FileMode.Open))
            {
                md5HasStream = GetChecksum.Md5HashFile(bytes);
                md5HashBytes = GetChecksum.Md5HashFile(stream);
            }
           md5HashBytes.Should().BeEquivalentTo(md5HasStream);
        }

    }

}
