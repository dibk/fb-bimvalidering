﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using arkitektum.dibk.bimvalidering.Models;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine;
using FluentAssertions;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class DbServicesTest
    {

        [Fact(Skip = "Internal use")]
        public void ValidatingModelWithMvdXml()
        {
            Guid validationId = new Guid("1C758B7B-C11E-4AB3-AEB4-ED9E2FC3C58C");
            eByggesaksBIMReportResultsDB reportResults;
            using (var context = new EByggesaksBimDBContext())
            {

                reportResults = context.EByggesaksBimReportResults.Find(validationId);
            }

            reportResults.Should().NotBeNull();
        }
        [Fact(Skip = "Internal use")]
        public void GetUserByMailTest()
        {
            var user = DbServices.GetUserFromEmail("m@test.no");

            user.Should().NotBeNull();
        }
    }
}
