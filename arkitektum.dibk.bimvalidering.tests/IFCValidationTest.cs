using System.Collections.Generic;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.bimvalidering.tests.Logger;
using FluentAssertions;
using Xbim.Ifc;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class IFCValidationTest
    {

        [Fact(Skip = "Integration Test")]
        public void ValidatingModelWithMvdXml()
        {
            List<ReportResult> results = null;
            using (var model = IfcStore.Open(@"TestData\SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc"))
            {
                const string mvdName = @"TestData\ramme_etttrinn_igangsetting.mvdxml";
                var mvd = mvdXML.LoadFromFile(mvdName);
                var logger = UnitTestLoggerFactory.GetLogger();
                var doc = new MvdEngine(mvd, model);
                results = new IFCValidation(logger).MvdValidationToReportResults(model, doc);
            }
            results.Should().NotBeNull();
        }

        //[Fact]
        //public void OpenIfcModel()
        //{
        //    var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
        //    string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //    var model = new IFCValidation().OpenModel(ifcDataFile);
        //    model.Should().NotBeNull();
        //}

        //[Fact]
        //public void ValidatingModelWithMvdXml()
        //{
        //    var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
        //    string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //    string fullMvdxmlFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");
        //    var model = new IFCValidation().OpenModel(ifcDataFile);
        //    var result = new MvdxmlServices().MvdValidation(model, fullMvdxmlFilePathName);
        //    result.Should().NotBeNull();
        //}
        //[Fact]
        //public void MvdValidationToReportResultsTest()
        //{
        //    var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
        //    string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //    string fullMvdxmlFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");
        //    var ifcModel = new IFCValidation().OpenModel(ifcDataFile);
        //    var validatedModel = new MvdxmlServices().MvdValidation(ifcModel, fullMvdxmlFilePathName);
        //    var result = new IFCValidation().MvdValidationToReportResults(ifcModel, validatedModel);
        //    var resultsPassList = result.Where(x => x.ResultSummary == "Godkjent").ToList();

        //    var resultTypes = new List<ResultType>();

        //    foreach (var reportResult in result)
        //    {
        //        if (reportResult.ResultSummary == "Godkjent" || reportResult.ResultSummary == "Advarsel")
        //        {
        //            var entityName = reportResult.Entity.ExpressType.Name;

        //            if (resultTypes.Any(r => r.RuleTypeName == reportResult.ConceptName && r.IfcEntityName == entityName))
        //            {
        //                var report = resultTypes.First(r => r.RuleTypeName == reportResult.ConceptName && r.IfcEntityName == entityName);
        //                //report.FirstOrDefault().RuleTypeCount = report.FirstOrDefault().RuleTypeCount + 1;
        //                report.RuleTypeCount = report.RuleTypeCount + 1;
        //            }
        //            else
        //            {
        //                resultTypes.Add(new ResultType()
        //                {
        //                    RuleTypeName = reportResult.ConceptName,
        //                    IfcEntityName = entityName,
        //                    RuleTypeCount = 1,
        //                    RuleTypeResult = reportResult.ResultSummary
        //                });
        //            }
        //        }
        //    }

        //    var warlingsDictionary = new Dictionary<string, int>();
        //    var warnins = resultsPassList.GroupBy(l => new { l.ConceptName, l.Entity.ExpressType.Name })
        //        .Select(g => new
        //        {
        //            concept = g.Key.Name + ":" + g.Key.ConceptName,
        //            Count = g.Count()
        //        }).ToDictionary(dict => dict.concept, dict => dict.Count);
        //    resultsPassList.Count.Should().Be(48);
        //}
        //[Fact(Skip = "model can be open!")]
        //public void ErrorOpeningIfcModel()
        //{
        //    try
        //    {
        //        string fullIfcFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\Referansebygg Sintef-kassa alt 1 TEK10 ARK -WrongShema.ifc");
        //        var model = new IFCValidation().OpenModel(fullIfcFilePathName);
        //        model.Should().BeNull();
        //    }
        //    catch (Exception e)
        //    {
        //        e.Message.Should().BeEquivalentTo("kan ikke åpne modellen");
        //    }
        //}
        //[Fact]
        //public void ErrorValidatingModelWithMvdXml()
        //{
        //    try
        //    {
        //        var file = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
        //        string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //        string fullMvdxmlFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\DiBK-Template - ARK 1.4.mvdxml");
        //        var model = new IFCValidation().OpenModel(ifcDataFile);
        //        var result = new MvdxmlServices().MvdValidation(model, fullMvdxmlFilePathName);
        //        result.Should().NotBeNull();
        //    }
        //    catch (Exception e)
        //    {
        //        e.Message.Should().BeEquivalentTo("Kan ikke validere modell mot mvdxml-fil");
        //    }
        //}

        //[Fact]
        //public void REGEX_name()
        //{
        //    List<string> entitiesPassList = new List<string>();
        //    Dictionary<string, string> entitiesName = new Dictionary<string, string>
        //    {
        //        {"IfcBuildingStorey", "01"},
        //        {"IfcChimney", "272 Monteringsferdige ildsteder"},
        //        {"IfcColumn", "222-Søyler"},
        //        {"IfcCovering", "218.Utstyr og komplettering"},
        //        {"IfcDoor", "234 Vinduer, dører, porter"},
        //        {"IfcRailing", "284 Balkonger og verandaer"},
        //        {"IfcRamp", "283 Ramper"},
        //        {"IfcRoof", "264 Takoppbygg"},
        //        {"IfcSlab", "252 Gulv på grunn"},
        //        {"IfcSpace", "290 Andre bygningsmessige deler"},
        //        {"IfcStair", "282 Utvendige trapper"},
        //        {"IfcWall", "244 Vinduer dører foldevegger"},
        //        {"IfcCurtainWall", "243 Systemvegger glassfelt"},
        //        {"IfcWindow", "263 Glasstak overlys takluker"},
        //        {"IfcTransportElement", "621 Heiser"}
        //    };

        //    foreach (var entity in entitiesName)
        //    {
        //        string rgxString = new RegexServices().GetRegex("Name", entity.Key);
        //        Regex rgx = new Regex(rgxString);
        //        if (rgx.IsMatch(entity.Value))
        //        {
        //            entitiesPassList.Add(entity.Key);
        //        }
        //    }
        //    entitiesPassList.Count().Should().Be(15);
        //}


    }
}
