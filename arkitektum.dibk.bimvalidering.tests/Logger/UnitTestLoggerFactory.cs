﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace arkitektum.dibk.bimvalidering.tests.Logger
{
    public class UnitTestLoggerFactory
    {
        public static ILogger GetLogger()
        {
            var logger = new LoggerConfiguration()
                .WriteTo.Console()
              .CreateLogger();
            return logger;
        }
    }
}
