﻿using System.IO;
using arkitektum.dibk.validationengine.Matrikkel;
using FluentAssertions;
//using no.kxml.skjema.dibk.matrikkelregistrering;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class MatrikkelregistreringTest
    {

        //    private readonly MatrikkelregistreringType _matrikkelregistrering;

        //    public MatrikkelregistreringTest()
        //    {
        //        const string godjkentFil = "SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc";
        //        var godjkentFil1 = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", godjkentFil);
        //        _matrikkelregistrering = new MatrikkelopplysningerService().GetMatrikkelregistreringType(godjkentFil1);
        //    }

        //    [Fact]
        //    public void Bruksnummer_OK_Test()
        //    {
        //     _matrikkelregistrering.eiendomsidentifikasjon[0].bruksnummer.Should().Be("2430");
        //    }

        //    [Fact]
        //    public void Kommunenummer_OK_Test()
        //    {
        //       _matrikkelregistrering.eiendomsidentifikasjon[0].kommunenummer.Should().Be("0904");
        //    }

        //    [Fact]
        //    public void Gaardsnummer_OK_Test()
        //    {
        //    _matrikkelregistrering.eiendomsidentifikasjon[0].gaardsnummer.Should().BeEquivalentTo("200");
        //    }

        //    [Fact]
        //    public void Festenummer_OK_Test()
        //    {
        //        _matrikkelregistrering.eiendomsidentifikasjon[0].festenummer.Should().BeEquivalentTo("1");
        //    }

        //    [Fact]
        //    public void Seksjonsnummer_OK_Test()
        //    {
        //        _matrikkelregistrering.eiendomsidentifikasjon[0].seksjonsnummer.Should().BeEquivalentTo("0");
        //    }

        //    [Fact]
        //    public void SiteAdressenummer_OK_Test()
        //    {
        //        _matrikkelregistrering.adresse[0].adressenummer.Should().BeEquivalentTo("5");
        //    }

        //    [Fact]
        //    public void SiteAdressenavn_OK_Test()
        //    {
        //        _matrikkelregistrering.adresse[0].adressenavn.Should().BeEquivalentTo("kyrkjevegen");
        //    }

        //    [Fact]
        //    public void SiteAdressebokstav_OK_Test()
        //    {
        //        _matrikkelregistrering.adresse[0].adressebokstav.Should().BeEquivalentTo("B");
        //    }

        //    [Fact]
        //    public void Bygningsnummer_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bygningsnummer.Should().BeEquivalentTo("987456321");
        //    }

        //    [Fact]
        //    public void Bygningstype_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bygningstype[0].kodeverdi.Should().BeEquivalentTo("161");
        //    }

        //    [Fact]
        //    public void BygningBebygdAreal_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bebygdAreal.Should().BeEquivalentTo("86.1");
        //    }
        //    [Fact]
        //    public void etasjenummer_OK_Test()
        //    {

        //        var etasje1 = _matrikkelregistrering.bygning[0].etasjer[0].etasjenummer;
        //        var etasje2 = _matrikkelregistrering.bygning[0].etasjer[1].etasjenummer;

        //        etasje1.Should().BeEquivalentTo(etasje2);
        //        _matrikkelregistrering.bygning[0].etasjer[0].etasjenummer.Should().BeEquivalentTo("01");
        //    }
        //    [Fact]
        //    public void etasjeplan_OK_Test()
        //    {
        //        var etasje1 = _matrikkelregistrering.bygning[0].etasjer[0].etasjeplan.kodeverdi;
        //        var etasje2 = _matrikkelregistrering.bygning[0].etasjer[1].etasjeplan.kodeverdi;

        //        etasje1.Should().BeEquivalentTo("H");
        //        etasje2.Should().BeEquivalentTo("L");
        //    }
        //    [Fact]
        //    public void etasjerBruksarealTotalt_OK_Test()
        //    {
        //        var etasje1 = _matrikkelregistrering.bygning[0].etasjer[0].bruksarealTotalt;
        //        var etasje2 = _matrikkelregistrering.bygning[0].etasjer[1].bruksarealTotalt;

        //        etasje1.Should().Be(76);
        //        etasje2.Should().Be(76.4);
        //    }
        //    [Fact]
        //    public void etasjerBruttoarealTotalt_OK_Test()
        //    {
        //        var etasje1 = _matrikkelregistrering.bygning[0].etasjer[0].bruttoarealTotalt;
        //        var etasje2 = _matrikkelregistrering.bygning[0].etasjer[1].bruttoarealTotalt;
        //        etasje1.Should().Be(86.1);
        //        etasje2.Should().Be(80.1);
        //    }
        //    [Fact]
        //    public void bruksenhetsEtasjeplan_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bruksenheter[0].bruksenhetsnummer.etasjeplan.kodeverdi.Should().BeEquivalentTo("H");
        //    }
        //    [Fact]
        //    public void bruksenhetsEtasjenummer_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bruksenheter[0].bruksenhetsnummer.etasjenummer.Should().BeEquivalentTo("01");
        //    }
        //    [Fact]
        //    public void bruksenhetsLoepenummer_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bruksenheter[0].bruksenhetsnummer.loepenummer.Should().BeEquivalentTo("01");
        //    }
        //    [Fact]
        //    public void bruksenhetsBruksareal_OK_Test()
        //    {
        //        _matrikkelregistrering.bygning[0].bruksenheter[0].bruksareal.Should().Be(77);
        //    }
    }
}