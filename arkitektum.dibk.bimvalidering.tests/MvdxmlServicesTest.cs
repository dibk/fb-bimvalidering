﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using arkitektum.dibk.bimvalidering.tests.Services.Builer;
using FluentAssertions;
using Xbim.MvdXml;
using Xunit;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine.MvdXml;


namespace arkitektum.dibk.bimvalidering.tests
{
    public class MvdxmlServicesTest
    {

        [Fact]
        public void GetConceptNameByName()
        {
            var concept = new MvdXmlV1Builder().Concept("212.09 Predefinert heis type : 212 TransportElement");
            var conceptName = MvdxmlServices.GetConceptName(concept);
            conceptName.Should().BeEquivalentTo("Predefinert heis type");
        }

        [Fact]
        public void GetConceptNameByCode()
        {
            var concept = new MvdXmlV1Builder().Concept("Predefinert heis type", "212.09");
            var conceptName = MvdxmlServices.GetConceptName(concept);
            conceptName.Should().BeEquivalentTo("Predefinert heis type");
        }

        [Fact]
        public void GetDefinition()
        {
            var comncetDefinition = new Dictionary<string, string>()
            {
                {"definition1","something"},
                {"lang1","Eng"},
                {"definition2","Noko"},
                {"lang2","No"},
            };
            var concept = new MvdXmlV1Builder().Concept("Predefinert heis type", "212.09", comncetDefinition);
            var definition = MvdxmlServices.GetDefinition(concept.Definitions);
            definition[0].Body.Value.Should().BeEquivalentTo("Noko");
        }
        [Fact]
        public void GetDefinitionEng()
        {
            var comncetDefinition = new Dictionary<string, string>()
            {
                {"definition1","something"},
                {"lang1","Eng"},
                {"definition2","Noko"},
            };
            var concept = new MvdXmlV1Builder().Concept("Predefinert heis type", "212.09", comncetDefinition);
            var definition = MvdxmlServices.GetDefinition(concept.Definitions);
            definition[0].Body.Value.Should().BeEquivalentTo("something");
        }

        //TODO  gets all the requirements of a View requirement
        [Fact(Skip = "Is not ready yet")]
        public void GetRequirementformView()
        {
            string mvdxmlFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");
            var mvdxml = mvdXML.LoadFromFile(mvdxmlFile);
            var view = mvdxml.Views.Single(v => v.name.Contains("P13"));
            if (view != null)
            {
                var exporExchangeRequirement = view.ExchangeRequirements.Single(er => er.applicability.ToString() == "export");
                if (exporExchangeRequirement != null)
                {

                    //var allConceptRoots = mvdxml.GetAllConceptsRoots().ToArray();
                    //var conceptt =
                    //    allConceptRoots.SelectMany(
                    //        cr => cr.Concepts.SelectMany(
                    //            c => c.Requirements.Where(
                    //                r => r.exchangeRequirement == exporExchangeRequirement.uuid)));
                    //foreach (var requirementsRequirement in conceptt)
                    //{
                    //    var noko = requirementsRequirement.ParentConcept;

                    //}
                }
            }
        }

        private static RequirementsRequirementRequirement GetRequirement(string requieremnt)
        {
            switch (requieremnt)
            {
                case "MAN":
                    return RequirementsRequirementRequirement.mandatory;
                case "OPT":
                    return RequirementsRequirementRequirement.recommended;
                default:
                    return RequirementsRequirementRequirement.recommended;
            }
        }

        // Serializaer to create the new mvdxml file 
        private string Serialize(object form)
        {
            //TODO skeep <Definition> <Body >...</Body> </Definition>
            var serializer = new XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);
            return stringWriter.ToString();
        }
        private sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }
    }

}
