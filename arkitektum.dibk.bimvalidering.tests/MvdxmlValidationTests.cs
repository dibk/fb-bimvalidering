using System;
using System.IO;
using System.Linq;
using System.Reflection;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.MvdXml;
using FluentAssertions;
using Xbim.Common;
using Xbim.Ifc;
using Xbim.Ifc4.Interfaces;
using Xbim.Ifc4.Kernel;
using Xbim.Ifc4.ProductExtension;
using Xbim.Ifc4.SharedBldgElements;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;
using Xunit;
using Xunit.Abstractions;
using static System.String;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class MvdxmlValidationTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly Xbim.MvdXml.mvdXML _mvdXml;
        private readonly Xbim.MvdXml.ModelView _mvdXmlModelView;

        private readonly IfcStore _ifcModel;
        //private readonly Dictionary<string, string> _mvdxmlExcRequirement;


        public MvdxmlValidationTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _ifcModel = IfcStore.Open(@"TestData\SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc");

            var mvdxmlFile = Path.Combine(@"TestData\ramme_etttrinn_igangsetting.mvdxml");
            _mvdXml = mvdXML.LoadFromFile(mvdxmlFile);
            _mvdXmlModelView = _mvdXml.Views[0];
        }
        //TODO check that rules give back a result, can be that the rule is not working....!

        [Fact(DisplayName = "IfcProject: TrueNorth - OK test")]
        public void IfcProjectTrueNorthOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcProject>().FirstOrDefault();

                var conceptRule = GetConcept("IfcProject", "konstruksjonsretning");
                if (conceptRule != null)
                {
                    result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }
        [Fact(DisplayName = "IfcProject: TrueNorth - Warning test")]
        public void IfcProjectTrueNorthErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcProject>().FirstOrDefault();

                var conceptRule = GetConcept("IfcProject", "konstruksjonsretning");
                if (conceptRule != null)
                {
                    result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }
        [Fact(DisplayName = "IfcProject: CRSName - OK test")]
        public void IfcProjectCrsNameOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcProject>().FirstOrDefault();

                var conceptRule = GetConcept("IfcProject", "konstruksjonsretning");
                if (conceptRule != null)
                {
                    result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }
        [Fact(DisplayName = "IfcProject: CRSName - Warning test")]
        public void IfcProjectCrsNameErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcProject>().FirstOrDefault();

                var conceptRule = GetConcept("IfcProject", "Koordinatsystem");
                if (conceptRule != null)
                {
                    result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }

        [Fact(DisplayName = "IfcSite: AddressLines - OK test")]
        public void IfcSiteAddressOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "Postadresse");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }

        [Fact(DisplayName = "IfcSite: AddressLines - Warning test")]
        public void IfcSiteAddressErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "Postadresse");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }
        [Fact(DisplayName = "IfcSite: RefLatitude - OK test")]
        public void IfcSiteRefLatitudeOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "breddegrad");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }

        [Fact(DisplayName = "IfcSite: RefLatitude - Warning test")]
        public void IfcSiteRefLatitudeErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "breddegrad");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }
        [Fact(DisplayName = "IfcSite: RefLongitude - OK test")]
        public void IfcSiteRefLongitudeOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "lengdegrad");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }

        [Fact(DisplayName = "IfcSite: RefLongitude - Warning test")]
        public void IfcSiteRefLongitudeErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "lengdegrad");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }
        [Fact(DisplayName = "IfcSite: RefElevation - OK test")]
        public void IfcSiteRefElevationeOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "høyde");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }

        [Fact(DisplayName = "IfcSite: RefElevation - Warning test")]
        public void IfcSiteRefElevationErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "høyde");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Warning);
        }
        [Fact(DisplayName = "IfcSite: LandTitleNumber - OK test")]
        public void IfcSitLandTitleNumberOkTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var siteAdress = GetConcept("IfcSite", "Matrikkelnummer");
                if (siteAdress != null)
                {
                    result = siteAdress.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Pass);
        }

        [Fact(DisplayName = "IfcSite: LandTitleNumber - Feil test")]
        public void IfcSiteLandTitleNumberErrorTest()
        {
            var result = ConceptTestResult.DoesNotApply;
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSite>().FirstOrDefault();

                var conceptRule = GetConcept("IfcSite", "Matrikkelnummer");
                var concept = conceptRule.AppliesTo(ifcEntity);
                if (conceptRule != null)
                {
                    result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                }
            }
            result.Should().Be(ConceptTestResult.Fail);
        }
        [Fact(DisplayName = "IfcBuilding: GrossPlannedArea - OK test")]
        public void IfcBuildingBYAOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "BYA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuilding: GrossPlannedArea - Feil test")]
        public void IfcBuildingBYAErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "BYA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Fail);

            }
        }    
        [Fact(DisplayName = "IfcBuilding: OccupancyType - OK test")]
        public void IfcBuildingBygningstypeOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Bygningstype");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuilding: OccupancyType - Feil test")]
        public void IfcBuildingBygningstypeErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Bygningstype");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Fail);

            }
        }
        [Fact(DisplayName = "IfcBuilding: BuildingID - OK test")]
        public void IfcBuildingBygningsnummerOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Bygningsnummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuilding: BuildingID - Warning test")]
        public void IfcBuildingBygningsnummereErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Bygningsnummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);

            }
        }
        [Fact(DisplayName = "IfcBuilding: Height - OK test")]
        public void IfcBuildingMønehøydeOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Mønehøyde");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuilding: Height - Warning test")]
        public void IfcBuildingMønehøydeeErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuilding>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuilding", "Mønehøyde");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);

            }
        }  
        
        [Fact(DisplayName = "IfcBuildingStorey: LongName - OK test")]
        public void IfcBuildingStoreyBruksenhetsnummeretOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "etasjenummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuildingStorey: LongName - Warning test")]
        public void IfcBuildingStoreyBruksenhetsnummeretErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "etasjenummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);

            }
        }
        [Fact(DisplayName = "IfcBuildingStorey: GrossPlannedArea - OK test")]
        public void IfcBuildingStoreyBtaOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "BTA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuildingStorey: GrossPlannedArea - Feil test")]
        public void IfcBuildingStoreyBtaErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "BTA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Fail);

            }
        }
        
        [Fact(DisplayName = "IfcBuildingStorey: NetPlannedArea - OK test")]
        public void IfcBuildingStoreyBraOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "BRA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcBuildingStorey: NetPlannedArea - Feil test")]
        public void IfcBuildingStoreyBraErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBuildingStorey>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBuildingStorey", "BRA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Fail);

            }
        }
        [Fact(DisplayName = "IfcZone: LongName - OK test", Skip = "IfcZone Don't Working")]
        public void IfcZoneBruksenhetsNummerOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "Bruksenhetsnummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcZone: LongName - Warning test", Skip = "IfcZone Don't Working")]
        public void IfcZoneBruksenhetsNummerErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "Bruksenhetsnummer");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }    
        [Fact(DisplayName = "IfcZone: GrossPlannedArea - OK test", Skip = "IfcZone Don't Working")]
        public void IfcZoneBtaOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "Bta");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcZone: GrossPlannedArea - Warning test", Skip = "IfcZone Don't Working")]
        public void IfcZoneBtaErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "BTA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }

        [Fact(DisplayName = "IfcZone: NetPlannedArea - OK test", Skip = "IfcZone Don't Working")]
        public void IfcZoneBraOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "BRA");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);//feil
            }
        }
        [Fact(DisplayName = "IfcZone: Reference - OK test", Skip = "IfcZone Don't Working")]
        public void IfcZoneReferanseOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcZone>().FirstOrDefault();

                var conceptRule = GetConcept("IfcZone", "Referanse");
                var conceptApplies = conceptRule.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);//feil
            }
        }
        [Fact(DisplayName = "IfcWindow: IsExternal - OK test")]
        public void IfcWindowUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWindow>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWindow", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcWindow: IsExternal - Warning test")]
        public void IfcWindowUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWindow>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWindow", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }
        [Fact(DisplayName = "IfcDoor: IsExternal - OK test")]
        public void IfcDoorUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcDoor>().FirstOrDefault();

                var conceptRule = GetConcept("IfcDoor", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcDoor: IsExternal - Warning test")]
        public void IfcDoorUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcDoor>().FirstOrDefault();

                var conceptRule = GetConcept("IfcDoor", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }
        [Fact(DisplayName = "IfcCovering: IsExternal - OK test")]
        public void IfcCoveringUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcCovering>().FirstOrDefault();

                var conceptRule = GetConcept("IfcCovering", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcCovering: IsExternal - Warning test")]
        public void IfcCoveringUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcCovering>().FirstOrDefault();

                var conceptRule = GetConcept("IfcCovering", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }

        [Fact(DisplayName = "IfcWall: IsExternal - OK test")]
        public void IfcWallUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWall", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcWall: IsExternal - Warning test")]
        public void IfcWallUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWall", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }
        [Fact(DisplayName = "IfcWall: LoadBearing - OK test")]
        public void IfcWallLoadBearingOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWall", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcWall: LoadBearing - Warning test")]
        public void IfcWallLoadBearingErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcWall", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }     
        [Fact(DisplayName = "IfcBeam: LoadBearing - OK test")]
        public void IfcBeamLoadBearingOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcBeam>().FirstOrDefault();

                var conceptRule = GetConcept("IfcBeam", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcTransportElement: PredefineType - OK test")]
        public void IfcTransportElementTypeOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcTransportElement>().FirstOrDefault();

                var conceptRule = GetConcept("IfcTransportElement", "heis-type");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var rule = GetTemplateRuleParameters(conceptRule);


                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcColumn: LoadBearing - OK test")]
        public void IfcColumnLoadBearingOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcColumn>().FirstOrDefault();

                var conceptRule = GetConcept("IfcColumn", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcColumn: IsExternal - OK test")]
        public void IfcColumnIsExternalOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcColumn>().FirstOrDefault();

                var conceptRule = GetConcept("IfcColumn", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcSlab: IsExternal - OK test")]
        public void IfcSlabUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSlab>().FirstOrDefault();

                var conceptRule = GetConcept("IfcSlab", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcSlab: IsExternal - Warning test")]
        public void IfcSlabUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSlab>().FirstOrDefault();

                var conceptRule = GetConcept("IfcSlab", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }
        [Fact(DisplayName = "IfcSlab: LoadBearing - OK test")]
        public void IfcSlabLoadBearingOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSlab>().FirstOrDefault();

                var conceptRule = GetConcept("IfcSlab", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcSlab: LoadBearing - Warning test")]
        public void IfcSlabLoadBearingErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcSlab>().FirstOrDefault();

                var conceptRule = GetConcept("IfcSlab", "Vektbærende");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }


        [Fact(DisplayName = "IfcStair: IsExternal - OK test")]
        public void IfcStairIsExternalOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcStair>().FirstOrDefault();

                var conceptRule = GetConcept("IfcStair", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcRamp: IsExternal - OK test")]
        public void IfcRampIsExternalOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcRamp>().FirstOrDefault();

                var conceptRule = GetConcept("IfcRamp", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcRailing: IsExternal - OK test", Skip = "IfcRailing don't found in file")]
        public void IfcRailingIsExternalOkTest()
        {
            
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcRailing>().FirstOrDefault();

                var conceptRule = GetConcept("IfcRailing", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcRailing: Height - OK test", Skip = "IfcRailing don't found in file")]
        public void IfcRailingHeightOkTest()
        {
            using (var ifcModel = OpenValidIfcModelV2())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcRailing>().FirstOrDefault();

                var conceptRule = GetConcept("IfcRailing", "rekkverkethøyde");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var rules = Join(";", items);

                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }

        [Fact(DisplayName = "IfcCurtainWall: IsExternal - OK test")]
        public void IfcCurtainWallUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcCurtainWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcCurtainWall", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var parameter = GetTemplateRuleParameters(conceptRule);


                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcCurtainWall: IsExternal - Warning test")]
        public void IfcCurtainWallUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcCurtainWall>().FirstOrDefault();

                var conceptRule = GetConcept("IfcCurtainWall", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var parameter = GetTemplateRuleParameters(conceptRule);
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }    
        [Fact(DisplayName = "IfcRoof: IsExternal - OK test")]
        public void IfcRoofUtvendigOkTest()
        {
            using (var ifcModel = OpenValidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcRoof>().FirstOrDefault();

                var conceptRule = GetConcept("IfcRoof", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();

                var items = conceptRule?.TemplateRules.Items;
                var parameter = GetTemplateRuleParameters(conceptRule);


                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Pass);
            }
        }
        [Fact(DisplayName = "IfcRoof: IsExternal - Warning test")]
        public void IfcRoofUtvendigErrorTest()
        {
            using (var ifcModel = OpenInvalidIfcModel())
            {
                var doc = new MvdEngine(_mvdXml, ifcModel);
                IPersistEntity ifcEntity = ifcModel.Instances.OfType<IfcRoof>().FirstOrDefault();

                var conceptRule = GetConcept("IfcRoof", "Utvendig");
                var conceptApplies = conceptRule?.AppliesTo(ifcEntity);
                conceptApplies.Should().BeTrue();
                var parameter = GetTemplateRuleParameters(conceptRule);
                var result = conceptRule?.Test(ifcEntity, Concept.ConceptTestMode.ThroughRequirementRequirements);
                result.Should().Be(ConceptTestResult.Warning);
            }
        }
        //*****

        private static string GetTemplateRuleParameters(Concept concept)
        {
            var rules = Empty;
            var items = concept?.TemplateRules.Items;

            if (items?.FirstOrDefault() is TemplateRulesTemplateRule)
            {
                var templateRules = items.Select(i => (TemplateRulesTemplateRule)i);

                rules = Join(";", templateRules.Select(t => t.Parameters));
            }
            return rules;
        }
        private Concept GetConcept(string applicableRootEntity, string conceptName)
        {
            Concept concept = null;
            var krav = _mvdXmlModelView.Roots.Where(conceptRoor => conceptRoor.applicableRootEntity == applicableRootEntity).Select(c => c.Concepts.Where(d => d.name.Contains(conceptName)));
            var applicableRootEntities = _mvdXmlModelView.Roots.Where(conceptRoor => conceptRoor.applicableRootEntity == applicableRootEntity).ToArray();
            if (applicableRootEntities.Any())
            {
                var concepts = applicableRootEntities.FirstOrDefault(c => c.Concepts.Any(j => j.name.Contains(conceptName)))?.Concepts;
                var concepts1 = applicableRootEntities.Select(c => c.Concepts.Where(j => j.name.Contains(conceptName))).First().ToArray();
                if (concepts1.Any())
                {
                    concept = concepts1.FirstOrDefault();
                }
                if (concepts != null && concepts.Any())
                {
                }

            }

            return concept;
        }

        private IfcStore OpenValidIfcModel()
        {
            return IfcStore.Open(@"TestData\SampleHouse_Godkjent-P13 V1.0 Matrikkel.ifc");
        }

        private IfcStore OpenInvalidIfcModel()
        {
            return IfcStore.Open(@"TestData\SampleHouse_Feil-P13 V1.0 Matrikkel.ifc");
        }

        private IfcStore OpenValidIfcModelV2()
        {
            return IfcStore.Open(@"TestData\SampleHouse_GSN_OF(03).ifc");
        }


    }
}
