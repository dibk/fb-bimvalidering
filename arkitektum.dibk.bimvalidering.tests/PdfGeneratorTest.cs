﻿using System.IO;
using arkitektum.dibk.mvdxml.PdfBuilder;
using arkitektum.dibk.validationengine.MvdXml.PdfBuilder;
using FluentAssertions;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class PdfGeneratorTest
    {
        [Fact]
        public void PDFreport()
        {
            string mvdxmlPath = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");
            string dibkLogo = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\11224A4.png");
            string buildingSmartLogo = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\BuildingsmartNorge_Logo.png");
            var fileStream = new PdfGenerator(mvdxmlPath, dibkLogo, buildingSmartLogo).CreatePdf();
            fileStream.Should().BeOfType(typeof(MemoryStream));
        }
    }
}
