﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using arkitektum.dibk.validationengine;
using FluentAssertions;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{
    public class RegexServicesTests
    {
        [Fact]
        public void Regex_CRSNameOkTest()
        {
            var parameter = "CRSName";
            var value = "EPSG:5239";
            string rgxString = new RegexServices().GetRegex(parameter);
            Regex rgx = new Regex(rgxString);
            var result = rgx.IsMatch(value);

            result.Should().BeTrue();
        }
        [Fact]
        public void Regex_CRSNameErrorTest()
        {
            var parameter = "CRSName";
            var value = "5239";
            string rgxString = new RegexServices().GetRegex(parameter);
            Regex rgx = new Regex(rgxString);
            var result = rgx.IsMatch(value);

            result.Should().BeTrue();
        } 
        [Fact]
        public void Regex_CRSNameOKTest2()
        {
            var parameter = "CRSName";
            var value = "239";
            string rgxString = new RegexServices().GetRegex(parameter);
            Regex rgx = new Regex(rgxString);
            var result = rgx.IsMatch(value);

            result.Should().BeFalse();
        }
        [Fact]
        public void Regex_CRSNameErrorTest2()
        {
            var parameter = "CRSName";
            var value = "239";
            string rgxString = new RegexServices().GetRegex(parameter);
            Regex rgx = new Regex(rgxString);
            var result = rgx.IsMatch(value);

            result.Should().BeFalse();
        }
     
        
        [Fact]
        public void REGEX_P13eByggesaksBIM()
        {

            List<string> entitiesPass = new List<string>();
            Dictionary<string, string> entitiesDictionary =
                new Dictionary<string, string>
                {
                    {"LandTitleNumber", "0904-200/2430/1/0"},
                    {"IfcBuildingStorey.LongName", "H01"},
                    {"IfcZone.LongName", "H0101"},
                    {"IfcBuildin.OccupancyType", "164"}
                };
            var countPass = GetValue(entitiesDictionary, entitiesPass).Count();
            Dictionary<string, string> entitiesErrorDictionary =
                new Dictionary<string, string>
                {
                    {"LandTitleNumber", "0904+200/2430/1/0"},
                    {"IfcBuildingStorey.LongName", "H1"},
                    {"IfcZone.LongName", "H010e"},
                    {"IfcBuildin.OccupancyType", "A164"},
                };
            var countFailed = GetValue(entitiesErrorDictionary, entitiesPass).Count();

            // see if the second list have new items that meen thar the regex control Pass
            countPass.Should().Be(countFailed);
        }





        private static IEnumerable<string> GetValue(Dictionary<string, string> entitiesDictionary, List<string> entitiesList)
        {
            foreach (var entity in entitiesDictionary)
            {
                string rgxString = new RegexServices().GetRegex(entity.Key);
                Regex rgx = new Regex(rgxString);
                if (rgx.IsMatch(entity.Value))
                {
                    entitiesList.Add(entity.Key);
                }
            }
            return entitiesList;
        }
    }
}
