﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xbim.MvdXml;

namespace arkitektum.dibk.bimvalidering.tests.Services.Builer
{
    public class MvdXmlV1Builder
    {
        private readonly mvdXML _foMvdXmlV11;

        public MvdXmlV1Builder()
        {
            _foMvdXmlV11 = new mvdXML();
        }

        public mvdXML Build()
        {
            return _foMvdXmlV11;
        }


        public ModelView[] ModelView(string conceptName)
        {
            var modelView = new[]
            {new ModelView()
                {
                    Roots = new []
                    {new ConceptRoot()
                        {
                            Concepts = new []
                            {
                                new Concept()
                                {
                                    name = conceptName,
                                },
                            }
                        },
                    }
                }
            };
            return modelView;
        }

        public Concept Concept(string conceptName, string conceptCode = null, Dictionary<string, string> definitionDefinition = null)
        {
            var concept = new Xbim.MvdXml.Concept()
            {
                name = conceptName
            };
            if (!string.IsNullOrEmpty(conceptCode))
                concept.code = conceptCode;

            if (definitionDefinition != null)
            {
                var i = definitionDefinition.Count(k => k.Key.ToLower().Contains("definition"));
                if (i != 0)
                {
                    concept.Definitions = new DefinitionsDefinition[i];
                    for (var j = 0; j < i; j++)
                    {
                        string definition = string.Concat("definition", j + 1);
                        var lan = string.Concat("lang", j + 1);
                        if (definitionDefinition.Any(k => k.Key == definition))
                        {
                            concept.Definitions[j] =
                                new DefinitionsDefinition()
                                {
                                    Body = new DefinitionsDefinitionBody()
                                    {
                                        Value = definitionDefinition[definition]
                                    }
                                };
                            if (definitionDefinition.Any(k => k.Key == lan))
                                concept.Definitions[j].Body.lang = definitionDefinition[lan];
                        }

                    }

                }
            }
            return concept;
        }
    }
}
