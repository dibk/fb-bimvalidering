﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;
using arkitektum.dibk.bimvalidering.bcf;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.MvdXml;
using FluentAssertions;
using Xbim.Ifc4.Kernel;
using Xunit;

namespace arkitektum.dibk.bimvalidering.tests
{

    public class bcfServiceTest
    {
        //[Fact]
        //public void bcfZipCreated()
        //{
        //    List<ReportResult> reportList = new List<ReportResult>();
        //    string bcfzipguid = Guid.NewGuid().ToString();
        //    string fullIfcFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\SampleHouse - TestFile.ifc");
        //    string markupfil = bcfzipguid + ".bcfzip";
        //    string bcfpath = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\CreatedFiles\" + markupfil);
        //    string ifcProjectGuid = "1o0c33arXF9AEePDXPKItb";
        //    string generateBcfZip = new bcfService().GenerateBcfZip(bcfpath, "SampleHouse - TestFile.ifc", reportList, ifcProjectGuid);
        //    File.Exists(generateBcfZip).Should().BeTrue();
        //    File.Delete(generateBcfZip);
        //}

        //[Fact]
        //public void GenerateMarkup()
        //{
        //    List<ReportResult> reportList = new List<ReportResult>();
        //    var errorGuid = Guid.NewGuid().ToString();
        //    string ifcFilename = null;
        //    string ifcProjectGuid = null;
        //    try
        //    {
        //        var file = "SampleHouse_Feil-P13 V1.0 Matrikkel.ifc";
        //        string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //        ifcFilename = Path.GetFileNameWithoutExtension(ifcDataFile);
        //        string fullMvdxmlFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");

        //        using (var ifcModel = new IFCValidation().OpenModel(ifcDataFile))
        //        {
        //            ifcProjectGuid = ifcModel.Instances.FirstOrDefault<IfcProject>().GlobalId;
        //            var validatedModel = new MvdxmlServices().MvdValidation(ifcModel, fullMvdxmlFilePathName);
        //            var reportResults = new IFCValidation().MvdValidationToReportResults(ifcModel, validatedModel);
        //            reportList = reportResults.Where(x => x.ResultSummary == "Feil").ToList();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        e.Message.Should().BeEquivalentTo("Kan ikke validere modell mot mvdxml-fil");
        //    }
        //    var bytes = new bcfService().GenerateMarkup(errorGuid, ifcFilename, reportList[1], ifcProjectGuid);
        //    var errorList = new List<string>();
        //    XDocument xDocument;
        //    using (var markupStream = new MemoryStream(bytes))
        //    {
        //        xDocument = XDocument.Load(markupStream);
        //    }
        //    string xsdShema = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\Services\TestSkjema\BCF20\markup.xsd");
        //    XmlSchemaSet schema = new XmlSchemaSet();
        //    schema.Add("", xsdShema);
        //    bool validationErrors = false;
        //    xDocument.Validate(schema, (sender, e) =>
        //    {
        //        errorList.Add(e.Message);
        //        validationErrors = true;
        //    });
        //    validationErrors.Should().Be(false);
        //}
        //[Fact]
        //public void GenerateViewpoint()
        //{
        //    List<ReportResult> reportList = new List<ReportResult>();
        //    try
        //    {
        //        var file = "SampleHouse_Feil-P13 V1.0 Matrikkel.ifc";
        //        string ifcDataFile = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\", file);
        //        string fullMvdxmlFilePathName = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\TestData\ramme_etttrinn_igangsetting.mvdxml");

        //        using (var ifcModel = new IFCValidation().OpenModel(ifcDataFile))
        //        {
        //            var validatedModel = new MvdxmlServices().MvdValidation(ifcModel, fullMvdxmlFilePathName);
        //            var reportResults = new IFCValidation().MvdValidationToReportResults(ifcModel, validatedModel);
        //            reportList = reportResults.Where(x => x.ResultSummary == "Feil").ToList();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        e.Message.Should().BeEquivalentTo("Kan ikke validere modell mot mvdxml-fil");
        //    }
        //    string xsdShema = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\Services\TestSkjema\BCF20\visinfo.xsd");
        //    var bytes = new bcfService().GenerateViewpoint(reportList[1]);
        //    var errorList = new List<string>();
        //    XDocument xDocument;
        //    using (var ViewpointStream = new MemoryStream(bytes))
        //    {
        //        xDocument = XDocument.Load(ViewpointStream);
        //    }

        //    XmlSchemaSet schema = new XmlSchemaSet();
        //    schema.Add("", xsdShema);
        //    bool validationErrors = false;
        //    xDocument.Validate(schema, (sender, e) =>
        //    {
        //        errorList.Add(e.Message);
        //        validationErrors = true;
        //    });
        //    validationErrors.Should().Be(false);
        //}
        //[Fact]
        //public void GenerateVersion()
        //{
        //    //TODO  xsd shema "Copy To Output = Copy Always" from the solution or copy the xsd to the unitest ?
        //    string testBaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        //    string solution_dir1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

        //    string xsdShema = Path.Combine(Directory.GetCurrentDirectory() + @"..\..\..\Services\TestSkjema\BCF20\version.xsd");
            
        //    var bytes = new bcfService().GenerateVersion();
        //    var errorList = new List<string>();
        //    XDocument xDocument;
        //    using (var viewpointStream = new MemoryStream(bytes))
        //    {
        //        xDocument = XDocument.Load(viewpointStream);
        //    }

        //    XmlSchemaSet schema = new XmlSchemaSet();
        //    schema.Add("", xsdShema);
        //    bool validationErrors = false;
        //    xDocument.Validate(schema, (sender, e) =>
        //    {
        //        errorList.Add(e.Message);
        //        validationErrors = true;
        //    });
        //   validationErrors.Should().Be(false);
        //}
    }
}
