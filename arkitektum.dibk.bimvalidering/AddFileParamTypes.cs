﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using arkitektum.dibk.bimvalidering.Models;


namespace arkitektum.dibk.bimvalidering
{
    public class AddFileParamTypes : IOperationFilter
    {
        public string Id { get; private set; }

        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            //Add parameters to POST to show upload file box in swagger
            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            name = "IfcFile",
                            @in = "formData",
                            description = "File to upload",
                            required = true,
                            type = "file",

                        },
                        new Parameter
                        {
                            name  = "navn",
                            @in = "header",
                            description = "Bruker navn",
                            required = false,
                            type = "string",
                        },
                        new Parameter
                        {
                            name  = "email",
                            @in = "header",
                            description = "Bruker e-postadresse",
                            required = false,
                            type = "string",
                        },
                        new Parameter
                        {
                            name  = "orgNr",
                            @in = "header",
                            description = "organisasjonsnavnnummer",
                            required = false,
                            type = "string",
                        },
                        new Parameter
                        {
                            name  = "orgName",
                            @in = "header",
                            description = "organisasjonsnavn",
                            required = false,
                            type = "string",
                        }

                    };
                operation.consumes.Add("multipart/form-data");
            }
        }
    }
}