﻿using System.Web;
using System.Web.Optimization;

namespace arkitektum.dibk.bimvalidering
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
           
            bundles.Add(new StyleBundle("~/Content/bower_components/dibk-fb-webcommon/assets/css/styles").Include(
                "~/Content/bower_components/dibk-fb-webcommon/assets/css/main.min.css",
                "~/Content/temp.css"
                ));
        }
    }
}
