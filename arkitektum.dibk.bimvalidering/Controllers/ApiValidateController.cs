﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using arkitektum.dibk.bimvalidering.bcf;
using arkitektum.dibk.validationengine;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http.Description;
using arkitektum.dibk.bimvalidering.Models;
using arkitektum.dibk.bimvalidering.Models.SearchEngine;
using arkitektum.dibk.bimvalidering.Services;
using arkitektum.dibk.validationengine.IfcGeoRef;
using arkitektum.dibk.validationengine.IfcGeoRef.Models;
using Serilog;

namespace arkitektum.dibk.bimvalidering.Controllers
{
    public class ApiValidateController : ApiController
    {

        private readonly ILogger _logger;
        private readonly IFCValidation _ifcValidation;

        public ApiValidateController()
        {
            _logger = Log.ForContext<ApiValidateController>();
            _ifcValidation = new IFCValidation(_logger);
        }
        /// <summary>
        /// Validate ifc model
        /// </summary>
        /// <returns>header location for GET to bcfzip report</returns>
        /// <response code="200">forespørsel vellykket, valideringsrespons send</response>
        /// <response code="404">Kan ikke finne noen fil.</response>  
        /// <response code="415">Ikke støttet medietype</response>
        /// <response code="500">Intern serverfeil</response>
        //[HttpPost, Route("api/validate")]
        [Route("api/validate")]
        [ResponseType(typeof(validationReportSummary))]
        //[Consumes("application/zip")]
        //public HttpResponseMessage Post(UploadFileModel model)
        public HttpResponseMessage Post(string navn, string email, string orgNr, string orgName)
        {

            Stopwatch generalStopwatch = Stopwatch.StartNew();
            Stopwatch stopwatch = Stopwatch.StartNew();

            var httpRequest = HttpContext.Current.Request;
            navn = httpRequest.Params.Get("navn");
            email = httpRequest.Params.Get("email");
            orgNr = httpRequest.Params.Get("orgNr");
            orgName = httpRequest.Params.Get("orgName");
            //Get Guid From Header            
            Guid reportGuid = Guid.NewGuid();

            var correlationId = HttpContext.Current.Request.Headers.Get("x-correlation-id");
            if (!string.IsNullOrEmpty(correlationId))
                Guid.TryParse(correlationId, out reportGuid);

            _logger.Information("Start Validation API");

            HttpResponseMessage response = null;
            string methodName;
            long elapsedTime;

            if (httpRequest.Files.Count == 0)
            {
                _logger.Error("file not fount {elapsedTime}", generalStopwatch.ElapsedMilliseconds);
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kan ikke finne noen fil"); // Not file found
            }

            var postedFile = httpRequest.Files[0];
            string fileName = postedFile.FileName;
            if (postedFile.ContentLength > (250 * 1024 * 1024))
            {
                _logger.Debug("file size {fileSize}", postedFile.ContentLength);
                return Request.CreateResponse(HttpStatusCode.LengthRequired,"Vi beklager, men filstørrelsen må være mindre enn 250MB.");
            }

            string path = Path.Combine(HttpContext.Current.Server.MapPath("~/data/"), fileName);
            // Get/Check File extension
            var allowedExtensions = new[] { ".ifc", ".ifczip", ".ifcxml", ".xBIM" };
            var extension = Path.GetExtension(fileName);
            if (!allowedExtensions.Contains(extension.ToLower()))
            {
                return Request.CreateErrorResponse(HttpStatusCode.UnsupportedMediaType, "Ikke støttet medietype");
            }
            UserInfo userInfo = null;
            string ifcShema = null;
            string ifcProjectGuid = null;
            string ifcViewDefinition = null;
            GeoRefModel geoRefModel = null;

            List<ReportResult> reportErrors = new List<ReportResult>();
            List<ReportResult> reportWarnings = new List<ReportResult>();

            // Save file to Server to get the right ifcShema
            // TODO find out how to get Ifcshema from a stream file

            postedFile.SaveAs(path);

            var mvdPath = HttpContext.Current.Server.MapPath(Path.Combine("~/mvd", "ramme_etttrinn_igangsetting.mvdxml"));
            List<ResultType> resultTypes = null;


            IfcReader ifcReader;
            try
            {
                ifcReader = new IfcReader(_logger, path);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Can't open IfcModel");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Intern serverfeil");
            }


            try
            {
                stopwatch.Start();
                var report = _ifcValidation.ValidateIfcModelWithMvdxml(mvdPath, ifcReader);

                reportErrors = report.Where(x => x.ResultSummary == "Feil").ToList();
                reportWarnings = report.Where(x => x.ResultSummary == "Advarsel").ToList(); // set all filter concept from report to list ReportResul to creat bcfzip file
                if (reportErrors.Any() || reportWarnings.Any())
                {
                    resultTypes =new Helpers(_logger).CreateResultTypeListFormReport(report, reportGuid);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Validatin IfcModel with MvdXml");
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Intern serverfeil");
                return response;
            }

            ifcShema = ifcReader.IfcSchema;
            ifcProjectGuid = ifcReader.IfcProjectGuid;
            ifcViewDefinition = ifcReader.IfcViewDefinition;

            try
            {
                geoRefModel = new GeoRefChecker(_logger, ifcReader).GetIfcModelGeorefLevel();
            }
            catch (Exception e)
            {
                _logger.Error(e, "can't get ifcModel Georeferencering'", generalStopwatch.ElapsedMilliseconds);
            }
            //Dispose IfcModel
            ifcReader.Dispose();

            try
            {
                //Creat bcfzip File 
                string markupfil = reportGuid + ".bcfzip";
                if (reportErrors.Count > 0)
                {

                    string bcfErrorPath = Path.Combine(HttpContext.Current.Server.MapPath(Path.Combine("~/", "markup", markupfil)));
                    new bcfService(_logger).GenerateBcfZip(bcfErrorPath, postedFile.FileName, reportErrors, ifcProjectGuid);
                }
                if (reportWarnings.Count > 0)
                {
                    string bcfzipFilterpath = Path.Combine(HttpContext.Current.Server.MapPath(Path.Combine("~/", "markup", string.Concat("Warning_", markupfil))));
                    new bcfService(_logger).GenerateBcfZip(bcfzipFilterpath, postedFile.FileName, reportWarnings, ifcProjectGuid);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "Creating bcfzip File's message used {elapsedTime}", generalStopwatch.ElapsedMilliseconds);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Kan ikke lage bcfzip-fil");
            }


            try
            {


                // Set headers with link to download the bcfzip file
                var validationRespoceConpts = new validationReportSummary
                {
                    ErrorCount = reportErrors.Count,
                    WarningCount = reportWarnings.Count,
                    ValidationId = reportGuid.ToString(),
                };

                var strPathAndQuery = Request.RequestUri.GetLeftPart(UriPartial.Authority) + Request.GetConfiguration().VirtualPathRoot.TrimEnd('/');
                response = Request.CreateResponse(HttpStatusCode.OK, validationRespoceConpts);
                response.Headers.Location = new Uri(strPathAndQuery + "/api/validationreport/" + reportGuid);
                File.Delete(path);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Creating Responce");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Intern serverfeil");
            }

            try
            {
                //Create Checksum from the Stream file
                postedFile.InputStream.Position = 0;
                var checksumMD5 = GetChecksum.Md5HashFile(postedFile.InputStream);

                // Add the report to the Data Base, retur TRUE if the values are saved
                stopwatch.Start();
                userInfo = new UserServices().CreateUserInfo(navn, email, orgNr, orgName);
                stopwatch.Start();
                DbServices.SaveReportResultInDb(reportErrors.Count, reportWarnings.Count, checksumMD5, reportGuid, userInfo, resultTypes, ifcShema);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Saiving info in DB");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Intern serverfeil");
            }

            //Log in Kibana
            _logger.Information("ReportGuid {reportGuid} - IfcSchema:{ifcSchema} ReferenceView [{modelViewDefinition}], userInfo {@userInfo} Georeferencering level {@geoRefLevel}", reportGuid, ifcShema, ifcViewDefinition, UserInfoElasticIndex.GetUserInfoElasticIndex(userInfo), geoRefModel);
           
            if (resultTypes != null && resultTypes.Any())
            {
                var resulTypeElastic = ResulTypeElasticIndex.GetElasticIndex(resultTypes).ToArray();
                Parallel.ForEach(resulTypeElastic, typeElastic => _logger.Information("ReportGuid {reportGuid} - IfcSchema:{ifcSchema} ReferenceView [{modelViewDefinition}], userInfo {@userInfo} file: {filename} report resuls {@resultTye}", reportGuid, ifcShema, ifcViewDefinition, UserInfoElasticIndex.GetUserInfoElasticIndex(userInfo), fileName, typeElastic));
            }

            generalStopwatch.Stop();
            elapsedTime = generalStopwatch.ElapsedMilliseconds;
            _logger.Debug("{methodName} POST used {elapsedTime}", "Validation API", elapsedTime);
            return response;
        }

        /// <summary>
        /// Returns stream in bcfzip format for validation
        /// </summary>
        /// <param name="id">Validerings-ID fra validering</param>
        /// <returns>validation report of errors in bcfzip file</returns>
        /// <response code="200">Forespørselen er oppfylt og filer sendes</response> 
        /// <response code="404">Det er ingen fil med denne validerings-ID</response>  
        /// <response code="500">Intern serverfeil</response> 
        [Route("api/validationreport/{Id}")]
        [HttpGet]
        [ResponseType(typeof(FileStream))]
        public HttpResponseMessage GetFiles(string id)
        {
            HttpResponseMessage response;
            string rootPath = HttpContext.Current.Server.MapPath("~/markup");
            DirectoryInfo validationReportsDirectory = new DirectoryInfo(rootPath);
            // All the files in the directory that contain same Id
            FileInfo[] validationReports = validationReportsDirectory.GetFiles("*" + id + ".bcfzip");
            if (validationReports.Length != 0)
            {
                var content = new MultipartContent();
                // This foreach is in case that are more than one file, if we create a file with a the list of items that may be filter for example
                foreach (FileInfo validationReport in validationReports)
                {
                    string filePathAndName = validationReport.FullName;
                    var fileStream = new StreamContent(new FileStream(filePathAndName, FileMode.Open));

                    fileStream.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = validationReport.Name
                    };
                    // Recognice bcfzip extention
                    fileStream.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    content.Add(fileStream);

                }
                response = Request.CreateResponse(HttpStatusCode.OK, "Filen ble funnet");
                response.Content = content;
            }
            else response = Request.CreateErrorResponse(HttpStatusCode.NotFound, "Kan ikke finne noen fil");
            return response;
        }

        /// <summary>
        ///  Returns a single bcfzip file for validation, errors or warnings
        /// </summary>
        /// <param name="id">Validerings-ID fra validering</param>
        /// <param name="fileType">Specify which file to obtain "error" or "warning"</param>
        /// <returns>validation report in bcfzip file format</returns>
        /// <response code="200">Forespørselen er oppfylt og filer sendes</response> 
        /// <response code="404">Det er ingen fil med denne validerings-ID</response>  
        /// <response code="400">Intern serverfeil</response> 
        [Route("api/validationreport/{id}/{fileType}")]
        [HttpGet]
        [ResponseType(typeof(FileStream))]
        public HttpResponseMessage GetFile(string id, string fileType)
        {
            fileType = fileType.ToLower();
            HttpResponseMessage response = null;
            string rootPath = HttpContext.Current.Server.MapPath("~/markup");
            DirectoryInfo validationReportsDirectory = new DirectoryInfo(rootPath);
            // All the files in the directory that contain same Id
            if (string.IsNullOrEmpty(fileType) || (!fileType.Equals("error") && !fileType.Equals("warning")))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"Feiltype må angis 'error' eller 'warning' f.eks.:  https://test-bimvalbygg.dibk.no/api/validationreport/{id}/error.");
            StreamContent streamContent;
            FileInfo validationReport = null;

            try
            {
                if (fileType.Equals("warning"))
                    validationReport = validationReportsDirectory.GetFiles("Warning_" + id + ".bcfzip").FirstOrDefault();
                else
                    validationReport = validationReportsDirectory.GetFiles(id + ".bcfzip").FirstOrDefault();

                string filePathAndName = validationReport.FullName;

                streamContent = new StreamContent(new FileStream(filePathAndName, FileMode.Open));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"fil ikke funnet ");
            }

            try
            {
                streamContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = validationReport.Name
                };
                // Recognice bcfzip extention
                streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/bcfzip");

                response = Request.CreateResponse(HttpStatusCode.OK, "Filen ble funnet");
                response.Content = streamContent;

                return response;

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"kan ikke sendes bcfzip filen ");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationId"></param>
        /// <param name="fileChecksum"></param>
        /// <returns>Validate if the file is approved to be sent</returns>
        /// <response code="200">Filen er godkjent</response> 
        /// <response code="406">Filen er ikke godkjent</response>  
        /// <response code="417">Kan ikke finne validation Id</response>  
        /// <response code="412">Filen er endret etter validering</response>  
        /// <response code="500">Internal Server Error</response> 
        [HttpGet]
        [Route("api/valideringsstatus/{validationId:Guid}/{fileChecksum}")]
        public HttpResponseMessage GetValideringsStatus(Guid validationId, string fileChecksum)
        {
            HttpResponseMessage response;
            try
            {
                eByggesaksBIMReportResultsDB reportResults;
                using (var context = new EByggesaksBimDBContext())
                {
                    reportResults = context.EByggesaksBimReportResults.Find(validationId);
                }

                if (reportResults == null)
                {
                    response = Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Kan ikke finne validation Id");
                }
                else
                {
                    if (reportResults.FileChecksum == fileChecksum)
                    {
                        response = reportResults.ReportErrorsCount == 0 ? Request.CreateErrorResponse(HttpStatusCode.OK, "Filen er godkjent") :
                            Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Filen er ikke godkjent");
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, "Filen er endret etter validering");

                    }
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
                Trace.WriteLine(e.StackTrace);
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Problem å få tilgang til DB");
            }
            return response;
        }

    }
}
