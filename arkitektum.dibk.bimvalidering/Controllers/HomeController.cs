﻿using arkitektum.dibk.bimvalidering.bcf;
using arkitektum.dibk.bimvalidering.Models;
using arkitektum.dibk.ifcconvert;
using arkitektum.dibk.validationengine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using arkitektum.dibk.bimvalidering.Logger;
using arkitektum.dibk.bimvalidering.Models.SearchEngine;
using arkitektum.dibk.bimvalidering.Services;
using arkitektum.dibk.validationengine.IfcGeoRef;
using arkitektum.dibk.validationengine.IfcGeoRef.Models;
using arkitektum.dibk.validationengine.Logger;
using arkitektum.dibk.validationengine.Matrikkel;
using arkitektum.dibk.validationengine.MvdXml.PdfBuilder;
using Microsoft.Isam.Esent.Interop;
using Serilog;


namespace arkitektum.dibk.bimvalidering.Controllers
{

    [HandleError]
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private IFCValidation _ifcValidation;
        
        public HomeController()
        {
            _logger = Log.ForContext<HomeController>();
            _ifcValidation = new IFCValidation(_logger);
        }

        public ActionResult Index()
        {
            ViewBag.Title = "BIM validering";
            return View();
        }

        public ActionResult Uttrekk()
        {
            ViewBag.Title = "BIM uttrekk";
            return View();
        }

        public ActionResult PdfActionResult()
        {
            var mvdxmlPath = Path.Combine(Server.MapPath("~/mvd/"), "ramme_etttrinn_igangsetting.mvdxml");
            string dibkLogo = Path.Combine(Server.MapPath("~/data/"), "11224A4.png");
            string buildingSmartLogo = Path.Combine(Server.MapPath("~/data/"), "BuildingsmartNorge_Logo.png");
            Stream fileStream = new PdfGenerator(mvdxmlPath, dibkLogo, buildingSmartLogo).CreatePdf();
            var fileStreamResult =
                new FileStreamResult(fileStream, "application/pdf")
                {
                    FileDownloadName = string.Concat("P13_ebyggesak", ".pdf")
                };
            return fileStreamResult;
        }

        public async Task<ActionResult> ValidationReport(UploadFileModel model)
        {

            Stopwatch generalStopwatch = Stopwatch.StartNew();
            long elapsedTime;
            Stopwatch stopwatch = Stopwatch.StartNew();

            _logger.Information("Start eByggesakBIM - MVC");
            ViewBag.Title = "BIM valideringsrapport";
            var validationReportView = new ValidationReportView();

            var modelFiles = model.File;
            if (modelFiles?.ContentLength > 0)
            {
                var file = modelFiles;

                // https://stackoverflow.com/a/43617282
                double fileSize = 250 * 1024 * 1024;
                if (file.ContentLength > fileSize)
                {
                    _logger.Error($"Whong File size {file.ContentLength} > {fileSize}");
                    throw new FileLoadException("Vi beklager, men filstørrelsen må være mindre enn 250MB.");
                }
                else
                {

                    string path;
                    string extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".ifc", ".ifczip", ".ifcxml", ".xBIM" };
                    if (!allowedExtensions.Contains(extension.ToLower()))
                    {
                        _logger.Error("Wrong file extension");
                        throw new FileLoadException("Ikke lovlig filtype");
                    }
                    string ifcProjectGuid = null;
                    string ifcShema;
                    string ifcViewDefinition;
                    GeoRefModel geoRefModel = null;
                    IfcReader ifcReader;
                    List<ResultType> resultTypes = null;
                    // Create a GUID to use as reference for the report result of the Ifc model and for naming of the bcfzip report files
                    var reportGuid = Guid.NewGuid();
                    List<ReportResult> reportResults;
                    string fileName = String.Empty;
                    try
                    {
                        fileName = Path.GetFileName(file.FileName);
                        validationReportView.ifcFilename = fileName;
                        var tempFolder = Server.MapPath("~/data/");
                        path = Path.Combine(tempFolder, fileName);
                        file.SaveAs(path);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Can't save IfcModel");
                        throw new Exception("Server Feil");
                    }

                    try
                    {
                        ifcReader = new IfcReader(_logger, path);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e, "Can't open IfcModel");
                        throw new Exception("Can't open IfcModel");
                    }

                    try
                    {
                        var mvdXmlFileName = "ramme_etttrinn_igangsetting.mvdxml";
                        var mvdpath = Path.Combine(Server.MapPath("~/mvd/"), mvdXmlFileName);
                        reportResults = _ifcValidation.ValidateIfcModelWithMvdxml(mvdpath, ifcReader);
                        _logger.Information($"IfcModel validated with {mvdXmlFileName}");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "validating IfcModel with mvdxml");
                        throw new Exception("Feil i validering");
                    }

                    ifcShema = ifcReader.IfcSchema;
                    ifcViewDefinition = ifcReader.IfcViewDefinition;
                    ifcProjectGuid = ifcReader.IfcProjectGuid;

                    try
                    {
                        geoRefModel = new GeoRefChecker(_logger, ifcReader).GetIfcModelGeorefLevel();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        _logger.Error(e, "Getting ifc georeferencing level");
                    }
                    try
                    {

                        var warningsResults = reportResults.Where(x => x.ResultSummary == "Advarsel").ToList();
                        var errorsResults = reportResults.Where(x => x.ResultSummary == "Feil").ToList();

                        validationReportView.report = reportResults.Where(x => x.ResultSummary == "Godkjent" || x.ResultSummary == "Anbefales").ToList();
                        validationReportView.reportErrors = errorsResults;
                        validationReportView.reportErrorsCount = errorsResults.Count;
                        validationReportView.ReportWarningResults = warningsResults;
                        validationReportView.reportWarningResultsCount = warningsResults.Count;

                        if (errorsResults.Any() || warningsResults.Any())
                        {
                            resultTypes = new Helpers(_logger).CreateResultTypeListFormReport(reportResults, reportGuid);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Can't create resultType list");
                        throw new Exception("Kan ikke lage rapport");
                    }
                    try
                    {
                        validationReportView.ValidationId = reportGuid;
                        if (resultTypes != null)
                        {
                            bcfService bcf = new bcfService(_logger);
                            stopwatch.Start();
                            string bcfzipFileName = reportGuid + ".bcfzip";
                            if (validationReportView.reportErrors.Count > 0)
                            {
                                string bcfpath = Path.Combine(Server.MapPath("~/markup/"), bcfzipFileName);
                                string bcfFilename = bcf.GenerateBcfZip(bcfpath, validationReportView.ifcFilename, validationReportView.reportErrors, ifcProjectGuid);
                                validationReportView.bcfFilename = "/markup/" + Path.GetFileName(bcfFilename);
                            }
                            if (validationReportView.ReportWarningResults.Count > 0)
                            {
                                string bcfzipFilterpath = Path.Combine(Server.MapPath("~/markup/"), string.Concat("Warning_", bcfzipFileName));
                                string bcfFilterFileName = bcf.GenerateBcfZip(bcfzipFilterpath, validationReportView.ifcFilename, validationReportView.ReportWarningResults, ifcProjectGuid);
                                validationReportView.bcfZipFiletrFilename = "/markup/" + Path.GetFileName(bcfFilterFileName);
                            }
                            stopwatch.Stop();
                            _logger.Debug("Done creating BcfZip files used {elapsedTime}", stopwatch.ElapsedMilliseconds);
                            _logger.Information("Bcfzip report created");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Creating bcfzip files");
                        throw new Exception("Feil i bcf bygging", ex);
                    }

                    //Create WexBim
                    try
                    {
                        var web = new WexBIMServices(_logger);

                        string wex =await web.CreateWexBIM(ifcReader, path);
                        if (!string.IsNullOrEmpty(wex))
                        {
                            string wexPathName = Path.Combine("/data/", Path.GetFileName(wex));
                            validationReportView.wexBIMFilename = wexPathName;
                            web = null;
                            _logger.Information("WexBim created in {wexBimPath}", wexPathName);
                        }
                    }
                    catch (Exception exception)
                    {
                        if (exception.InnerException != null && exception.InnerException.Message.Contains("timed out."))
                        {
                            _logger.Error(exception, "The operation has timed out.");
                            validationReportView.wexBIMFilename = "Time out";

                        }
                        else
                        {
                            _logger.Error(exception, "Can't save wexBim'");
                            validationReportView.wexBIMFilename = "";
                        }
                        //var exception = new Exception("kan ikke opprette 3D-modellvisning");
                        //throw new Exception("Feil i wex bygging", ex);
                    }

                    // Close/Dispose IfcModel
                    ifcReader.Dispose();

                    


                    //Create Checksum from the file
                    var bytes = System.IO.File.ReadAllBytes(path);
                    var checksumMD5 = GetChecksum.Md5HashFile(bytes);

                    // Add the report to the Data Base, retur TRUE if the values are saved
                    UserInfo userInfo = null;
                    try
                    {
                        userInfo = new UserServices().CreateUserInfo(model);
                        stopwatch.Start();
                        DbServices.SaveReportResultInDb(validationReportView.reportErrorsCount, validationReportView.reportWarningResultsCount, checksumMD5, reportGuid, userInfo, resultTypes, ifcShema);
                        stopwatch.Stop();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Saving Report to DB");
                    }

                    System.IO.File.Delete(path);

                    _logger.Information("ReportGuid {reportGuid} - IfcSchema:{ifcSchema} ReferenceView [{modelViewDefinition}], userInfo {@userInfo} Georeferencering level {@geoRefLevel}", reportGuid, ifcShema, ifcViewDefinition, UserInfoElasticIndex.GetUserInfoElasticIndex(userInfo), geoRefModel);
                   
                    if (resultTypes != null && resultTypes.Any())
                    {
                        var resulTypeElastic = ResulTypeElasticIndex.GetElasticIndex(resultTypes).ToArray();
                        Parallel.ForEach(resulTypeElastic, typeElastic => _logger.Information("ReportGuid {reportGuid} - IfcSchema:{ifcSchema} ReferenceView [{modelViewDefinition}], userInfo {@userInfo} file: {filename} report results {@resultTye}", reportGuid, ifcShema, ifcViewDefinition, UserInfoElasticIndex.GetUserInfoElasticIndex(userInfo), fileName, typeElastic));
                    }

                    generalStopwatch.Stop();
                    elapsedTime = generalStopwatch.ElapsedMilliseconds;
                    _logger.Debug("{methodName} used {elapsedTime}", "eByggesakBIM - MVC", elapsedTime);

                    return View(validationReportView);
                }
            }
            throw new Exception("Velg en fil for å validere, prøv igjen");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="FileLoadException"></exception>
        /// <exception cref="Exception"></exception>
        public ActionResult Matrikkelopplysninger()
        {
            MatrikkelregistreringType matrikkelregistrering = new MatrikkelregistreringType();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string path;
                    string extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".ifc", ".ifczip", ".ifcxml" };
                    if (!allowedExtensions.Contains(extension.ToLower()))
                    {
                        throw new FileLoadException("Ikke lovlig filtype");
                    }
                    else
                    {
                        try
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/data/"), fileName);
                            file.SaveAs(path);
                            MatrikkelopplysningerService service = new MatrikkelopplysningerService(_logger);
                            matrikkelregistrering = service.GetMatrikkelregistreringType(path);
                            System.IO.File.Delete(path);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Feil ved utfylling av skjema", ex);
                        }
                    }
                    return View(matrikkelregistrering);
                }

            }
            throw new Exception("Velg en fil for å validere, prøv igjen");
        }
    }
}


