﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using arkitektum.dibk.bimvalidering.Models;
using arkitektum.dibk.validationengine.Logger;
using Microsoft.Ajax.Utilities;
using Serilog;

namespace arkitektum.dibk.bimvalidering
{

    /// <summary>
    /// 
    /// </summary>
    public class EByggesaksBimDBContext : DbContext
    {
        private readonly ILogger _logger = Log.ForContext<EByggesaksBimDBContext>();

        public EByggesaksBimDBContext() : base("DefaultConnection")
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<eByggesaksBIMReportResultsDB> EByggesaksBimReportResults { get; set; }

        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<ResultType> ResultTypes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Map entity to table 
            modelBuilder.Entity<eByggesaksBIMReportResultsDB>().ToTable("ReportResultInfo");
            modelBuilder.Entity<UserInfo>().ToTable("UserInfo");
            modelBuilder.Entity<ResultType>().ToTable("ResultTypes");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class DbServices
    {
        private static readonly ILogger _loggerDb = Log.ForContext<DbServices>();

        internal static bool SaveReportResultInDb(int reportErrorsCount, int reportWarningResultsCount, string checksumSha256, Guid reportGuid, UserInfo userInfo, List<ResultType> resultTypeses, string ifcShema)
        {
            try
            {
                using (var context = new EByggesaksBimDBContext())
                {
                    var eByggesaksBimReportResultsDb = new eByggesaksBIMReportResultsDB()
                    {
                        ReportResultId = reportGuid,
                        ReportErrorsCount = reportErrorsCount,
                        ReportWarningResultsCount = reportWarningResultsCount,
                        FileChecksum = checksumSha256,
                        IfcShema = ifcShema,
                        DateTime = DateTime.Now,
                        ResultTypes = resultTypeses,

                    };
                    if (userInfo != null)
                    {
                        int? userInfoId = null;
                        if (!string.IsNullOrEmpty(userInfo.Email))
                        {
                            userInfoId = GetUserFromEmail(userInfo.Email);
                        }

                        if (!string.IsNullOrEmpty(userInfo.Name) && userInfoId == null)
                        {
                            userInfoId = GetUserFromName(userInfo.Name);
                        }

                        if (userInfoId != null)
                        {
                            eByggesaksBimReportResultsDb.UserInfoId = userInfoId;
                        }
                        else
                        {
                            eByggesaksBimReportResultsDb.UserInfo = userInfo;
                        }
                    }

                    context.EByggesaksBimReportResults.Add(eByggesaksBimReportResultsDb);
                    context.SaveChanges();

                }

                return true;

            }
            catch (Exception ex)
            {

                _loggerDb.Error(ex, "Saiving to DB");
                throw new Exception("Problem å få tilgang til Database");
            }
        }

        public static int? GetUserFromEmail(string email)
        {
            try
            {
                UserInfo userInfo = null;
                using (var context = new EByggesaksBimDBContext())
                {
                    var userExist = context.UserInfos.AnyAsync(u => u.Email == email);
                    if (userExist.Result)
                    {
                        userInfo = context.UserInfos.First(u => u.Email == email);
                    }
                }
                if (userInfo != null)
                    return userInfo.UserInfoId;
            }
            catch (Exception)
            {
                //throw new Exception("Problem å få tilgang til Database");
            }
            return null;
        }
        public static int? GetUserFromName(string name)
        {
            try
            {
                UserInfo userInfo = null;
                using (var context = new EByggesaksBimDBContext())
                {
                    var userExist = context.UserInfos.AnyAsync(u => u.Name == name);
                    if (userExist.Result)
                    {
                        userInfo = context.UserInfos.First(u => u.Name == name);
                    }
                }
                if (userInfo != null)
                    return userInfo.UserInfoId;
            }
            catch (Exception)
            {
                //
            }
            return null;
        }
    }
}