﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using arkitektum.dibk.validationengine;
using Microsoft.Ajax.Utilities;
using System.Web.Razor.Parser.SyntaxTree;
using arkitektum.dibk.validationengine.Logger;
using Block = Microsoft.Ajax.Utilities.Block;

namespace arkitektum.dibk.bimvalidering
{

    /// <summary>
    /// 
    /// </summary>
    public class GetChecksum
    {

        public static string Md5HashFile(byte[] file)
        {
            using (MD5 md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(file)).Replace("-", string.Empty);
            }
        }

        public static string Sha256HashFile(byte[] file)
        {
            using (SHA256Managed sha256 = new SHA256Managed())
            {
                return BitConverter.ToString(sha256.ComputeHash(file)).Replace("-", string.Empty);
            }
        }

        public static string Md5HashFile(Stream stream)
        {
            using (MD5 md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
            }

        }

        public static string Sha256HashFile(Stream stream)
        {
            using (SHA256Managed sha256 = new SHA256Managed())
            {
                return BitConverter.ToString(sha256.ComputeHash(stream)).Replace("-", string.Empty);
            }
        }
    }
}