﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Serilog;
using Serilog.Configuration;

namespace arkitektum.dibk.bimvalidering.Logger.SerilogEnrichers
{
    public static class SerilogConfigurationExtensions
    {
        public static LoggerConfiguration WithCorrelationIdHeader(this LoggerEnrichmentConfiguration enrichmentConfiguration,
            string headerKey = "x-correlation-id")
        {
            if (enrichmentConfiguration == null) throw new ArgumentNullException(nameof(enrichmentConfiguration));
            return enrichmentConfiguration.With(new CorrelationIdHeaderEnricher(headerKey));
        }
    }
}