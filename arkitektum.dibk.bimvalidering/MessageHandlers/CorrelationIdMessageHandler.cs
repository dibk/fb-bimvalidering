﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;

namespace arkitektum.dibk.bimvalidering.MessageHandlers
{
    public class CorrelationIdMessageHandler : DelegatingHandler
    {
        private readonly ILogger _logger = Log.ForContext<CorrelationIdMessageHandler>();
        async protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string correlationId = null;
            var key = request.Headers.FirstOrDefault(k => k.Key.ToLower().Equals("x-correlation-id"));
            if (key.Value != null && !string.IsNullOrWhiteSpace(key.Value.FirstOrDefault()))
            {
                correlationId = key.Value.FirstOrDefault();
                _logger.Information("Header contained CorrelationId: {@CorrelationId}", correlationId);
            }
            else
            {
                correlationId = Guid.NewGuid().ToString();
                _logger.Information("Generated new CorrelationId: {@CorrelationId}", correlationId);
            }
            using (LogContext.PushProperty("CorrelationId", correlationId))
            {
                HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
                response.Headers.Add("x-correlation-id", correlationId);
                return response;
            }
        }
    }
}