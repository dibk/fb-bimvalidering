namespace arkitektum.dibk.bimvalidering.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReportResultsInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "bim.ReportResultInfo",
                c => new
                    {
                        ReportResultId = c.Guid(nullable: false),
                        ReportErrorsCount = c.Int(nullable: false),
                        ReportWarningResultsCount = c.Int(nullable: false),
                        FileChecksum = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.ReportResultId);
            
        }
        
        public override void Down()
        {
            DropTable("bim.ReportResultInfo");
        }
    }
}
