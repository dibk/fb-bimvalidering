namespace arkitektum.dibk.bimvalidering.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateReportResultsInfo : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "bim.ReportResultInfo", newSchema: "dbo");
            AlterColumn("dbo.ReportResultInfo", "FileChecksum", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ReportResultInfo", "FileChecksum", c => c.String(maxLength: 64));
            MoveTable(name: "dbo.ReportResultInfo", newSchema: "bim");
        }
    }
}
