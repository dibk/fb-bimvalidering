namespace arkitektum.dibk.bimvalidering.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeModelLogin : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ResultTypes",
                c => new
                    {
                        ResultTypeId = c.Int(nullable: false, identity: true),
                        IfcEntityName = c.String(),
                        RuleTypeName = c.String(),
                        RuleTypeResult = c.String(),
                        RuleTypeCount = c.Int(nullable: false),
                        ReportResultId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ResultTypeId)
                .ForeignKey("dbo.ReportResultInfo", t => t.ReportResultId, cascadeDelete: true)
                .Index(t => t.ReportResultId);
            
            CreateTable(
                "dbo.UserInfo",
                c => new
                    {
                        UserInfoId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OrgNr = c.String(),
                        OrgName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.UserInfoId);
            
            AddColumn("dbo.ReportResultInfo", "DateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.ReportResultInfo", "IfcShema", c => c.String());
            AddColumn("dbo.ReportResultInfo", "UserInfoId", c => c.Int());
            CreateIndex("dbo.ReportResultInfo", "UserInfoId");
            AddForeignKey("dbo.ReportResultInfo", "UserInfoId", "dbo.UserInfo", "UserInfoId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportResultInfo", "UserInfoId", "dbo.UserInfo");
            DropForeignKey("dbo.ResultTypes", "ReportResultId", "dbo.ReportResultInfo");
            DropIndex("dbo.ResultTypes", new[] { "ReportResultId" });
            DropIndex("dbo.ReportResultInfo", new[] { "UserInfoId" });
            DropColumn("dbo.ReportResultInfo", "UserInfoId");
            DropColumn("dbo.ReportResultInfo", "IfcShema");
            DropColumn("dbo.ReportResultInfo", "DateTime");
            DropTable("dbo.UserInfo");
            DropTable("dbo.ResultTypes");
        }
    }
}
