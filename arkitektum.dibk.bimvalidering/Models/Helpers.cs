﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.Logger;
using Serilog;

namespace arkitektum.dibk.bimvalidering.Models
{
    public class Helpers
    {
        private ILogger _logger;

        public Helpers(ILogger logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportResults"></param>
        /// <param name="reportGuid"></param>
        /// <returns></returns>
        public List<ResultType> CreateResultTypeListFormReport(List<ReportResult> reportResults, Guid reportGuid)
        {
            var resultTypes = new List<ResultType>();
            using (var logg = new ValidationPerfTimerLogger(_logger))
            {
                foreach (var reportResult in reportResults)
                {
                    if (reportResult == null)
                        continue;

                    if (reportResult.ResultSummary == "Feil" || reportResult.ResultSummary == "Advarsel")
                    {
                        var entityName = IfcTranslator.GetEntityName(reportResult);

                        if (resultTypes.Any(r => (r.RuleTypeName == reportResult.ConceptName || r.RuleTypeName.Contains("Konsept er ukjent")) && r.IfcEntityName == entityName))
                        {
                            var report = resultTypes.First((r => (r.RuleTypeName == reportResult.ConceptName || r.RuleTypeName.Contains("Konsept er ukjent")) && r.IfcEntityName == entityName));
                            //report.FirstOrDefault().RuleTypeCount = report.FirstOrDefault().RuleTypeCount + 1;
                            report.RuleTypeCount = report.RuleTypeCount + 1;
                        }
                        else
                        {
                            var resultType = new ResultType()
                            {
                                IfcEntityName = entityName,
                                RuleTypeCount = 1,
                                RuleTypeResult = reportResult.ResultSummary,
                                ReportResultId = reportGuid
                            };
                            ////when IfcEntity is not supported we dont have information in mvc xml to get Rule TypeName thats way we have to add the requirement text us rule name
                            resultType.RuleTypeName = reportResult.InvolvedRequirement.Contains("Konsept er ukjent") ? reportResult.InvolvedRequirement : reportResult.ConceptName;
                            resultTypes.Add(resultType);
                        }
                    }
                }
            }
            return resultTypes;
        }
    }
}