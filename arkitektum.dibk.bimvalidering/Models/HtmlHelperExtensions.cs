﻿using System.Web.Configuration;
using System.Web.Mvc;

namespace arkitektum.dibk.bimvalidering.Models
{
    public static class HtmlHelperExtensions
    {
        public static string ApplicationVersionNumber(this HtmlHelper helper)
        {
            return WebConfigurationManager.AppSettings["AppVersionNumber"];
        }
    }
}