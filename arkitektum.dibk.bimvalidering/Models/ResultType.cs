﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace arkitektum.dibk.bimvalidering.Models
{
    public class ResultType
    {
        [Key]
        public int ResultTypeId { get; set; }

        public string IfcEntityName { get; set; }
        public string RuleTypeName { get; set; }
        public string RuleTypeResult { get; set; }
        public int RuleTypeCount { get; set; }


        
        public Guid ReportResultId { get; set; }
        [ForeignKey("ReportResultId")]
        public virtual eByggesaksBIMReportResultsDB EByggesaksBimReportResultsDb { get; set; }
        
    }
}