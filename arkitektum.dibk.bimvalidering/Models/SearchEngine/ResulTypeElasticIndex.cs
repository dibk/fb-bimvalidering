﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace arkitektum.dibk.bimvalidering.Models.SearchEngine
{
    public class ResulTypeElasticIndex
    {
        public string IfcEntityName { get; set; }
        public string RuleTypeName { get; set; }
        public string RuleTypeResult { get; set; }
        public int RuleTypeCount { get; set; }

        public static List<ResulTypeElasticIndex> GetElasticIndex(List<ResultType> resultTypes)
        {
            List<ResulTypeElasticIndex> resulTypeElastic = null;
            if (resultTypes!=null && resultTypes.Any())
            {
                resulTypeElastic = new List<ResulTypeElasticIndex>();
                foreach (var resultType in resultTypes)
                {
                    resulTypeElastic.Add(new ResulTypeElasticIndex()
                    {
                        IfcEntityName = resultType.IfcEntityName,
                        RuleTypeName = resultType.RuleTypeName,
                        RuleTypeResult = resultType.RuleTypeResult,
                        RuleTypeCount = resultType.RuleTypeCount
                    }
                    );
                }
            }
            return resulTypeElastic;
        }
    }
}