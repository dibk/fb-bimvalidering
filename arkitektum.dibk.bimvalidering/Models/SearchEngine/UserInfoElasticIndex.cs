﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace arkitektum.dibk.bimvalidering.Models.SearchEngine
{
    //[ElasticsearchType(IdProperty = nameof(ArchiveReference))]
    public class UserInfoElasticIndex
    {
        public string Name { get; set; }
        public string OrgNr { get; set; }
        public string OrgName { get; set; }

        public static UserInfoElasticIndex GetUserInfoElasticIndex(UserInfo userInfo)
        {
            if (userInfo == null)
                return null;

            var userInfoElastic = new UserInfoElasticIndex()
            {
                Name = userInfo.Name,
                OrgNr = userInfo.OrgNr,
                OrgName = userInfo.OrgName
            };
            return userInfoElastic;
        }
    }
}