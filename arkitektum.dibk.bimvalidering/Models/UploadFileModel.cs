﻿using System.Collections.Generic;
using System.Web;

namespace arkitektum.dibk.bimvalidering.Models
{
    public class UploadFileModel
    {
        public UploadFileModel()
        {
            Files = new List<HttpPostedFileBase>();
        }
        public List<HttpPostedFileBase> Files { get; set; }
        public HttpPostedFileBase File { get; set; }

        public string Navn { get; set; }
        public string Email { get; set; }
        public string Organisasjonsnummer { get; set; }
        public string OrganisasjonsNavn { get; set; }
    }
}