﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace arkitektum.dibk.bimvalidering.Models
{
    public class UserInfo
    {
        [Key]
        public int UserInfoId { get; set; }
        public string Name { get; set; }
        public string OrgNr { get; set; }
        public string OrgName { get; set; }
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress]
        public string Email { get; set; }

        //public Guid ReportResultId { get; set; }
        public List<eByggesaksBIMReportResultsDB> EByggesaksBimReportResultsDbs { get; set; }
    }
}