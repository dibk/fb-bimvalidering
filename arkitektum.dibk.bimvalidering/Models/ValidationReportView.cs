﻿using System;
using System.Collections.Generic;
using arkitektum.dibk.validationengine;

namespace arkitektum.dibk.bimvalidering.Models
{
    public class ValidationReportView
    {
        public string ifcFilename;
        public Guid ValidationId;
        public List<ReportResult> report;
        public List<ReportResult> reportErrors;
        public List<ReportResult> ReportWarningResults;
        public int reportWarningResultsCount;
        public int reportErrorsCount;
        public string wexBIMFilename;
        //hvis en ønsker å vise 3D modell av det som testes - https://xbimteam.github.io/examples/creating-wexbim-file.html http://xbimteam.github.io/XbimWebUI/
        public string bcfFilename;
        public string bcfZipFiletrFilename;
    }
}