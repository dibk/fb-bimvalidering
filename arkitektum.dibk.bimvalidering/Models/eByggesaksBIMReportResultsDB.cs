﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace arkitektum.dibk.bimvalidering.Models
{
    /// <summary>
    /// Modell to create DB from the report result of the model 
    /// </summary>
    [Table("ReportResultInfo")]
    public class eByggesaksBIMReportResultsDB
    {
        /// <summary>
        /// GUID from the report result, same GUID in the bcfzip file 
        /// </summary>
        [Key]
        [Column(Order = 0)]
        public Guid ReportResultId { get; set; }
        /// <summary>
        /// Number of error in the report
        /// </summary>
        [Column(Order = 1)]
        public DateTime DateTime { get; set; }
        [Column(Order = 2)]
        public int ReportErrorsCount { get; set; }
        /// <summary>
        /// Number of warnings in the report
        /// </summary>
        [Column(Order = 3)]
        public int ReportWarningResultsCount { get; set; }
        [Column(Order = 4)]
        public string IfcShema { get; set; }
        /// <summary>
        /// SHA256 checksum of the sent file
        /// </summary>
        [Column(Order = 5)]
        public string FileChecksum { get; set; }
        [Column(Order = 6)]


        public int? UserInfoId { get; set; }
        [ForeignKey("UserInfoId")]
        public virtual UserInfo UserInfo { get; set; }

        public List<ResultType> ResultTypes { get; set; }
    }

}