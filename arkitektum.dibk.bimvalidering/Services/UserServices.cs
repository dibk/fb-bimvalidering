﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using arkitektum.dibk.bimvalidering.Models;

namespace arkitektum.dibk.bimvalidering.Services
{
    public class UserServices
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="navn"></param>
        /// <param name="email"></param>
        /// <param name="orgNr"></param>
        /// <param name="orgNavn"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public UserInfo CreateUserInfo(string navn, string email, string orgNr, string orgNavn)
        {
            UserInfo userInfo = null;
            if (!string.IsNullOrEmpty(navn) || !string.IsNullOrEmpty(email) || !string.IsNullOrEmpty(orgNr) || !string.IsNullOrEmpty(orgNavn))
            {
                userInfo = new UserInfo()
                {
                    Name = navn,
                    Email = email,
                    OrgNr = orgNr,
                    OrgName = orgNavn,
                };
            }
            return userInfo;
        }
        public UserInfo CreateUserInfo(UploadFileModel model)
        {
         return CreateUserInfo(model.Navn, model.Email, model.Organisasjonsnummer, model.OrganisasjonsNavn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="navn"></param>
        /// <param name="email"></param>
        /// <param name="orgNr"></param>
        /// <param name="orgNavn"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(string navn, string email, string orgNr, string orgNavn, string guid)
        {
            var userinfo = new UserInfo();

            return userinfo;
        } /// <summary>
        /// 
        /// </summary>
        /// <param name="navn"></param>
        /// <param name="email"></param>
        /// <param name="orgNr"></param>
        /// <param name="orgNavn"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public UserInfo UpdateUserInfo(string navn, string email, string orgNr, string orgNavn, string guid)
        {
            var userinfo = new UserInfo();

            return userinfo;
        }

    }
}