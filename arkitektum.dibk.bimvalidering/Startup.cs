﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using arkitektum.dibk.bimvalidering.Logger;
using arkitektum.dibk.bimvalidering.Logger.SerilogEnrichers;
using Microsoft.Owin;
using Owin;
using Serilog;
using Serilog.Events;

[assembly: OwinStartup(typeof(arkitektum.dibk.bimvalidering.Startup))]
namespace arkitektum.dibk.bimvalidering
{
    public partial class Startup
    {
     
        public void Configuration(IAppBuilder app)
        {
            ConfigureLogging();
        }

        private void ConfigureLogging()
        {
            var elasticSearchUrl = ConfigurationManager.AppSettings["SearchEngine:ConnectionUrl"];
            var elasticUsername = ConfigurationManager.AppSettings["SearchEngine:ConnectionUsername"];
            var elasticPassword = ConfigurationManager.AppSettings["SearchEngine:ConnectionPassword"];
            var elasticIndexFormat = ConfigurationManager.AppSettings["SearchEngine:SerilogSinkIndexFormat"];

            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var loggerPath = Path.Combine(basePath, @"App_data\LoggerDebugTemp.txt");

            //lOGG https://github.com/serilog/serilog/wiki/Debugging-and-Diagnostics
            //Serilog.Debugging.SelfLog.Enable(msg => System.Diagnostics.Trace.WriteLine(msg));

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithCorrelationIdHeader()
                .WriteTo.Console()
                .WriteTo.Trace(outputTemplate: "{Timestamp:HH:mm:ss.fff} {CorrelationId} {SourceContext} [{Level}] {ArchiveReference} {Message}{NewLine}{Exception}")
                //.WriteTo.File(loggerPath, LogEventLevel.Verbose, "{Timestamp:HH:dd/MM/yy tt:mm:ss.fff} {CorrelationId} {SourceContext} [{Level}] {ArchiveReference} {Message}{NewLine}{Exception}")
                .WriteTo.Elasticsearch(new Serilog.Sinks.Elasticsearch.ElasticsearchSinkOptions(new Uri(elasticSearchUrl))
                {
                    AutoRegisterTemplate = true,
                    ModifyConnectionSettings = connectionConfiguration => connectionConfiguration.BasicAuthentication(elasticUsername, elasticPassword),
                    IndexFormat = elasticIndexFormat,
                    TypeName = null
                })
                .CreateLogger();
        }
    }
}
