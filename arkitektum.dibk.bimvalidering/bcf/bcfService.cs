﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using arkitektum.dibk.validationengine;
using System.IO;
using System.Xml.Serialization;
using System.IO.Compression;
using System.Runtime.InteropServices;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine.Logger;
using Serilog;


namespace arkitektum.dibk.bimvalidering.bcf
{
    public class bcfService
    {
        private readonly ILogger _logger;
        public bcfService(ILogger logger)
        {
            _logger = logger;
            _logger= _logger.ForContext<bcfService>();
        }
        public string GenerateBcfZip(string bcfZipFile, string ifcFilename, List<ReportResult> reportlistReportResults, string ifcProjectGuid)
        {

            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                using (var fileStream = new FileStream(bcfZipFile, FileMode.CreateNew))
                {
                    using (var archive = new ZipArchive(fileStream, ZipArchiveMode.Create, true, System.Text.Encoding.UTF8))
                    {
                        var verBytes = GenerateVersion();
                        var zipArchiveEntryVer = archive.CreateEntry("bcf.version", CompressionLevel.Fastest);
                        using (var zipStream = zipArchiveEntryVer.Open())
                            zipStream.Write(verBytes, 0, verBytes.Length);

                        foreach (var item in reportlistReportResults)
                        {
                            string errorGuid = Guid.NewGuid().ToString();
                            var bytes = GenerateMarkup(errorGuid, ifcFilename, item, ifcProjectGuid);
                            var viewpBytes = GenerateViewpoint(item);
                            //** bcfzip standard required to create folder structure, but DDS-CAD cannot import when it has this structure
                            //var mappen = archive.CreateEntry(errorGuid + "/", CompressionLevel.Fastest);
                            //using (var zipStream = mappen.Open())
                            //    zipStream.Write(bytes, 0, bytes.Length);
                            var zipArchiveEntry = archive.CreateEntry(errorGuid + "/markup.bcf", CompressionLevel.Fastest);
                            using (var zipStream = zipArchiveEntry.Open())
                                zipStream.Write(bytes, 0, bytes.Length);
                            var zipArchiveEntryViewport = archive.CreateEntry(errorGuid + "/viewpoint.bcfv", CompressionLevel.Fastest);
                            using (var zipStream = zipArchiveEntryViewport.Open())
                                zipStream.Write(viewpBytes, 0, viewpBytes.Length);
                            var basePath = AppDomain.CurrentDomain.BaseDirectory;
                            archive.CreateEntryFromFile(Path.Combine(basePath, @"data\snapshot.png"), errorGuid + "/snapshot.png", CompressionLevel.Fastest);
                        }
                    }
                }
                return bcfZipFile; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorGuid"></param>
        /// <param name="ifcFilename"></param>
        /// <param name="item"></param>
        /// <param name="ifcProjectGuid"></param>
        /// <returns></returns>
        public byte[] GenerateMarkup(string errorGuid, string ifcFilename, ReportResult item, string ifcProjectGuid)
        {
            var topicDictionary = GetTopicStatusAndType(item);
            string beskrivelse = null;
            if (item != null)
            {
                var definiton =new IFCValidation(_logger).GetDefinition(item);
                if (definiton != null)
                    beskrivelse = definiton["value"];
            }
            string commentGuid = Guid.NewGuid().ToString();
            string viewPointsGuid = Guid.NewGuid().ToString();
            Markup markup = new Markup
            {
                Header = new[]
                {
                    new HeaderFile()
                    {
                        IfcProject = ifcProjectGuid,
                        IfcSpatialStructureElement = item.EntityIfcGuid,
                        Filename = ifcFilename,
                        Date = IfcTranslator.GetDateTimeFromReportResul(item),
                        DateSpecified = true
                    }
                },
                Topic = new Topic()
                {
                    Guid = errorGuid,
                    ReferenceLink = IfcTranslator.GetReferenceLink(item),
                    Title = item.ConceptName,
                    CreationDate = DateTime.Now,
                    CreationDateSpecified = true,
                    CreationAuthor = "DiBK BIM validering byggesoknad",
                    TopicType = topicDictionary["topicType"],
                    TopicStatus = topicDictionary["topicStatus"]
                },
                Comment = new[]
                {
                    new Comment()
                    {
                        Guid = commentGuid,
                        Status = topicDictionary["topicStatus"],
                        Date = DateTime.Now,
                        Author = "DiBK BIM validering byggesøknad",
                        Comment1 = "EntityLabel (" + item.EntityLabel + "): " + beskrivelse,
                        Topic = new CommentTopic() {Guid = errorGuid},
                        Viewpoint = new CommentViewpoint() {Guid = viewPointsGuid}
                    }
                },
                Viewpoints = new[]
                {
                    new ViewPoint()
                    {
                        Guid = viewPointsGuid,
                        Snapshot = "snapshot.png",
                        Viewpoint = "viewpoint.bcfv"
                    },
                }
            };
            MemoryStream fs = new MemoryStream();
            XmlSerializer xSer = new XmlSerializer(typeof(Markup));
            xSer.Serialize(fs, markup);
            return fs.ToArray();
        }
        public byte[] GenerateViewpoint(ReportResult item)
        {
            VisualizationInfo visualizationInfo = new VisualizationInfo
            {
                Components = new[]
                {
                    new Component()
                    {
                        IfcGuid = item.EntityIfcGuid,
                        Selected = true,SelectedSpecified = true,
                        Visible = true,
                        Color = new byte[] {255,00,00} //RGB "Red1"
                    }
                },
                PerspectiveCamera = new PerspectiveCamera()
                {
                    FieldOfView = 45,
                    CameraDirection = new Direction() { X = 1, Y = 1, Z = 1 },
                    CameraUpVector = new Direction() { X = 1, Y = 1, Z = 1 },
                    CameraViewPoint = new Point() { X = 1, Y = 1, Z = 1 }
                }
            };

            MemoryStream fs = new MemoryStream();
            XmlSerializer xSer = new XmlSerializer(typeof(VisualizationInfo));
            xSer.Serialize(fs, visualizationInfo);
            return fs.ToArray();
        }
        public byte[] GenerateVersion()
        {
            Version info = new Version();
            info.VersionId = "2.0";
            info.DetailedVersion = "2.0";

            MemoryStream fs = new MemoryStream();
            XmlSerializer xSer = new XmlSerializer(typeof(Version));
            xSer.Serialize(fs, info);
            return fs.ToArray();
        }

        private static Dictionary<string, string> GetTopicStatusAndType(ReportResult item)
        {
            var topicDictionary = new Dictionary<string, string>()
            {
                {"topicType", null},
                {"topicStatus", null}
            };
            topicDictionary["topicStatus"] = "";
            var noko = topicDictionary["topicStatus"];
            switch (item.ResultSummary)
            {
                //change Open to Unknown...
                case "Feil":
                    topicDictionary["topicType"] = "Issue";
                    topicDictionary["topicStatus"] = "Unknown";
                    break;
                case "Filter advarsel":
                    topicDictionary["topicType"] = "Issue";
                    topicDictionary["topicStatus"] = "Unknown";
                    break;
                case "Godkjent":
                    topicDictionary["topicType"] = "Issue";
                    topicDictionary["topicStatus"] = "Closed";
                    break;
                default:
                    topicDictionary["topicType"] = "Issue";
                    topicDictionary["topicStatus"] = "Unknown";
                    break;
            }
            return topicDictionary;
        }
    }
}