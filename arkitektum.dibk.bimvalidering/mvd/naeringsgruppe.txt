#Kode #Navn
S Annen tjenesteyting
Y Annet som ikke er næring
B Bergverksdrift og utvinning
X Bolig
F Bygge- og anleggsvirksomhet
D Elektrisitets- gass- damp- og varmtvannsforsyning
M Faglig  vitenskaplig og teknisk tjenesteyting
K Finansierings- og forsikringsvirksomhet
N Forretningsmessig tjenesteyting
Q Helse- og sosialtjenester
C Industri
J Informasjon og kommunikasjon
U Internasjonale organisasjoner og organer
A Jordbruk  skogbruk og fiske
R Kulturell virksomhet underholdning og fritidsaktiviteter
T Lønnet arbeid i private husholdninger
O Offentlig administrasjon og forsvar og trygdeordninger underlagt offentlig forvaltning
L Omsetning og drift av fast eiendom
I Overnattings- og serveringsvirksomhet
H Transport og lagring
P Undervisning
E Vannforsyning  avløps- og renovasjonsvirksomhet
G Varehandel reparasjon av motorvogner
