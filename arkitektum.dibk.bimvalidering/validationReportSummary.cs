﻿namespace arkitektum.dibk.bimvalidering
{

    public class validationReportSummary
    {
        /// <summary>
        /// Antall av feilene som finnes i modellen
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// Antall av filter advarslene som finnes i modell
        /// </summary>
        public int WarningCount { get; set; }
        /// <summary>
        /// Referansekode av verifisering - Id
        /// </summary>
        public string ValidationId { get; set; }
    }
}