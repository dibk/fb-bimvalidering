﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.Logger;
using Microsoft.Isam.Esent.Interop;
using Serilog;
using Serilog.Core;
using Xbim.Ifc;
using Xbim.ModelGeometry.Scene;

namespace arkitektum.dibk.ifcconvert
{
    public class WexBIMServices
    {
        private readonly ILogger _logger;

        //private static Task<string> _createWexBIM;
        public WexBIMServices(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<wexBIM>();
        }


        public async Task<string> CreateWexBIM(IfcReader fileName, string icfModelPath)
        {
            var model = fileName.IfcModel;
            var wexBimFilename = Path.ChangeExtension(icfModelPath, "wexBIM");
            var millisecondsTimeout = 60000;
            try
            {
                var wexbimModel = new Task(() => { if (model != null) WexBim(model, wexBimFilename); });
                wexbimModel.Start();
                await wexbimModel.TimeoutAfter(millisecondsTimeout);

            }
            catch (Exception e)
            {
                if (e.Message.Contains("timed out"))
                {
                    _logger.Error("The operation has timed out. processing time is greater than  {millisecondsTimeout} milliseconds");
                    wexBimFilename = "Time out";
                    throw new Exception("Time Out");
                }

                _logger.Error(e, "Can't Create WexBim'");
                wexBimFilename = "";
                throw new Exception(e.Message);

            }

            return wexBimFilename;
        }

        private void WexBim(IfcStore model, string wexBimFilename)
        {
            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                try
                {
                    var context = new Xbim3DModelContext(model);
                    context.CreateContext();
                }
                catch (Exception e)
                {
                    _logger.Error(e, "Can't create xbim 3D model context");
                    throw;
                }

                if (!string.IsNullOrEmpty(wexBimFilename))
                    using (var wexBiMfile = File.Create(wexBimFilename))
                    {
                        using (var wexBimBinaryWriter = new BinaryWriter(wexBiMfile))
                        {
                            model.SaveAsWexBim(wexBimBinaryWriter);
                            wexBimBinaryWriter.Close();
                        }
                        wexBiMfile.Close();
                    }
            }
        }
    }
    public static class TaskExtentions
    {
        internal struct VoidTypeStruct { }  // See Footnote #1
        public static async Task TimeoutAfter1(this Task task, int millisecondsTimeout)
        {
            if (task == await Task.WhenAny(task, Task.Delay(millisecondsTimeout)))
                await task;
            else
                throw new TimeoutException();
        }
        public static Task TimeoutAfter(this Task task, int millisecondsTimeout)
        {
            // Short-circuit #1: infinite timeout or task already completed
            if (task.IsCompleted || (millisecondsTimeout == Timeout.Infinite))
            {
                // Either the task has already completed or timeout will never occur.
                // No proxy necessary.
                return task;
            }

            // tcs.Task will be returned as a proxy to the caller
            TaskCompletionSource<VoidTypeStruct> tcs = new TaskCompletionSource<VoidTypeStruct>();

            // Short-circuit #2: zero timeout
            if (millisecondsTimeout == 0)
            {
                // We've already timed out.
                tcs.SetException(new TimeoutException());
                return tcs.Task;
            }

            // Set up a timer to complete after the specified timeout period
            Timer timer = new Timer(state =>
            {
                // Recover your state information
                var myTcs = (TaskCompletionSource<VoidTypeStruct>)state;

                // Fault our proxy with a TimeoutException
                myTcs.TrySetException(new TimeoutException());
            }, tcs, millisecondsTimeout, Timeout.Infinite);

            // Wire up the logic for what happens when source task completes
            task.ContinueWith((antecedent, state) =>
                {
                    // Recover our state data
                    var tuple =
                            (Tuple<Timer, TaskCompletionSource<VoidTypeStruct>>)state;

                    // Cancel the Timer
                    tuple.Item1.Dispose();

                    // Marshal results to proxy
                    MarshalTaskResults(antecedent, tuple.Item2);
                },
                Tuple.Create(timer, tcs),
                CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously,
                TaskScheduler.Default);

            return tcs.Task;
        }
        internal static void MarshalTaskResults<TResult>(
            Task source, TaskCompletionSource<TResult> proxy)
        {
            switch (source.Status)
            {
                case TaskStatus.Faulted:
                    proxy.TrySetException(source.Exception);
                    break;
                case TaskStatus.Canceled:
                    proxy.TrySetCanceled();
                    break;
                case TaskStatus.RanToCompletion:
                    Task<TResult> castedSource = source as Task<TResult>;
                    proxy.TrySetResult(
                        castedSource == null ? default(TResult) : // source is a Task
                            castedSource.Result); // source is a Task<TResult>
                    break;
            }
        }
    }
}
