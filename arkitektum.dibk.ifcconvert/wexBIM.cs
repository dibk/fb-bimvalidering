﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.Logger;
using Serilog;
using Xbim.Ifc;
using Xbim.ModelGeometry.Scene;


namespace arkitektum.dibk.ifcconvert
{
    public class wexBIM
    {
        private readonly ILogger _logger;

        public wexBIM(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<wexBIM>();
        }

        public string CreateWexBIM(IfcReader fileName, string icfModelPath)
        {
            Xbim3DModelContext context;
            var model = fileName.IfcModel;

            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                try
                {
                    context = new Xbim3DModelContext(model);
                    context.CreateContext();
                }
                catch (Exception e)
                {
                    _logger.Error(e, "Can't create xbim 3D model context");
                    return null;
                }
                try
                {
                    var wexBimFilename = Path.ChangeExtension(icfModelPath, "wexBIM");
                    if (!string.IsNullOrEmpty(wexBimFilename))
                        using (var wexBiMfile = File.Create(wexBimFilename))
                        {
                            using (var wexBimBinaryWriter = new BinaryWriter(wexBiMfile))
                            {
                                try
                                {
                                    model.SaveAsWexBim(wexBimBinaryWriter);
                                }
                                catch (Exception e)
                                {
                                    wexBimFilename = "";
                                    _logger.Error(e, "Can't save wexBim'");
                                }

                                wexBimBinaryWriter.Close();
                            }
                            wexBiMfile.Close();
                        }
                    return wexBimFilename;
                }
                catch (Exception e)
                {
                    _logger.Error(e, "Debug - Error creating WexBim");
                    throw new Exception("kan ikke opprette 3D-modellvisning", e);
                }

            }
        }

        public string CreateWexBIM(string fileName)
        {
            string wexBimFilename;
            try
            {
                using (var model = IfcStore.Open(fileName))
                {

                    var context = new Xbim3DModelContext(model);
                    context.CreateContext();

                    wexBimFilename = Path.ChangeExtension(fileName, "wexBIM");
                    using (var wexBiMfile = File.Create(wexBimFilename))
                    {
                        using (var wexBimBinaryWriter = new BinaryWriter(wexBiMfile))
                        {
                            try
                            {
                                model.SaveAsWexBim(wexBimBinaryWriter);
                            }
                            catch (Exception e)
                            {
                                wexBimFilename = "";
                                _logger.Error(e, "Can't save wexBim'");
                            }

                            wexBimBinaryWriter.Close();
                        }
                        wexBiMfile.Close();
                    }
                    context = null;
                    model.Close();
                }

                return wexBimFilename;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Debug - Error creating WexBim");
                throw new Exception("kan ikke opprette 3D-modellvisning", e);
            }
        }
    }
}
