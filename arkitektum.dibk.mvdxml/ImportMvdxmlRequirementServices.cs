﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace arkitektum.dibk.mvdxml
{
    public class ImportMvdxmlRequirementServices
    {
        public Dictionary<string, string> ImportFile(Stream inputStream)
        {

            var mvdxmlConceptRoolDictionary = new Dictionary<string, string>();
            using (ExcelPackage package = new ExcelPackage(inputStream))
            {
                mvdxmlConceptRoolDictionary = ReadWorksheet(package, 1);
            }
            return mvdxmlConceptRoolDictionary;
        }
        private static Dictionary<string, string> ReadWorksheet(ExcelPackage package, int sheetId)
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets[sheetId];
            var mvdxmlConceptRoolDictionary = new Dictionary<string, string>();
            GetConceptsRulesFromWorkSheet(mvdxmlConceptRoolDictionary, worksheet);
            return mvdxmlConceptRoolDictionary;
        }

        private static void GetConceptsRulesFromWorkSheet(Dictionary<string, string> mvdxmlConcepstRulesDictionary, ExcelWorksheet worksheet)
        {
            for (int currentRow = 4; currentRow < worksheet.Dimension.Rows - 4; currentRow++)
            {
                //14 is the first column of categories in the excel worksheet in this case "P13 ebyggesak"
                if (RowIsP13(worksheet, currentRow, 14))
                {
                    mvdxmlConcepstRulesDictionary.Add(worksheet.Cells[currentRow, 6].Text, worksheet.Cells[currentRow, 12].Text);
                }
            }
        }

        private static bool RowIsP13(ExcelWorksheet worksheet, int row, int column)
        {
            return !string.IsNullOrEmpty(worksheet.Cells[row, column].Text);
        }
    }
}
