﻿using System;
using System.Collections.Generic;
using System.Linq;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine.Logger;
using arkitektum.dibk.validationengine.MvdXml;
using Serilog;
using Xbim.Common;
using Xbim.Ifc2x3.MeasureResource;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;


namespace arkitektum.dibk.validationengine
{
    public class CreateMvdxmlReportResults
    {
        private ILogger _logger;

        public CreateMvdxmlReportResults(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<CreateMvdxmlReportResults>();
        }

        public ReportResult ValidateEntityValue(RequirementsRequirement requirementsRequirement, IPersistEntity entity)
        {
            ConceptTestResult testResult = ConceptTestResult.DoesNotApply;
            // Obtain the definition in Norwegian and delete the additional definition of BIMQ in mvdxml
            requirementsRequirement.ParentConcept.Definitions = MvdxmlServices.GetDefinition(requirementsRequirement.ParentConcept.Definitions);
            //Get just the name of the concept
            requirementsRequirement.ParentConcept.name = MvdxmlServices.GetConceptName(requirementsRequirement.ParentConcept);

            try
            {
                if (requirementsRequirement.ParentConcept.TemplateRules.Items.Any())
                {
                    //  Check if requirment is mandatory
                    if (requirementsRequirement.requirement != RequirementsRequirementRequirement.mandatory && requirementsRequirement.requirement != RequirementsRequirementRequirement.recommended)
                    {
                        if (requirementsRequirement.requirement == RequirementsRequirementRequirement.notrelevant)
                            testResult = ConceptTestResult.Warning;
                        else
                            testResult = ConceptTestResult.DoesNotApply;
                    }
                    else
                    {
                        //Test entity with mvdxml
                        //TODO problem with IfcProject in IFC2x3
                        try
                        {
                            testResult = requirementsRequirement.Test(entity);
                        }
                        catch (Exception)
                        {
                            //using the old IFC2x3 standard which does not include "trueNorth" https://standards.buildingsmart.org/IFC/RELEASE/IFC2x3/TC1/HTML/ifcrepresentationresource/lexical/ifcrepresentationcontext.htmfile 
                            if (entity.ExpressType.ExpressName == "IfcProject")
                            {
                                var templateRule = requirementsRequirement.ParentConcept.TemplateRules.Items.FirstOrDefault();
                                if (templateRule?.GetType() == typeof(Xbim.MvdXml.TemplateRulesTemplateRule))
                                {
                                    var ruleParameters = ((TemplateRulesTemplateRule)templateRule).Parameters;
                                    if (ruleParameters.Contains("trueNorth"))
                                    {
                                        testResult = ConceptTestResult.Warning;
                                    }
                                }
                            }
                        }

                        if (testResult != ConceptTestResult.DoesNotApply)
                        {
                            //See if the entity needs to be controlled with REGEX
                            var regexControlTestResults = new RegexServices().EntityValueRegexCheck(entity, requirementsRequirement);
                            if (regexControlTestResults != null)
                                testResult = regexControlTestResults.FirstOrDefault().Value;
                        }
                    }
                    // See if the requirement is recommended, and set resul as warning not as feil, this is because the regex check
                    if (requirementsRequirement.requirement == RequirementsRequirementRequirement.recommended)
                    {
                        if (testResult == ConceptTestResult.Fail)
                        {
                            testResult = ConceptTestResult.Warning;
                        }
                    }
                }
                else
                {
                    testResult = requirementsRequirement.Test(entity);
                }
            }
            catch (Exception ex)
            {
                //bug i mvdxml eksport i bsguiden
                //TODO Control this catch, not used under normal conditions!, find out problem with IfcProject with IFC2x3
                var errorResult = new ReportResult(requirementsRequirement.ParentConcept, entity, ConceptTestResult.Fail, requirementsRequirement.GetExchangeRequirement());
                _logger.Error(ex, "Problem validating '{entity}' with ParentConcept:{parentConcept}", entity, requirementsRequirement.ParentConcept);
                return errorResult;
            }
            var result = new ReportResult(requirementsRequirement.ParentConcept, entity, testResult, requirementsRequirement.GetExchangeRequirement());
            return result;
        }
        public ReportResult CreateReportResultWithEntityToFilter(IPersistEntity entity)
        {

            var reportResult = new ReportResult(
                new Concept()
                {
                    //name = entity.ExpressType.Name,
                    name = "Kreves ikke",
                    Definitions = new DefinitionsDefinition[]
                    {
                        new DefinitionsDefinition()
                        {
                            Body = new DefinitionsDefinitionBody()
                            {
                                Value = "Sjekk om konseptet kan filteres bort før leveranse til byggesak"
                            }
                        }
                    }
                },
                entity,
                ConceptTestResult.DoesNotApply,
                new ModelViewExchangeRequirement() { name = "Konsept er ukjent for spesifikasjon" }
            );
            return reportResult;
        }
    }

}
