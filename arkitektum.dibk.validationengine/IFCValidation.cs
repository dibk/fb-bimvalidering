﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;
using System.IO;
using arkitektum.dibk.validationengine.Logger;
using arkitektum.dibk.validationengine.MvdXml;
using Serilog;
using Xbim.Ifc;
using System.Threading.Tasks;

namespace arkitektum.dibk.validationengine
{
    public class IFCValidation
    {
        private readonly ILogger _logger;
        private List<ReportResult> _reportResults;

        public IFCValidation(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<IFCValidation>();
        }

        public Dictionary<string, string> GetDefinition(ReportResult item)
        {

            if (item.Concept == null)
                return null;

            DefinitionsDefinition[] definitions = item.Concept.Definitions;

            string bodyValue = null;
            string bodyLang = null;
            if (definitions == null) return null;
            var definitionDefinitions = definitions.Where(d => !string.IsNullOrEmpty(d.Body.Value));
            var definitionsDefinitions = definitionDefinitions as DefinitionsDefinition[] ?? definitionDefinitions.ToArray();
            if (definitionsDefinitions.Any())
            {

                foreach (var definition in definitionsDefinitions.Where(d => !string.IsNullOrEmpty(d.Body.Value)))
                {
                    if (definition.Body.lang.ToLower() == "no")
                    {
                        bodyValue = definition.Body.Value;
                        bodyLang = definition.Body.lang;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(bodyValue))
                {
                    bodyValue = definitionsDefinitions[0].Body.Value;
                    var allDefinitions = definitionsDefinitions.Where(d => !string.IsNullOrEmpty(d.Body.Value));
                    var enumerable = allDefinitions as DefinitionsDefinition[] ?? allDefinitions.ToArray();
                    if (enumerable.Any())
                    {
                        var firstOrDefault = enumerable.FirstOrDefault();
                        if (firstOrDefault != null) bodyValue = firstOrDefault.Body.Value;
                    }
                }
            }
            //BIMQ export definition with extra data from Pset_ and Qto_, take away this extra definition 
            if (bodyValue != null)
            {
                var definitonIdex = bodyValue.IndexOf("[Definition from IFC]:", StringComparison.Ordinal);
                if (definitonIdex > 0)
                    bodyValue = bodyValue.Substring(0, definitonIdex - 2);
            }
            var newDefinitionsDefinitions = new[]
               {
                new DefinitionsDefinition()
                {
                    Body = new DefinitionsDefinitionBody()
                    {
                        lang = bodyLang ?? "",
                        Value = bodyValue ?? ""
                    }
                }
            };
            var definitionDictionary = new Dictionary<string, string>()
            {
                {"value",bodyValue },
                {"lang",bodyLang }
            };

            //return newDefinitionsDefinitions;
            return definitionDictionary;
        }



        public List<ReportResult> ValidateIfcModelWithMvdxml(string mvdpath, IfcReader ifcReader)
        {
            var validatedModel = new MvdxmlServices(_logger).MvdValidation(ifcReader.IfcModel, mvdpath);
            var reportResults = MvdValidationToReportResults(ifcReader.IfcModel, validatedModel);
            return reportResults;
        }

        /// <summary>
        /// Check mvdxml validation results with REGEX and Whitelist and generate a List of the Results
        /// </summary>
        /// <param name="ifcModel"></param>
        /// <param name="mvdxmlValidationResults"></param>
        /// <returns>List of validation results</returns>
        public List<ReportResult> MvdValidationToReportResults(IfcStore ifcModel, MvdEngine mvdxmlValidationResults)
        {
            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                //https://stackoverflow.com/a/17416410
                ConcurrentBag<ReportResult> resultList = new ConcurrentBag<ReportResult>();

                var basePath = AppDomain.CurrentDomain.BaseDirectory;
                List<string> whitelist = null;

                //Get all the concepts that mast be in the IFC model from a txt file 
                var ifcConceptsWhiteList = Path.Combine(basePath, "mvd\\Ifc4konseptList.csv");
                try
                {
                    if (!File.Exists(ifcConceptsWhiteList))
                    {
                        _logger.Error("Can't read Ifc4konseptList.csv");
                        return null;
                    }

                    whitelist = GetWhiteList(ifcConceptsWhiteList);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Reading Ifc4konseptList");
                    throw;
                }


                // https://docs.xbim.net/examples/using-linq-for-optimal-performance.html
                //Add entities to filte to the report
                var mvdEntitiesToFilter = ifcModel.Instances.Where(a => !whitelist.Any(e => a.ExpressType.ExpressName.Equals(e)))?.ToArray();
                if (mvdEntitiesToFilter.Any())
                {
                    Parallel.ForEach(mvdEntitiesToFilter, (entity) =>
                    {
                        var reportResult = new CreateMvdxmlReportResults(_logger).CreateReportResultWithEntityToFilter(entity);
                        resultList.Add(reportResult);
                    });
                }

                //Get IFC entities in mvdxml that have to be check
                var expressTypes = mvdxmlValidationResults.GetExpressTypes();
                var mvdEntitiesToCheck = ifcModel.Instances.Where(a => expressTypes.Any(e => a.ExpressType.ExpressName.Equals(e.ExpressName)))?.ToArray();
                if (mvdEntitiesToCheck.Any())
                {
                    Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();
                    Parallel.ForEach(mvdEntitiesToCheck, (entity) =>
                    {
                        //Get Requirements for entity
                        var thisEntityExpressType = entity.ExpressType;
                        var suitableRoots = mvdxmlValidationResults.GetConceptRoots(thisEntityExpressType);
                        var requirements = suitableRoots.SelectMany(c => c.Concepts.Where(s => s.Requirements.All(d => d.requirement != RequirementsRequirementRequirement.excluded)));

                        // create list report and control value with regex and mvdxml
                        var enumerable = requirements as Concept[] ?? requirements.ToArray();
                        if (enumerable.Any())
                        {
                            foreach (var concept in enumerable)
                            {
                                try
                                {
                                    if (ifcModel.Header.SchemaVersion == "IFC2X3" && concept.name.IndexOf("EPSG-koden", StringComparison.InvariantCultureIgnoreCase) >= 0)
                                    {
                                        concept.Requirements.First().requirement = RequirementsRequirementRequirement.notrelevant;
                                    }
                                    else
                                    {
                                        var requirementsRequirement = concept.Requirements.FirstOrDefault();
                                        var entityValueResult = new CreateMvdxmlReportResults(_logger).ValidateEntityValue(requirementsRequirement, entity);
                                        resultList.Add(entityValueResult);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex, "Problem Creating mvdxmlreport '{entity}' with ParentConcept:{parentConcept}", entity, concept.Requirements?.FirstOrDefault());
                                }
                            }
                        }
                    });
                    stopWatch.Stop();
                    _logger.Debug("{methodName} used {elapsedTime}", "ValidateEntityValue", stopWatch.ElapsedMilliseconds);
                }
                return resultList.ToList();
            }
        }

        private static List<string> GetWhiteList(string ifcConceptsWhiteList)
        {
            var whitelist = new List<string>();
            using (var reader = new StreamReader(ifcConceptsWhiteList))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line == null) continue;
                    var values = line.Split(';');
                    whitelist.Add(values[0]);
                }
            }
            return whitelist;
        }
    }
}


