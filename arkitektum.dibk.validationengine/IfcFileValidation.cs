﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xbim.Ifc;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;

namespace arkitektum.dibk.validationengine
{
    class IfcFileValidation
    {
        public List<ReportResult> Validate(Stream ifcDataFileStream, string mvdValidationFile, Xbim.IO.IfcStorageType fileExtension, Xbim.Common.Step21.IfcSchemaVersion schema)
        {
            var model = IfcStore.Open(ifcDataFileStream, Xbim.IO.IfcStorageType.Ifc, Xbim.Common.Step21.IfcSchemaVersion.Ifc4, XbimModelType.MemoryModel);
            var mvd = mvdXML.LoadFromFile(mvdValidationFile);
            var doc = new MvdEngine(mvd, model);

            var resultList = new List<ReportResult>();



            return resultList;
        }
    }
}
