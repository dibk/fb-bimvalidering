﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace arkitektum.dibk.validationengine.IfcGeoRef.Models
{
    public class GeoRefModel
    {
        public bool GeoRef10 { get; set; }
        public bool GeoRef20 { get; set; }
        public bool GeoRef30 { get; set; }
        public bool GeoRef40 { get; set; }
        public bool GeoRef50 { get; set; }
    }
}