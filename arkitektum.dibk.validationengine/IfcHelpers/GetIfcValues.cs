﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xbim.Common;
using Xbim.Ifc2x3.Kernel;
using Xbim.Ifc4.Interfaces;
using Xbim.Ifc4.Kernel;
using Xbim.Ifc4.QuantityResource;

namespace arkitektum.dibk.validationengine.IfcHelpers
{
    public class GetIfcValues
    {
        public static string Pset_StringValue(IPersistEntity entity, string psetName, string propertyName)
        {
            try
            {
                if (entity.Model.Header.SchemaVersion == "IFC2X3")
                {
                    if (((Xbim.Ifc2x3.Kernel.IfcObject)entity).GetPropertySingleValue(psetName, propertyName) != null)
                    {
                        var Pset2x3 = ((Xbim.Ifc2x3.Kernel.IfcObject)entity).GetPropertySingleValue(psetName, propertyName);
                        return Pset2x3.NominalValue.ToString();
                    }
                    return null;
                }
                else
                {
                    if (((Xbim.Ifc4.Kernel.IfcObject)entity).GetPropertySingleValue(psetName, propertyName) != null)
                    {
                        var Pset4 = ((Xbim.Ifc4.Kernel.IfcObject)entity).GetPropertySingleValue(psetName, propertyName);
                        return Pset4.NominalValue.ToString();
                    }
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static string Qto_Area(IPersistEntity entity, string quantityPsetName, string quantityName)
        {
            try
            {
                if (entity.Model.Header.SchemaVersion == "IFC2X3")
                {
                    var qtoElementPhysicalSimpleQuantity = ((Xbim.Ifc2x3.Kernel.IfcObject)entity).GetElementPhysicalSimpleQuantity(quantityPsetName, quantityName);
                    return ((Xbim.Ifc2x3.QuantityResource.IfcQuantityArea)qtoElementPhysicalSimpleQuantity)?.AreaValue.ToString();
                }
                else
                {
                    var qtoElementPhysicalSimpleQuantity =((Xbim.Ifc4.Kernel.IfcObject)entity).GetElementPhysicalSimpleQuantity(quantityPsetName,quantityName);
                    var areaValue = ((IfcQuantityArea) qtoElementPhysicalSimpleQuantity)?.AreaValue.ToString();
                    return areaValue;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<ClassificationReference> GetIfcClassification(IPersistEntity entit, List<ClassificationReference> classificationReferencesList)
        {
            if (entit.Model.Header.SchemaVersion == "IFC2X3")
            {
                IfcRealAssociatesClassicationToclassificationReferencesListIfc2X3(entit, classificationReferencesList);
            }
            else
            {
                IfcRealAssociatesClassicationToclassificationReferencesListIfc4(entit, classificationReferencesList);
            }
            return classificationReferencesList;
        }
        internal static List<ClassificationReference> GetIfcBuildingStoreyIfcSpaceClassification(IIfcBuildingStorey ifcBuildingStorey, List<ClassificationReference> classificationReferencesList)
        {
            foreach (var ifcspace in ifcBuildingStorey.IsDecomposedBy.SelectMany(r => r.RelatedObjects.Where(d => d.ExpressType.ToString() == "IfcSpace")))
            {
                GetIfcClassification(ifcspace, classificationReferencesList);
            }
            return classificationReferencesList;
        }

        private static void IfcRealAssociatesClassicationToclassificationReferencesListIfc2X3(IPersistEntity entity, List<ClassificationReference> classificationReferencesList)
        {
            var allRealAssociatesClassification = RealAssociatesClassificationIfc2X3(entity);
            if (allRealAssociatesClassification == null)
            {
                return;
            }
            else
            {
                foreach (Xbim.Ifc2x3.Kernel.IfcRelAssociatesClassification ifcRelAssociatesClassification in allRealAssociatesClassification)
                {
                    var classificationReference = ((Xbim.Ifc2x3.ExternalReferenceResource.IfcClassificationReference)ifcRelAssociatesClassification.RelatingClassification);
                    classificationReferencesList.Add(new ClassificationReference()
                    {
                        EntityLabel = entity.EntityLabel,
                        RelAssociatesClassificationName_Type = ifcRelAssociatesClassification.Name,
                        ClassificationReferenceIdentification_Code = classificationReference.ItemReference,
                        ClassificationReferenceName_Name = classificationReference.Name
                    });
                }
            }
        }
        internal static IEnumerable<Xbim.Ifc2x3.Kernel.IfcRelAssociates> RealAssociatesClassificationIfc2X3(IPersistEntity entity)
        {
            try
            {
                var allRealAssociatesClassification = ((Xbim.Ifc2x3.Kernel.IfcObjectDefinition)entity).HasAssociations.Where(d => d.GetType().Name == "IfcRelAssociatesClassification");
                return allRealAssociatesClassification;
            }
            catch
            {
                return null;
            }
        }
        private static void IfcRealAssociatesClassicationToclassificationReferencesListIfc4(IPersistEntity entity, List<ClassificationReference> classificationReferencesList)
        {
            var allRealAssociatesClassification = RealAssociatesClassificationIfc4(entity);
            if (allRealAssociatesClassification == null)
            {
                return;
            }
            else
            {
                foreach (Xbim.Ifc4.Kernel.IfcRelAssociatesClassification ifcRelAssociatesClassification in allRealAssociatesClassification)
                {
                    var classificationReference = ((Xbim.Ifc4.ExternalReferenceResource.IfcClassificationReference)ifcRelAssociatesClassification.RelatingClassification);
                    classificationReferencesList.Add(new ClassificationReference()
                    {
                        EntityLabel = entity.EntityLabel,
                        RelAssociatesClassificationName_Type = ifcRelAssociatesClassification.Name,
                        ClassificationReferenceIdentification_Code = classificationReference.Identification,
                        ClassificationReferenceName_Name = classificationReference.Name
                    });
                }
            }
        }
        internal static IEnumerable<Xbim.Ifc4.Kernel.IfcRelAssociates> RealAssociatesClassificationIfc4(IPersistEntity entity)
        {
            try
            {
                var allRealAssociatesClassification = ((Xbim.Ifc4.Kernel.IfcObjectDefinition)entity).HasAssociations.Where(d => d.GetType().Name == "IfcRelAssociatesClassification");
                return allRealAssociatesClassification;
            }
            catch
            {
                return null;
            }
        }

        //TODO: get all IfcSpace conttained in IfcSpace/IfcZone
        public static Dictionary<string, string> GetIfcSpaceContained(Xbim.Ifc4.Interfaces.IIfcSpace ifcSpace, Dictionary<string, string> RelatedIfcSpaceDictionary)
        {
            var ifcspace1 = ifcSpace.IsDecomposedBy.SelectMany(r => r.RelatedObjects.Where(d => d.ExpressType.ToString() == "IfcSpace"));
            //var allIfcBuildingStorey = ifcModel.Instances.OfType<Xbim.Ifc4.Interfaces.IIfcBuildingStorey>();
            return RelatedIfcSpaceDictionary;
        }
    }
}
