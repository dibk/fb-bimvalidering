﻿using System.Collections.Generic;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;

namespace arkitektum.dibk.validationengine.IfcHelpers
{
    public class IfcRelAssociatesClassificationFilter
    {
        private readonly List<ClassificationReference> _classificationReferences;
        private readonly IEnumerable<ClassificationReference> _classificationReferencesFilter;
        public IfcRelAssociatesClassificationFilter(List<ClassificationReference> classificationReferences)
        {
            _classificationReferences = classificationReferences;
        }
        public IEnumerable<ClassificationReference> GetByClassType(string classType)
        {
            var classificationReferencesNotNull = _classificationReferences.Where(c => c.RelAssociatesClassificationName_Type != null);
            var noko = classificationReferencesNotNull.Where(c => c.RelAssociatesClassificationName_Type.Contains(classType));

            return noko;
        }
        public string GetByClassTypeToString(string classType)
        {
            string ClassRefClassIdentificationString = null;
            var classTypeAny = _classificationReferences.Where(c => c.RelAssociatesClassificationName_Type != null && c.RelAssociatesClassificationName_Type.Contains(classType));
            if (classTypeAny.Any())
            {
                ClassRefClassIdentificationString = classTypeAny.FirstOrDefault().ClassificationReferenceIdentification_Code;
            }
            return ClassRefClassIdentificationString;
        }
        public IEnumerable<ClassificationReference> GetByTypeAndIdentification(string classType, string calssIdentification)
        {
            var classTypEnumerable = GetByClassType(classType);
            IEnumerable<ClassificationReference> classByTypeAndIdentification = null;
            if (classTypEnumerable.Any())
            {
                classByTypeAndIdentification =GetIEnumerableClassByIdentification(classTypEnumerable, calssIdentification);
            }
            return classByTypeAndIdentification;
        }
        private IEnumerable<ClassificationReference> GetIEnumerableClassByIdentification(IEnumerable<ClassificationReference> classTypEnumerable, string calssIdentification)
        {
            return classTypEnumerable.Where(c => c.ClassificationReferenceIdentification_Code == calssIdentification); 
        }
        public string CountByTypeAndIdentification(string classType, string calssIdentification)
        {
            var classTypEnumerable = GetByClassType(classType);     
            IEnumerable<ClassificationReference> classByTypeAndIdentification = null;
            string CountOfcClassByTypeAndIdentification = null;
            if (classTypEnumerable.Any())
            {
                classByTypeAndIdentification = GetIEnumerableClassByIdentification(classTypEnumerable, calssIdentification);
                if (classByTypeAndIdentification!= null)
                {
                    CountOfcClassByTypeAndIdentification = classByTypeAndIdentification.Count().ToString();
                }
            }
            return CountOfcClassByTypeAndIdentification;
        }
    }

    public class ClassificationReference
    {
        public int EntityLabel { get; set; }
        public string RelAssociatesClassificationName_Type { get; set; }
        public string ClassificationReferenceIdentification_Code { get; set; }
        public string ClassificationReferenceName_Name { get; set; }
    }
}
