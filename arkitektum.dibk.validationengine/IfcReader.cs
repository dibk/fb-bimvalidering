﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using arkitektum.dibk.validationengine.Logger;
using Xbim.Ifc;
using Xbim.Ifc4.Interfaces;
using Serilog;

namespace arkitektum.dibk.validationengine
{
    public class IfcReader
    {
        private readonly ILogger _logger;
        public  IfcStore IfcModel { get; set; }
        public  string IfcSchema { get; set; }
        public  string IfcViewDefinition { get; set; }
        public  string IfcProjectGuid { get; set; }

        public IfcReader(ILogger logger, string path)
        {
            _logger = logger;
            _logger = _logger.ForContext<IfcReader>();
            OpenModel(path);
        }

        /// <summary>
        /// Open model with Stream File type
        /// </summary>
        /// <param name="ifcDataFileStream">Ifc Stream data file to be validated</param>
        /// <param name="ifcStorageType">Type of data (*.ifc, *.ifcxml, *.ifczip)</param>
        /// <param name="schema">IFC schema (IFC2x3, IFC4). Other schemas are not supported by this class.</param>
        /// <returns>IfcStore</returns>
        //public IfcStore OpenModel(Stream ifcDataFileStream, IfcStorageType ifcStorageType, IfcSchemaVersion schema)
        //{
        //    try
        //    {
        //        var model = IfcStore.Open(ifcDataFileStream, ifcStorageType, schema, XbimModelType.MemoryModel);
        //        ifcDataFileStream.Close();
        //        return model;
        //    }
        //    catch (Exception streaException)
        //    {
        //        throw new Exception("kan ikke åpne modellen", streaException);
        //    }
        //}
        //***
        public void Dispose()
        {
            IfcModel.Dispose();
        }
        /// <summary>
        /// Open _ifcModel with file
        /// </summary>
        /// <param name="ifcDataFile"></param>
        /// <returns>IfcStore</returns>
        public IfcStore OpenModel(string ifcDataFile)
        {
            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                try
                {
                    //var _ifcModel = IfcStore.Open(ifcDataFile);
                    IfcModel = IfcStore.Open(ifcDataFile, null, 0, (pct, o) => { });
                    var instancesCount = IfcModel?.Instances?.Count;
                    _logger.Debug("IfcModel ifcSchema:{ifcSchema} viewDefinition:[{ifcViewDefinition}]", GetIfcSchema(), GetIfcViewDefinition());
                    GetIfcProjectGuid();
                    return IfcModel;
                }
                catch (Exception fileException)
                {
                    _logger.Error(fileException, "Xbim can't open IfcModel");
                    throw new Exception("kan ikke åpne modellen", fileException);
                }
            }
        }
        public string GetIfcSchema()
        {
            IfcSchema = IfcModel.SchemaVersion.ToString();
            return IfcSchema;
        }
        public string GetIfcViewDefinition()
        {
            var headerHaveViewDefinition = IfcModel.Header.FileDescription.Description.Any(d => d.Contains("ViewDefinition"));
            if (headerHaveViewDefinition)
            {
                var viewDefinition = IfcModel.Header.FileDescription.Description.First(d => d.Contains("ViewDefinition"));
                var index = viewDefinition.LastIndexOf("ViewDefinition [", StringComparison.Ordinal) + "ViewDefinition [".Length;
                var index2 = viewDefinition.IndexOf("]", index, StringComparison.Ordinal);
                IfcViewDefinition = viewDefinition.Substring(index, index2 - index);
            }
            return IfcViewDefinition;
        }
        public string GetIfcProjectGuid()
        {
            IfcProjectGuid = IfcModel.Instances.OfType<IIfcProject>().FirstOrDefault()?.GlobalId.ToString();

            if (IfcSchema == "Ifc4")
                IfcProjectGuid = IfcModel.Instances.FirstOrDefault<Xbim.Ifc4.Kernel.IfcProject>().GlobalId.ToString();
            else if (IfcSchema == "Ifc2x3")
                IfcProjectGuid = IfcModel.Instances.FirstOrDefault<Xbim.Ifc2x3.Kernel.IfcProject>().GlobalId.ToString();

            return IfcProjectGuid;
        }

        /// <summary>
        /// Returns the first IfcProject entity.
        /// Valid files should only contain one IfcProject.
        /// </summary>
        public IIfcProject ProjReader()
        {
            var res = IfcModel.Instances.OfType<IIfcProject>().ToList();

            _logger.Information("IFC reader: Projects found = " + res.Count);

            if (res.Count == 0)
                _logger.Error("IFC reader: No IfcProject present. IFC file is NOT valid.");

            if (res.Count > 1)
                _logger.Error("IFC reader: More than one IfcProject present. IFC file is NOT valid.");

            return res.FirstOrDefault();
        }
        /// <summary>
        /// Returns the first IfcSite entity.
        /// Files according IFC implementers agreements should contain only one IfcSite.
        /// </summary>
        public IIfcSite SiteReader()
        {
            var res = IfcModel.Instances.OfType<IIfcSite>().ToList();

            _logger.Information("IFC reader: Sites found = " + res.Count);

            if (res.Count == 0)
                _logger.Warning("IFC reader: No IfcSite present. IFC file is valid but violated implementation agreements. Some LoGeoRef could not be checked.");

            if (res.Count > 1)
                _logger.Warning("IFC reader: More than one IfcSite present. This application supports currently only one (the first) site.");

            return res.FirstOrDefault();
        }
        /// <summary>
        /// Returns the first IfcBuilding entity.
        /// More than one IfcBuilding in one file is not in scope yet.
        /// </summary>
        public IIfcBuilding BldgReader()
        {
            var res = IfcModel.Instances.OfType<IIfcBuilding>().ToList();

            _logger.Information("IFC reader: Buildings found = " + res.Count());

            if (res.Count == 0)
                _logger.Error("IFC reader: No IfcBuilding present. IFC file is valid but violated implementation agreements.");

            if (res.Count > 1)
                _logger.Warning("IFC reader: More than one IfcBuilding present. This application supports currently only one (the first) building.");

            return res.FirstOrDefault();
        }
        /// <summary>
        /// Returns a list of from IfcProduct inherited entities with global placement.
        /// Valid IFC-files should contain at least one, the IfcSite entity.
        /// </summary>
        public List<IIfcProduct> UpperPlcmProdReader()
        {
            var res = IfcModel.Instances.Where<IIfcLocalPlacement>(e => e.PlacementRelTo == null)
                .SelectMany(e => e.PlacesObject).ToList();

            _logger.Information("IFC reader: Objects with global placement found = " + res.Count());

            if (res.Count == 0)
                _logger.Error("IFC reader: No products with global placement present. IFC file is NOT valid.");

            return res;
        }
        /// <summary>
        /// Returns the first IfcMapConversion entity which is connected to the project`s context.
        /// Valid IFC-files reference only one such IfcMapConversion.
        /// </summary>
        public IIfcMapConversion MapReader(IIfcGeometricRepresentationContext ctx)
        {
            var map = IfcModel.Instances.OfType<IIfcMapConversion>().Where(m => m.SourceCRS == ctx).ToList();

            _logger.Information("IFC reader: MapConversion with connection to project found = " + map.Count());

            if (map.Count > 1)
                _logger.Error("IFC reader: More than one IfcMapConversion connected to IfcProject present. IFC file is valid but violated implementation agreements.");

            return map.FirstOrDefault();
        }

        /// <summary>
        /// Returns the first IfcPropertySet with "ProjectedCRS" in its name.
        /// If applied, there should be only one such entity.
        /// </summary>
        public IIfcPropertySet PSetReaderCRS()
        {
            var pset = IfcModel.Instances.OfType<IIfcPropertySet>()
                .Where(p => p.Name.ToString().Contains("ProjectedCRS")).ToList();

            _logger.Information("IFC reader: PsetCRS found = " + pset.Count);

            if (pset.Count > 1)
                _logger.Warning("IFC reader: More than one PropertySet for ProjectedCRS present. IFC file is valid but information could be redundant.");

            return pset.FirstOrDefault();
        }

        /// <summary>
        /// Returns the first IfcPropertySet with "MapConversion" in its name.
        /// If applied, there should be only one such entity.
        /// </summary>
        public IIfcPropertySet PSetReaderMap()
        {
            var pset = IfcModel.Instances.OfType<IIfcPropertySet>()
                .Where(p => p.Name.ToString().Contains("MapConversion")).ToList();

            _logger.Information("IFC reader: PsetMapConversion found = " + pset.Count());

            if (pset.Count > 1)
                _logger.Warning("IFC reader: More than one PropertySet for MapConversion present. IFC file is valid but information could be redundant.");

            return pset.FirstOrDefault();
        }
        /// <summary>
        /// Returns a list of IfcGeometricRepresentationContext entities.
        /// Valid IFC-files should contain at least one context for model view.
        /// Optionally there could be another context for plan view.
        /// </summary>
        public List<IIfcGeometricRepresentationContext> ContextReader(IIfcProject proj)
        {
            var allCtx = proj.RepresentationContexts.OfType<IIfcGeometricRepresentationContext>();  //includes also inherited SubContexts (not necessary for this application)

            var noSubCtx = allCtx.Where(ctx => ctx.ExpressType.ToString() != "IfcGeometricRepresentationSubContext").ToList(); //avoid subs (unneccessary overhead)

            _logger.Information("IFC reader: GeometricRepresentationContext found = " + noSubCtx.Count());

            if (noSubCtx.Count == 0)
                _logger.Error("IFC reader: No IfcGeometricRepresentationContext present. IFC file is NOT valid.");

            if (noSubCtx.Count > 2)
                _logger.Error("IFC reader: More than two IfcGeometricRepresentationContext present. IFC file is NOT valid.");

            return noSubCtx;
        }
    }
}
