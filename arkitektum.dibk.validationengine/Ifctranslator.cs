﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arkitektum.dibk.validationengine
{
    public static class IfcTranslator
    {
        public static DateTime GetDateTimeFromReportResul(ReportResult reportResult)
        {
            DateTime dateTime = DateTime.Today;
            if (reportResult?.Entity?.Model?.Header?.TimeStamp != null)
            {
                dateTime = Convert.ToDateTime(reportResult.Entity.Model.Header.TimeStamp);
            }

            return dateTime;
        }

        public static string GetReferenceLink(ReportResult reportResult)
        {
            var referenceLink = string.Empty;

            if (reportResult?.Entity?.ExpressType?.ExpressName != null)
                referenceLink = reportResult.Entity.ExpressType.ExpressName;

            return referenceLink;
        }

        public static string GetEntityName(ReportResult reportResult)
        {
            var entityName = string.Empty;

            if (reportResult?.Entity?.ExpressType?.Name != null)
                entityName = reportResult.Entity.ExpressType.Name;

            return entityName;
        }
    }
}
