﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Serilog;
using StackTrace = System.Diagnostics.StackTrace;
using Stopwatch = System.Diagnostics.Stopwatch;

namespace arkitektum.dibk.validationengine.Logger
{
    public class ValidationPerfTimerLogger : IDisposable
    {
        private readonly string _message;
        private readonly ILogger _logger;// = Log.ForContext<ValidationPerfTimerLogger>();
        private readonly string _methodName;
        private Stopwatch _timer;

        public ValidationPerfTimerLogger(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<ValidationPerfTimerLogger>();
            _methodName = new StackTrace().GetFrame(1).GetMethod().Name;
            this._timer = new Stopwatch();
            this._timer.Start();
        }

        public ValidationPerfTimerLogger(string message)
        {
            this._timer = new Stopwatch();
            this._timer.Start();
            this._message = message;
        }

        public void Dispose()
        {
            this._timer.Stop();
            var elapsedTime = this._timer.ElapsedMilliseconds;

            if (string.IsNullOrEmpty(_message))
                _logger.Debug("{methodName} used {elapsedTime}", _methodName, elapsedTime);
            else
                _logger.Debug("{methodName} used {elapsedTime} - {detailedMessage}", _methodName, elapsedTime, _message);
        }

        public void AddMessage(string message)
        {
            _logger.Debug("{methodName} - {message}", _methodName, message);
        }

        public void AddMessages(Dictionary<string, string> messageDictionary)
        {
            foreach (var fields in messageDictionary)
            {
                string message = "{methodName} - {"+$"{fields.Key}"+"}";
                _logger.Debug(message, _methodName, fields.Value);
            }
        }
    }
}