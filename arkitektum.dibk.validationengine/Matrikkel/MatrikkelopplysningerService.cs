﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
//using no.kxml.skjema.dibk.matrikkelregistrering;
using arkitektum.dibk.validationengine;
using arkitektum.dibk.validationengine.IfcHelpers;
using Serilog;

namespace arkitektum.dibk.validationengine.Matrikkel
{
    public class MatrikkelopplysningerService
    {
        private readonly ILogger _logger;
        public MatrikkelopplysningerService(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<MatrikkelopplysningerService>();
        }
        public MatrikkelregistreringType GetMatrikkelregistreringType(string path)
        {
            var matrikkelregistrering = new MatrikkelregistreringType();

            using (var ifcModel = new IfcReader(_logger,path).IfcModel)
            {
                // **Eiendom/Byggested**
                var ifcSite = ifcModel.Instances.FirstOrDefault<Xbim.Ifc4.Interfaces.IIfcSite>();

                // *Eiendomsinformas [knr-gnr/bnr/fnr/snr]

                matrikkelregistrering.eiendomsidentifikasjon = ifcSite.LandTitleNumber !=null? MatrikkelregistreringEiendom.GetMatrikkelnummeretRegex(ifcSite.LandTitleNumber.Value):null;

                // *Adresse til eiendom/byggested - IfcSite.SiteAddress
                matrikkelregistrering.adresse = ifcSite.SiteAddress != null ? MatrikkelregistreringEiendom.GetSiteAddress(ifcSite.SiteAddress) : null;

                // **Bygningsopplysninger**
                var ifcBuildings = ifcModel.Instances.OfType<Xbim.Ifc4.Interfaces.IIfcBuilding>();
                var bygningtypes = new List<BygningType>();
                foreach (var ifcBuilding in ifcBuildings)
                {
                   var bygningType = new BygningType();

                    // *bygningsnummer
                    bygningType.bygningsnummer = GetIfcValues.Pset_StringValue(ifcBuilding, "Pset_BuildingCommon", "BuildingID");

                    // *bebygdAreal [BYA]
                    //bygningBYA = GetIfcValues.Qto_Area(ifcBuilding, "Qto_BuildingBaseQuantities", "FootprintArea");
                    bygningType.bebygdAreal = GetIfcValues.Pset_StringValue(ifcBuilding, "Pset_BuildingCommon", "GrossPlannedArea");
                    // *bygningstype
                    string bygningstype = GetIfcValues.Pset_StringValue(ifcBuilding, "Pset_BuildingCommon", "OccupancyType");
                    bygningType.bygningstype = string.IsNullOrEmpty(bygningstype) ? null : MatrikkelregistreringBygning.BygningTypeKodeType(bygningstype);



                    // Get all classification from IfcBuilding and crate a list
                    List<ClassificationReference> ifcBuildingClassificationReferences = new List<ClassificationReference>();
                    GetIfcValues.GetIfcClassification(ifcBuilding, ifcBuildingClassificationReferences);
                    var eiendomRelAssociatesClassificationFilter = new IfcRelAssociatesClassificationFilter(ifcBuildingClassificationReferences);

                    bygningType.naeringsgruppe = ifcBuildingClassificationReferences.Any() ? MatrikkelregistreringBygning.GetNaeringsgruppe(eiendomRelAssociatesClassificationFilter) : null;



                    // **Estajer**

                    var allIfcBuildingStorey = ifcModel.Instances.OfType<Xbim.Ifc4.Interfaces.IIfcBuildingStorey>();
                    var etasjeTypes = new List<EtasjeType>();
                    foreach (Xbim.Ifc4.Interfaces.IIfcBuildingStorey ifcBuildingStorey in allIfcBuildingStorey)
                    {
                       var etasjetype = new EtasjeType();

                        //TODO get opplysingen from ifc file can be use Classification
                        // etasje opplysingen type - predefined default value
                        etasjetype.etasjeopplysning = BygningsOpplysningType.Ny;

                        // REGEX to get Etasjeplan and Estasjenummer
                        var etasjePlanNummerTemp = GetEtasjePlanNummerLoepenummerREGEX(ifcBuildingStorey.LongName.ToString());
                        var estajePlanNumer = etasjePlanNummerTemp ?? new[] { "", "" };
                        // Etasjeplan
                        etasjetype.etasjeplan = GetKodeType(estajePlanNumer[0]);
                        // Estasjenummer
                        etasjetype.etasjenummer = estajePlanNumer[1];
                        // Total Bruksareal (BRA) per etasje 
                        var etasjeBRA = GetIfcValues.Pset_StringValue(ifcBuildingStorey, "Pset_BuildingStoreyCommon", "NetPlannedArea");
                        etasjetype.bruksarealTotalt = ParceValueToDouble(etasjeBRA);
                        // Total Bruttoareal (BTA) per etasje
                        var etasjeBTA = GetIfcValues.Pset_StringValue(ifcBuildingStorey, "Pset_BuildingStoreyCommon", "GrossPlannedArea");
                        etasjetype.bruttoarealTotalt = ParceValueToDouble(etasjeBTA);

                        // Get all classification in IfcBuildingStorey
                        List<ClassificationReference> ifcBuildingStoreyClassificationReferencesList = new List<ClassificationReference>();
                        GetIfcValues.GetIfcClassification(ifcBuildingStorey, ifcBuildingStoreyClassificationReferencesList);
                        var ifcBuildingSoreyAssociatesClassificationFilter = new IfcRelAssociatesClassificationFilter(ifcBuildingStoreyClassificationReferencesList);

                        // Get all classification from IfcSpace that are inclue in the IfcBuldingStorey
                        List<ClassificationReference> ifcSpaceClassificationReferencesList = new List<ClassificationReference>();
                        GetIfcValues.GetIfcBuildingStoreyIfcSpaceClassification(ifcBuildingStorey, ifcSpaceClassificationReferencesList);
                       
                        // TODO implement: IfcRelAssociatesClassificationFilter
                        var ifcSpaceAssociatesClassificationFilter = new IfcRelAssociatesClassificationFilter(ifcSpaceClassificationReferencesList);

                        // *Etasjeopplysning
                        etasjetype.etasjeopplysning = ifcBuildingStoreyClassificationReferencesList.Any() ? MatrikkelregistreringEtasjer.GetBygningsOpplysningType(ifcSpaceAssociatesClassificationFilter) : BygningsOpplysningType.Ny;
                        etasjeTypes.Add(etasjetype);
                    }

                    // *Add etasje to Bygnign
                    bygningType.etasjer = etasjeTypes.ToArray();

                    //**Bruksenheter**
                    var allZones = ifcModel.Instances.OfType<Xbim.Ifc4.Interfaces.IIfcZone>();
                    var bruksenhetTypes = new List<BruksenhetType>();
                    
                    foreach (var ifcZone in allZones)
                    {
                        // control if the IfcZone is "bruksenhet"
                        string referenceValue = GetIfcValues.Pset_StringValue(ifcZone, "Pset_ZoneCommon", "Reference");
                        if (string.IsNullOrEmpty(referenceValue) | (referenceValue!=null && referenceValue.ToLower() != "bruksenhet"))
                            break;

                       var bruksenhettype = new BruksenhetType();

                        //TODO get opplysingen from ifc file can be use Classification
                        // bruksenhet opplysingen type - predefined default value
                        bruksenhettype.boligOpplysning= BygningsOpplysningType.Ny;

                        // Set Bruksenhetnummer Etasjeplan, etasjenummer, løpenumer
                        bruksenhettype.bruksenhetsnummer = MatrikkelregistreringBruksenheter.GetBruksenhetsnummer(ifcZone.LongName.ToString());
                        // Bruksareal (BRA)m²
                        string referenceValue1 = GetIfcValues.Pset_StringValue(ifcZone, "Pset_ZoneCommon", "NetPlannedArea");
                        bruksenhettype.bruksareal = string.IsNullOrEmpty(referenceValue1) ? 0 : ParceValueToDouble(referenceValue1);


                        // get all classification in IfcZone
                        List<ClassificationReference> ifcZoneClassificationReferencesList = new List<ClassificationReference>();
                        GetIfcValues.GetIfcClassification(ifcZone, ifcZoneClassificationReferencesList);
                        var ifcZoneAssociatesClassificationFilter = new IfcRelAssociatesClassificationFilter(ifcZoneClassificationReferencesList);


                        // TODO get all IfcSpace in ifcZone to get any WC, room, bad all this specifies in ifcClassification to the IfcSpace 
                        //**Bruksenheter**

                        //string bruksenhetOpplysningString = ifcZoneAssociatesClassificationFilter.GetByClassTypeToString("BygningsOpplysningType");
                        //if (ifcZoneClassificationReferencesList.Any())
                        //{
                        //    // Antall wc
                        //    var allbruksenhetTypeWc = ifcZoneAssociatesClassificationFilter.GetByTypeAndIdentification("Bruksenhetstype", "WC");
                        //    bruksenhetTypes[i].antallBad = allbruksenhetTypeWc.Any() ? allbruksenhetTypeWc.Count().ToString() : null;

                        //    var allBruksenhetstype = ifcZoneAssociatesClassificationFilter.GetByClassType("Bruksenhetstype");
                        //}

                        bruksenhetTypes.Add(bruksenhettype);
                    }
                    // Add bruksenhet til bygning
                    bygningType.bruksenheter = bruksenhetTypes.ToArray();
                    bygningtypes.Add(bygningType);
                }
                // Add all BygningType to matrikkelregister
                matrikkelregistrering.bygning = bygningtypes.ToArray();
            }
            return matrikkelregistrering;
        }

        private static double ParceValueToDouble(string value)
        {
            double valueDoble;
            double.TryParse(value, out valueDoble);
            return valueDoble;
        }

        public KodeType GetKodeType(string kodeVerdi, string kodebeskrivelse = "")
        {
            var kodeType = new KodeType()
            {
                kodeverdi = kodeVerdi,
                kodebeskrivelse = kodebeskrivelse
            };
            return kodeType;
        }

        public static string[] GetEtasjePlanNummerLoepenummerREGEX(string bruksenhetsnummer)
        {
            Regex pattern = new Regex(@"(?<etasjeplan>[H,h,K,k,L,l,U,u])(?<etasjenummer>[0-9][1-9])(?<loepenummer>[0-9][1-9])?$");
            Match match = pattern.Match(bruksenhetsnummer);
            if (!match.Success) return null;
            var estasjePlanAndNumer = new string[3];
            estasjePlanAndNumer[0] = match.Groups["etasjeplan"].Value;
            estasjePlanAndNumer[1] = match.Groups["etasjenummer"].Value;
            estasjePlanAndNumer[2] = match.Groups["loepenummer"].Value;
            return estasjePlanAndNumer;
        }

        public static string GetEtasjerPlanBeskrivelse(string kode)
        {
            switch (kode)
            {
                case "H":
                    return "Hovedetasje";
                case "K":
                    return "Kjeller";
                case "L":
                    return "Loft";
                case "U":
                    return "Underetasje";
                default:
                    return "-";
            }
        }
    }
}

