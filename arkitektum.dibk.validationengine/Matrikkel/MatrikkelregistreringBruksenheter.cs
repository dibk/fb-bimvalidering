﻿//using no.kxml.skjema.dibk.matrikkelregistrering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace arkitektum.dibk.validationengine.Matrikkel
{
    public class MatrikkelregistreringBruksenheter
    {
        public static BruksenhetsnummerType GetBruksenhetsnummer(string bruksenhetsnummer)
        {
            var bruksenhetsnummerArray =MatrikkelopplysningerService.GetEtasjePlanNummerLoepenummerREGEX(bruksenhetsnummer);
            if (bruksenhetsnummerArray == null)
                return new BruksenhetsnummerType()
                {
                    etasjeplan = new KodeType()
                    {
                        kodeverdi = "",
                        kodebeskrivelse = ""
                    },
                    etasjenummer = "",
                    loepenummer = ""
                };

            return new BruksenhetsnummerType()
            {
                etasjeplan = new KodeType()
                {
                    kodeverdi = bruksenhetsnummerArray[0],
                    kodebeskrivelse = MatrikkelopplysningerService.GetEtasjerPlanBeskrivelse(bruksenhetsnummerArray[0])
                },
                etasjenummer = bruksenhetsnummerArray[1],
                loepenummer = bruksenhetsnummerArray[2],
            };
        }

       public static string GetBruksenhetKodeBeskrivelse(string bruksenhetKode)
        {
            switch (bruksenhetKode)
            {
                case "A":
                    return "Annet enn Bolig";
                case "B":
                    return "Bolig";
                case "F":
                    return "Fritidsbolig";
                case "I":
                    return "Ikke godkjent bolig";
                case "U":
                    return "Unummerert bruksenhet";
            }
            return null;
        }
    }
}
