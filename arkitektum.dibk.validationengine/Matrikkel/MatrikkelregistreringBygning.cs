﻿using System;
using System.Collections.Generic;
using System.IO;
using arkitektum.dibk.validationengine.IfcHelpers;
//using no.kxml.skjema.dibk.matrikkelregistrering;


namespace arkitektum.dibk.validationengine.Matrikkel
{
    public class MatrikkelregistreringBygning
    {

        // naeringsgruppe by classification
        public static KodeType GetNaeringsgruppe(IfcRelAssociatesClassificationFilter eiendomRelAssociatesClassificationFilter)
        {

            string naeringsgruppeString = eiendomRelAssociatesClassificationFilter.GetByClassTypeToString("Naeringsgruppe");
            KodeType naeringsgruppe = string.IsNullOrEmpty(naeringsgruppeString) ? null : naeringsgruppeKodeType(naeringsgruppeString);
            return naeringsgruppe;
        }

        public static KodeType naeringsgruppeKodeType(string naeringsgruppeString)
        {
            Dictionary<string, string> naeringsgruppeDictionary = NaeringsgruppeAsDictionary();
            if (naeringsgruppeDictionary.ContainsKey(naeringsgruppeString))
            {
                var naeringsgruppeBeskrivelse = naeringsgruppeDictionary[naeringsgruppeString];
                var naeringsgruppe = new KodeType()
                {
                    kodeverdi = naeringsgruppeString,
                    kodebeskrivelse = naeringsgruppeBeskrivelse
                };
                return naeringsgruppe;
            }
            else
            {
                // TODO: Use logging facilities to issue a warning of missing bygningstypeKode
                return null;
            }
        }
        public static Dictionary<string, string> NaeringsgruppeAsDictionary()
        {
            var bygningstyper = new Dictionary<string, string>();
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            string path = Path.Combine(basePath, "mvd\\naeringsgruppe.txt");
            string[] bygningstyperRawLines = File.ReadAllLines(path);
            foreach (var line in bygningstyperRawLines)
            {
                if (line.StartsWith("#"))
                    continue;
                var indexOfFirstWhitSpace = line.IndexOf(" ");
                var bygningstypeKode = line.Substring(0, indexOfFirstWhitSpace);
                var bygningstypeBeskrivelse = line.Substring(indexOfFirstWhitSpace);
                bygningstyper.Add(bygningstypeKode, bygningstypeBeskrivelse);
            }
            return bygningstyper;
        }
        public static KodeType[] BygningTypeKodeType(string bygningTypeString)
        {
            Dictionary<string, string> BygningTypeDictionary = BygningTypeAsDictionary();
            if (BygningTypeDictionary.ContainsKey(bygningTypeString))
            {
                var bygningstypeBeskrivelse = BygningTypeDictionary[bygningTypeString];
                var bygningType = new KodeType[]
                {
                    new KodeType()
                    {
                        kodeverdi = bygningTypeString,
                        kodebeskrivelse = bygningstypeBeskrivelse
                    }
                };
                return bygningType;
            }
            else
            {
                // TODO: Use logging facilities to issue a warning of missing bygningstypeKode 
                return null;
            }
        }
        public static Dictionary<string, string> BygningTypeAsDictionary()
        {
            var bygningstyper = new Dictionary<string, string>();
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            string path = Path.Combine(basePath, "mvd\\Bygningstyper(NS3457).txt");
            string[] bygningstyperRawLines = File.ReadAllLines(path);
            foreach (var line in bygningstyperRawLines)
            {
                if (line.StartsWith("#"))
                    continue;
                var indexOfFirstWhitSpace = line.IndexOf(" ");
                var bygningstypeKode = line.Substring(0, indexOfFirstWhitSpace);
                var bygningstypeBeskrivelse = line.Substring(indexOfFirstWhitSpace);
                bygningstyper.Add(bygningstypeKode, bygningstypeBeskrivelse);
            }
            return bygningstyper;
        }
    }



}