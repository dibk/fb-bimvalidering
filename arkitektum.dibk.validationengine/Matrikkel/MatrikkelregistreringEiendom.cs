﻿//using no.kxml.skjema.dibk.matrikkelregistrering;
using System.Text;
using System.Text.RegularExpressions;
using Xbim.Ifc4.Interfaces;

namespace arkitektum.dibk.validationengine.Matrikkel
{
    public class MatrikkelregistreringEiendom
    {
        public static MatrikkelnummerType[] GetMatrikkelnummeretRegex(string landTitleNumber)
        {
            if (string.IsNullOrEmpty(landTitleNumber)) return null;
            var pattern = new Regex(@"(?<knr>\d+)-(?<gnr>\d+)\/(?<bnr>\d+)\/(?<fnr>\d+)\/(?<snr>\d+)");
            var match = pattern.Match(landTitleNumber);
            if (match.Success)
            {
                var matrikkelnummer = new[]
                {
                    new MatrikkelnummerType()
                    {
                        kommunenummer = match.Groups["knr"].Value,
                        gaardsnummer = match.Groups["gnr"].Value,
                        bruksnummer = match.Groups["bnr"].Value,
                        festenummer = match.Groups["fnr"].Value,
                        seksjonsnummer = match.Groups["snr"].Value,
                    }
                };
                return matrikkelnummer;
                }
            return null;
        }

        public static AdresseType[] GetSiteAddress(IIfcPostalAddress ifcPostalAddress)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var siteAddressLine in ifcPostalAddress.AddressLines)
            {
                if (i > 0)
                    sb.Append(" ");
                sb.Append(siteAddressLine.Value);
                i++;
            }
            var streetFullName = sb.ToString();
            if (string.IsNullOrEmpty(streetFullName)) return null;
            //REGEX Adresse til eiendom/byggested
            Regex pattern = new Regex(@"(?<gatenavn>(^[^0-9]*))?(?<husnumer>([0-9]*))?(?<bokstav>(\p{L}))?(\s)?(?<resten>((.*)))?");
            Match match = pattern.Match(streetFullName);
            var siteAddress = new[]
            {
                new AdresseType()
                {
                    adressekode = "-",
                    adressenavn = match.Groups["gatenavn"].Value.Trim(),
                    adressenummer = match.Groups["husnumer"].Value,
                    adressebokstav = match.Groups["bokstav"].Value,
                    seksjonsnummer = "-",
                }
            };
            return siteAddress;
        }
    }
}
