﻿//using no.kxml.skjema.dibk.matrikkelregistrering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using arkitektum.dibk.validationengine.IfcHelpers;
using Xbim.Ifc;
using Xbim.Ifc2x3.ProductExtension;

namespace arkitektum.dibk.validationengine.Matrikkel
{
    public class MatrikkelregistreringEtasjer
    {
        public static string[] GetEtajeplanAndNumer(string ifcLongName)
        {
            var regex = MatrikkelopplysningerService.GetEtasjePlanNummerLoepenummerREGEX(ifcLongName);
            return regex.Any() ? regex : new[] {"",""};
        }

        // **Eiendom/Byggested**
        public static BygningsOpplysningType GetBygningsOpplysningType(IfcRelAssociatesClassificationFilter ifcBuildingSoreyAssociatesClassificationFilter)
        {
            // etasjeopplysning (Etasjer.Kode)
            string etasjeopplysninString = ifcBuildingSoreyAssociatesClassificationFilter.GetByClassTypeToString("BygningsOpplysningType");

            switch (etasjeopplysninString)
            {
                case "Eksisterende":
                    return BygningsOpplysningType.Eksisterende;
                case "Ny":
                    return BygningsOpplysningType.Ny;
                case "Endret":
                    return BygningsOpplysningType.Endret;
                case "Fjernet":
                    return BygningsOpplysningType.Fjernet;
                default:
                    return BygningsOpplysningType.Ny;
            }
        }
    }
}
