﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using arkitektum.dibk.validationengine.Logger;
using Serilog;
using Xbim.Ifc;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;

namespace arkitektum.dibk.validationengine.MvdXml
{
    public class MvdxmlServices
    {
        private readonly ILogger _logger;

        public MvdxmlServices(ILogger logger)
        {
            _logger = logger;
            _logger = _logger.ForContext<MvdxmlServices>();
        }

        /// <summary>
        /// Validate ifc Model against mvdxml file 
        /// </summary>
        /// <param name="ifcModel"></param>
        /// <param name="mvdValidationFile"></param>
        /// <returns>MdvXml validation result</returns>
        public MvdEngine MvdValidation(IfcStore ifcModel, string mvdValidationFile)
        {
            using (var logger = new ValidationPerfTimerLogger(_logger))
            {
                try
                {
                    var mvd = mvdXML.LoadFromFile(mvdValidationFile);
                    var mvdValidation = new MvdEngine(mvd, ifcModel, true);// ForceModelSchema = true Kjører både mot ifc4 og ifc2x3 filer
                    return mvdValidation;
                }
                catch (Exception mvdXmlException)
                {
                    _logger.Error(mvdXmlException, "Xbim can't validat ifcModel with Mvdxml");
                    throw new Exception("Kan ikke validere modell mot mvdxml-fil", mvdXmlException);
                }
            }
        }

        /// <summary>
        /// A concept may have more than one definitions, we chose the one in Norwegian or the first
        /// </summary>
        /// <param name="definitions"></param>
        /// <returns></returns>
        public static DefinitionsDefinition[] GetDefinition(IEnumerable<DefinitionsDefinition> definitions)
        {
            string bodyValue = null;
            string bodyLang = null;
            if (definitions == null) return null;
            var definitionDefinitions = definitions.Where(d => !string.IsNullOrEmpty(d.Body.Value));
            var definitionsDefinitions = definitionDefinitions as DefinitionsDefinition[] ?? definitionDefinitions.ToArray();
            if (definitionsDefinitions.Any())
            {

                foreach (var definition in definitionsDefinitions.Where(d => !string.IsNullOrEmpty(d.Body.Value)))
                {
                    if (definition.Body.lang.ToLower() == "no")
                    {
                        bodyValue = definition.Body.Value;
                        bodyLang = definition.Body.lang;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(bodyValue))
                {
                    bodyValue = definitionsDefinitions[0].Body.Value;
                    var allDefinitions = definitionsDefinitions.Where(d => !string.IsNullOrEmpty(d.Body.Value));
                    var enumerable = allDefinitions as DefinitionsDefinition[] ?? allDefinitions.ToArray();
                    if (enumerable.Any())
                    {
                        var firstOrDefault = enumerable.FirstOrDefault();
                        if (firstOrDefault != null) bodyValue = firstOrDefault.Body.Value;
                    }
                }
            }
            //BIMQ export definition with extra data from Pset_ and Qto_, take away this extra definition 
            if (bodyValue != null)
            {
                var definitonIdex = bodyValue.IndexOf("[Definition from IFC]:", StringComparison.Ordinal);
                if (definitonIdex > 0)
                    bodyValue = bodyValue.Substring(0, definitonIdex - 2);
            }
            var newDefinitionsDefinitions = new[]
               {
                new DefinitionsDefinition()
                {
                    Body = new DefinitionsDefinitionBody()
                    {
                        lang = bodyLang ?? "",
                        Value = bodyValue ?? ""
                    }
                }
            };
            return newDefinitionsDefinitions;
        }


        /// <summary>
        /// BIMQ export name + entity + rule so we have to extract the name from the string
        /// </summary>
        /// <param name="concept"></param>
        /// <returns></returns>
        public static string GetConceptName(Concept concept)
        {
            if (concept.code != null) return concept.name;
            // extract just the name of the rul from BIMQ export
            var conceptName = concept.name;
            var conceptNameIndex = conceptName.IndexOf(":", StringComparison.Ordinal);
            if (conceptNameIndex > 0)
            {
                conceptName = conceptName.Substring(0, conceptNameIndex - 1);
                conceptName = conceptName.Contains(" ") ? conceptName.Substring(conceptName.IndexOf(" ", StringComparison.Ordinal)) : conceptName;
            }
            return conceptName.Trim();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="concept"></param>
        /// <returns></returns>
        public static string GetConceptCode(Concept concept)
        {
            if (!string.IsNullOrEmpty(concept.code)) return concept.code;
            // Extract Condept code from the string Name from BIMQ export format
            var indexOfEmptySpace = concept.name.IndexOf(" ", StringComparison.Ordinal);
            string code = indexOfEmptySpace > 0 ? concept.name.Substring(0, indexOfEmptySpace) : null;
            return code;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="concept"></param>
        /// <returns></returns>
        public static string GetConcepRootCode(ConceptRoot concept)
        {
            if (!string.IsNullOrEmpty(concept.code)) return concept.code;
            // Extract Condept code from the string Name from BIMQ export format
            var indexOfEmptySpace = concept.name.IndexOf(" ", StringComparison.Ordinal);
            string code = indexOfEmptySpace > 0 ? concept.name.Substring(0, indexOfEmptySpace) : null;
            return code;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conceptRoot"></param>
        /// <returns></returns>
        public static string GetConcepRootName(ConceptRoot conceptRoot)
        {
            if (!string.IsNullOrEmpty(conceptRoot.code)) return conceptRoot.name;
            var conceptRootName = conceptRoot.name;
            var indexOfEmptySpace = conceptRootName.IndexOf(" ", StringComparison.Ordinal);
            conceptRootName = indexOfEmptySpace > 0 ? conceptRootName.Substring(indexOfEmptySpace, conceptRootName.Length - indexOfEmptySpace) : conceptRootName;
            return conceptRootName;
        }

        /// <summary>
        /// Check if the  parameter to check is direct reference to the entity
        /// </summary>
        /// <param name="concept"></param>
        /// <returns></returns>
        public static string GetConceptParameter(Concept concept)
        {

            string conceptParameter = null;

            var templateRuesItems = concept.TemplateRules?.Items;
            var objectParameters = templateRuesItems?.FirstOrDefault(i => i.GetType().GetProperty("Parameters") != null);

            if (objectParameters == null)
            {
                //Parameter is inside item for example in Pset_
                var objectItems = templateRuesItems?.FirstOrDefault(i => i.GetType().GetProperty("Items") != null);
                if (objectItems != null)
                {
                    if (objectItems is TemplateRules templateRules)
                        objectParameters = templateRules.Items?.FirstOrDefault(i => i.GetType().GetProperty("Parameters") != null);
                }
            }

            if (objectParameters != null)
            {
                if (objectParameters is TemplateRulesTemplateRule templateRulesTemplateRule)
                {
                    var parameter = templateRulesTemplateRule.Parameters;
                    conceptParameter = GetConceptNameFromTemplateRule(parameter);
                }
            }

            return conceptParameter;
        }


        public static string GetConceptNameFromTemplateRule(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            string conceptParameter = null;
            if (input.IndexOf("property", StringComparison.CurrentCultureIgnoreCase) >= 0 && input.Contains("'"))
            {
                //This extracts the name of the property by searching for a "property" object in the string and extracts the value
                // f.eks T_psetName[Value]='Pset_WallCommon' AND T_Property[Value]='IsExternal'

                // string pattern = @"([""'])(?:(?=(\\?))\2.)*?\1"; regular expressions to get value in quotes
                var propertyIndex = input.IndexOf("=", input.IndexOf("Property", StringComparison.CurrentCultureIgnoreCase) + 1, StringComparison.Ordinal);
                var index = input.IndexOf("'", propertyIndex, StringComparison.Ordinal) + 1;
                var index2 = input.IndexOf("'", index, StringComparison.Ordinal);
                conceptParameter = input.Substring(index, index2 - index);
            }
            else
            {
                // if the template rule have just on property to check, this will extract the name of that property
                // F.eks LandTitleNumber[Exists]=TRUE
                conceptParameter = input.Contains("[") ? input.Substring(0, input.IndexOf("[", StringComparison.Ordinal)) : input;
            }


            return conceptParameter;
        }

        public static string[] GetAllConceptParameter(Concept concept)
        {
            var strings = new string[2];

            // The Parameter can be in Templatereules.templateRule 
            try
            {
                strings[0] = ((TemplateRulesTemplateRule)concept.TemplateRules.Items[0]).Parameters;
                strings[0] = strings[0].Contains("[") ? strings[0].Substring(0, strings[0].IndexOf("[", StringComparison.Ordinal)) : strings[0];
            }
            // If not the parameter is define in a subchema in mvdxml. Control if The entity have definition or type for example Pset_ Or Qto_ 
            catch
            {
                strings[0] = ((TemplateRulesTemplateRule)((TemplateRules)concept.TemplateRules.Items[0]).Items[0]).Parameters;
                var noko = Regex.Split(strings[0], $"\'");
                strings[0] = noko[1];
                strings[1] = noko[3];

            }
            return strings;
        }

        public static string GetApplicableRootEntity(RequirementsRequirement requirementsRequirement)
        {

            try
            {
                return requirementsRequirement.ParentConcept.ParentConceptRoot.applicableRootEntity;
            }
            catch
            {
                return null;
            }

        }
    }
}
