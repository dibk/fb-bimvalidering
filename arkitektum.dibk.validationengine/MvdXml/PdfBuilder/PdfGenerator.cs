﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xbim.MvdXml;
using System.IO;
using arkitektum.dibk.mvdxml.PdfBuilder;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace arkitektum.dibk.validationengine.MvdXml.PdfBuilder
{
    public class PdfGenerator
    {
        private readonly mvdXML _mvdXml;
        private readonly ModelView _views;
        private readonly string _dibkLogo;
        private readonly string _buildingSmartLogo;
        private Document _pdfDocument;
        private BaseFont _baseFont;
        private Font _fontBlackHeader;
        private Font _fontGrayHeader;
        private Font _fontBlueHeader;
        private Font _font2BBold;

        private Font _fontParragraf;
        private Font _fontParragrafBold;
        private Font _fontLink;
        //---
        private Font _fontHeader1;
        private Font _fontHeaderIfcMapping;
        private Font _fontSubHeader;

        private Font _fontHeader2;
        private Font _fontSubHeader2;
        private Font _fontHeader2IfcMapping;
        private Font _fontRequirementIfcName;


        private Font _fontRequirement;
        private MemoryStream _output;
        private PdfWriter _writer;
        private PdfContentByte _contentByte;
        private PdfPTable _table;
        private ColumnText ct;
        private readonly iTextSharp.text.Color _tableLineColor;

        public PdfGenerator(string mvdxmlPath, string dibkLogo, string buildingSmartLogo)
        {
            _mvdXml = mvdXML.LoadFromFile(mvdxmlPath);
            _dibkLogo = dibkLogo;
            _buildingSmartLogo = buildingSmartLogo;
            _views = _mvdXml.Views[0];
            _tableLineColor = new Color(158, 158, 158);
        }

        public Stream CreatePdf()
        {

            Startup();
            IntroductionPage();
            //AddConceptRoot();
            AddConceptPdfPage();


            //AddDescription();
            //AddPurpose();
            //AddContactOwner();
            //AddResolution();
            //AddCoverage();
            //AddProcessHistory();
            //AddMaintenance();
            //AddDistribution();
            //AddFeatureTypes();
            //AddAttributes();


            //PdfStructureTreeRoot root = writer.StructureTreeRoot;
            //PdfStructureElement div = new PdfStructureElement(root, new PdfName("Div"));
            //contentByte.BeginMarkedContentSequence(div);
            //contentByte.EndMarkedContentSequence();
            _pdfDocument.Close();
            _output.Flush();
            _output.Position = 0;
            return _output;
        }

        private void Startup()
        {
            // Document format
            _pdfDocument = new Document(PageSize.A4, 50, 50, 25, 50);
            _baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);

            // IntroductionPage fonts
            _fontBlueHeader = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 16f, Font.BOLD, new Color(0, 69, 134));
            _fontGrayHeader = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 14f, Font.BOLD, Color.GRAY);
            _fontBlackHeader = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 14f, Font.BOLD, Color.BLACK);
            _font2BBold = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.BOLD, new Color(0, 69, 134));

            //Concept Page fonts
            _fontParragraf = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.NORMAL, Color.BLACK);
            _fontRequirement = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.BOLDITALIC, new Color(0, 0, 255));
            _fontLink = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 6f, Font.NORMAL, new Color(0, 0, 255));

            _fontHeader1 = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 24f, Font.BOLD, new Color(34, 85, 153));
            _fontHeader2 = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 15f, Font.BOLD, new Color(34, 85, 153));

            _fontHeaderIfcMapping = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.BOLD, new Color(34, 85, 153));
            _fontRequirementIfcName = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 6f, Font.BOLD, new Color(34, 85, 153));

            //----

            _output = new MemoryStream();

            _writer = PdfWriter.GetInstance(_pdfDocument, _output);
            _writer.CloseStream = false;
            _writer.PdfVersion = PdfWriter.VERSION_1_7;

            //Tagged
            _writer.SetTagged();
            _writer.UserProperties = true;

            // must be initialized before document is opened.
            _writer.PageEvent = new PdfHeaderFooter(_mvdXml, _dibkLogo, _buildingSmartLogo);

            _pdfDocument.Open();
            _pdfDocument.AddTitle(_mvdXml.Views[0].name);
            _pdfDocument.AddAuthor("Direktoratet for byggkvalitet");
            _contentByte = _writer.DirectContent;

            _contentByte.BeginText();
            _contentByte.SetFontAndSize(_baseFont, 22);
            _contentByte.EndText();

        }

        private void IntroductionPage()
        {
            // new paragraph
            var headingParagraph = new Paragraph()
            {
                new Chunk("Prosjekt", _fontGrayHeader),
                Chunk.NEWLINE,
                new Chunk(_views.name,_fontBlackHeader)
            };
            headingParagraph.SpacingBefore = 50;
            _pdfDocument.Add(headingParagraph);

            // new paragraph
            headingParagraph = new Paragraph()
            {
                new Chunk("Omfatter faser", _fontGrayHeader),
                Chunk.NEWLINE,
                new Chunk("Forprosjekt",_fontBlackHeader)
            };
            headingParagraph.SpacingBefore = 50;
            _pdfDocument.Add(headingParagraph);


            // new paragraph
            var definition = MvdxmlServices.GetDefinition(_views.Definitions);
            headingParagraph = new Paragraph()
            {
                new Chunk("Prosjektbeskrivelse:", _fontGrayHeader),
                Chunk.NEWLINE,
            };
            if (definition != null)
            {
                headingParagraph.Add(new Chunk(definition[0].Body.Value, _fontBlackHeader));
            }
            headingParagraph.SpacingBefore = 50;
            _pdfDocument.Add(headingParagraph);

            // new paragraph
            definition = MvdxmlServices.GetDefinition(_views.ExchangeRequirements[0].Definitions);
            headingParagraph = new Paragraph()
            {
                new Chunk("BIM bruker-scenarier og prosesser", _fontBlueHeader),
                Chunk.NEWLINE,
                new Chunk(_views.ExchangeRequirements[0].name+"  ", _font2BBold),
            };
            if (definition != null)
            {
                headingParagraph.Add(new Chunk(definition[0].Body.Value, _fontParragraf));
            }
            headingParagraph.SpacingBefore = 60;
            _pdfDocument.Add(headingParagraph);
        }

        private void AddConceptPdfPage()
        {
            var viewRoots = _views.Roots.First().code != null ?
                _views.Roots.GroupBy(c => c.code?.Substring(0, 1)).OrderByDescending(c => c.Key) :
                _views.Roots.GroupBy(c => c.name?.Substring(0, 1)).OrderByDescending(c => c.Key);

            foreach (var conceptRoot in viewRoots)
            {


                // check if the code exists in the concept for Sort in ascending order in the PDF file, if there is no code, get the code from concept.name
                IEnumerable<ConceptRoot> concepts;
                
                if (conceptRoot.All(c => !string.IsNullOrEmpty(c.code)))
                {
                    concepts = conceptRoot.OrderBy(c => c.code.Substring(0, 3));
                }
                else
                {
                    concepts = conceptRoot.OrderBy(c => c.name.Substring(0, 3));
                }

                foreach (ConceptRoot root in concepts)
                {
                    // Add page for each entity
                    _pdfDocument.NewPage();

                    var conceptRootName = MvdxmlServices.GetConcepRootName(root);
                    var conceptRootCode = MvdxmlServices.GetConcepRootCode(root);


                    // * Set Header1 in PDF (Entity Code and Name) f. eks ("502 Site")

                    _pdfDocument.Add(new Paragraph(string.Concat(conceptRootCode, " ", conceptRootName), _fontHeader1)
                    {
                        SpacingBefore = 2
                    });

                    // *Set IfcEntity name f. eks ("IfcSite")
                    _pdfDocument.Add(new Paragraph(root.applicableRootEntity, _fontHeaderIfcMapping)
                    {
                        SpacingBefore = 2,
                        IndentationLeft = 10,
                    });

                    // *web link to the Ifcentity f. eks("https://standards.buildingsmart.org/IFC/RELEASE/IFC4/FINAL/HTML/link/ifcsite.htm")
                    var webLink = string.IsNullOrEmpty(conceptRootCode) ? null : new Phrase(CreateAnchor(conceptRootCode));
                    if (webLink != null)
                    {
                        _pdfDocument.Add(new Paragraph(webLink)
                        {
                            IndentationLeft = 10,
                        });
                    }

                    // *Entity definition from mvdxml
                    var definition = MvdxmlServices.GetDefinition(root.Definitions);

                    if (definition != null)
                    {
                        // *Title to the definition
                        _pdfDocument.Add(new Paragraph("Beskrivelse", _fontGrayHeader)
                        {
                            IndentationLeft = 10,
                            SpacingBefore = 5
                        });
                        _pdfDocument.Add(new Paragraph(definition[0].Body.Value, _fontParragraf)
                        {
                            IndentationLeft = 10,
                        });
                    }


                    //Add introduction to the requierments
                    //_pdfDocument.Add(new Paragraph("Modellkobling av egenskaper", _fontParragrafBold)
                    //{
                    //    SpacingAfter = 5,
                    //    IndentationLeft = 10,
                    //});

                    foreach (Concept concept in root.Concepts)
                    {

                        var marginLeftHeader2 = 12;
                        var marginLeftParagraph = 22;

                        // check the concep requirement to set the right bakground color
                        var requirement = concept.Requirements[0].requirement.ToString();
                        var requirementChunk = new Chunk(string.Concat("(", SetRequiremnt(requirement), ")"), _fontRequirement);
                        if (requirement.ToLower() == "mandatory")
                        {

                            requirementChunk.Font = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.BOLDITALIC, new Color(215, 65, 40)); ;
                        }
                        else
                        {
                            requirementChunk.Font = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 8f, Font.BOLDITALIC, new Color(250, 152, 0)); ;
                        }

                        // *Requirement Name and the requirement of the rule f. eks ("Georeferering, breddegrad recommended")
                        var conceptRequierment = new Paragraph()
                        {
                            new Chunk("\u2022"+" ",_fontHeader2),
                            new Chunk(MvdxmlServices.GetConceptName(concept),_fontHeader2),
                          new Chunk("   "),
                          requirementChunk
                        };
                        conceptRequierment.IndentationLeft = marginLeftHeader2;
                        conceptRequierment.SpacingBefore = 5;
                        _pdfDocument.Add(conceptRequierment);

                        string conceptCode = MvdxmlServices.GetConceptCode(concept);

                        if (!string.IsNullOrEmpty(conceptCode))
                        {
                            // *Ifc requierment mapping Heading2 f. eks ("IfcSite.RefLatitude")
                            var conceMainCode = GetMainCode(conceptCode);
                            string ifcEntity = GetIfcEntity(conceMainCode);

                            var conceptName = MvdxmlServices.GetConceptParameter(concept);
                            _pdfDocument.Add(new Paragraph(string.Concat(ifcEntity, ".", conceptName), _fontHeaderIfcMapping)
                            {
                                IndentationLeft = marginLeftParagraph,
                            });

                            // *Web Linkto ifc requierment f. eks ("https://standards.buildingsmart.org/IFC/RELEASE/IFC4/FINAL/HTML/link/ifcsite.htm")
                            var anchor = CreateAnchor(conceMainCode);
                            if (anchor != null)
                            {
                                var requiermentWeblink = string.IsNullOrEmpty(conceptCode) ? null : new Phrase(anchor);
                                _pdfDocument.Add(new Paragraph(requiermentWeblink)
                                {
                                    IndentationLeft = marginLeftParagraph
                                });
                            }
                        }
                        if (concept.Definitions == null) continue;
                        // *Title to the definition
                        _pdfDocument.Add(new Paragraph("Beskrivelse", _fontGrayHeader)
                        {
                            IndentationLeft = marginLeftParagraph
                        });
                        // *Definition from teh mvdxml of the requierment

                        definition = MvdxmlServices.GetDefinition(concept.Definitions);
                        _pdfDocument.Add(new Paragraph(definition[0].Body.Value, _fontParragraf)
                        {
                            IndentationLeft = marginLeftParagraph,
                        });
                    }
                }
            }
        }

        private static string SetRequiremnt(string requiremnt)
        {
            switch (requiremnt.ToLower())
            {
                case "mandatory":
                    return "Obligatorisk";
                case "recommended":
                    return "Advarsel";
                case "notrelevant":
                    return "Ikke relevant";
                case "notrecommended":
                    return "Ikke anbefalt";
                case "excluded":
                    return "Ekskludert";
                default:
                    return null;
            }
        }
        // create anchro for the weblink 
        private Anchor CreateAnchor(string conceptCode)
        {
            var entityLinkDictionary = GetWhiteList();
            string entityWebLink;
            if (string.IsNullOrEmpty(conceptCode)) return null;
            if (!entityLinkDictionary.TryGetValue(conceptCode, out entityWebLink))
            {
                entityWebLink = null;
            }
            var anchor = new Anchor(entityWebLink, _fontLink)
            {
                Reference = entityWebLink
            };
            return anchor;
        }
        // Get the right name of the entity that is check from the concept code
        private string GetIfcEntity(string conceptCode)
        {
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var ifcConceptsWhiteList = Path.Combine(basePath, "mvd\\Ifc4konseptList.csv");

            using (var reader = new StreamReader(ifcConceptsWhiteList))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line?.Split(';');
                    if (string.IsNullOrWhiteSpace(values?[1])) continue;
                    if (values[1] == conceptCode)
                    {
                        return values[0];
                    }
                }
            }
            return null;
        }

        // Get the main code to find the ifc entity & weblink in the dictionary from the whitlist
        private static string GetMainCode(string conceptCode)
        {

            // control the index of the last "." to get the web link for the object
            var index = conceptCode.Count(c => c == '.') == 2
                ? conceptCode.IndexOf(".", conceptCode.IndexOf(".", StringComparison.Ordinal) + 1, StringComparison.Ordinal) + 2
                : conceptCode.IndexOf(".", StringComparison.Ordinal);
            string code = index > 1 ? conceptCode.Substring(0, index) : null;
            return code;
        }

        // Create dictionary with concepts requirements code and weblink from Ifc4konseptList.csv
        private static Dictionary<string, string> GetWhiteList()
        {
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var ifcConceptsWhiteList = Path.Combine(basePath, "mvd\\Ifc4konseptList.csv");
            var entityLinkDictionary = new Dictionary<string, string>();
            // Add to dictionary all concepts requirements from file
            using (var reader = new StreamReader(ifcConceptsWhiteList))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line?.Split(';');
                    if (string.IsNullOrWhiteSpace(values?[1])) continue;
                    entityLinkDictionary.Add(values[1], values[2]);
                }
            }
            return entityLinkDictionary;
        }

    }
}