﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Xbim.MvdXml;
using System.IO;

namespace arkitektum.dibk.mvdxml.PdfBuilder
{
    public class PdfHeaderFooter : PdfPageEventHelper
    {
        private DateTime PrintTime = DateTime.Now;
        private BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
        private PdfContentByte cb;
        Font _baseFontBig;
        Font _baseFontNormal;
        Font _baseFontFooter;
        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate, template;

        private string _dibkLogo;
        //private ProductSheet _productsheet;
        private mvdXML _mvdXml;
        private string _buildingSmartLogo;

        public PdfHeaderFooter(mvdXML mvdXml, string dibkLogo, string buildingSmartLogo)
        {
            this._mvdXml = mvdXml;
            this._dibkLogo = dibkLogo;
            this._buildingSmartLogo = buildingSmartLogo;
            this._baseFontBig = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 16f, Font.BOLD, new Color(0, 69, 134));
            this._baseFontNormal = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 14f, Font.BOLD, new Color(0, 102, 204));
            this._baseFontFooter = FontFactory.GetFont("Arial", BaseFont.CP1252, BaseFont.EMBEDDED, 7f, Font.BOLD, new Color(178,178,178));

        }

        #region Properties

        public string Title { get; set; }

        public string HeaderLeft { get; set; }

        public string HeaderRight { get; set; }

        public Font HeaderFont { get; set; }

        public Font FooterFont { get; set; }

        #endregion

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
                cb = writer.DirectContent;
                template = cb.CreateTemplate(50, 50);
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);

            }
            catch (DocumentException de)
            {
                //handle exception here
            }
            catch (System.IO.IOException ioe)
            {
                //handle exception here
            }
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);



          
            var Heading1 = new Paragraph("ByggesaksBIM", _baseFontBig);
            var Heading2 = new Paragraph("Detaljer rapport", _baseFontNormal);

            //header
            cb.MoveTo(35, document.PageSize.Height - 10);
            document.Add(Heading1);
            document.Add(Heading2);

            Rectangle pageSize = document.PageSize;
            if (_buildingSmartLogo != "")
            {
                Image imageOrganization;
                imageOrganization = Image.GetInstance(_buildingSmartLogo);
                imageOrganization.Alt = "BuildingSmart logo";
                imageOrganization.ScalePercent(10);

                imageOrganization.SetAbsolutePosition(pageSize.GetRight(50) - 180, pageSize.GetTop(90));
                document.Add(imageOrganization);
            }

            var image_logo = Image.GetInstance(_dibkLogo);
            image_logo.Alt = "Dibk Logo";
            image_logo.ScalePercent(50);
            image_logo.SetAbsolutePosition(pageSize.GetRight(50) - 35, pageSize.GetTop(70));
            document.Add(image_logo);

            cb.MoveTo(35, document.PageSize.Height - 75);
            cb.LineTo(document.PageSize.Width - 35, document.PageSize.Height - 75);
            cb.SetLineWidth(0.5f);
            cb.Stroke();

            cb.BeginText();
            cb.SetFontAndSize(bf, 8);
            if (_mvdXml.name != null)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, _mvdXml.name, pageSize.GetRight(35), pageSize.GetTop(60), 0);
            cb.EndText();
       
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            //Create PdfTable object
            PdfPTable pdfTab = new PdfPTable(1);
            var Heading1 = new Paragraph("Detaljert rapport for  P13 – eByggesak in Forprosjekt for ARK", _baseFontFooter);
            var Heading2 = new Paragraph("©buildingSMART Norge – Direktoratet for byggkvalitet", _baseFontFooter);
            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            PdfPCell pdfCell1 = new PdfPCell(Heading1);
            PdfPCell pdfCell2 = new PdfPCell(Heading2);
            String text = "Page " + writer.PageNumber + " of ";


            //Add paging to footer
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            }

            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfCell2.HorizontalAlignment = Element.ALIGN_LEFT;

            pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;

            pdfCell1.Border = 0;
            pdfCell2.Border = 0;

            //add all three cells into PdfTable
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);

            pdfTab.TotalWidth = 400;
            pdfTab.LockedWidth = true;
            pdfTab.WidthPercentage = 80;



            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.GetBottom(50), writer.DirectContent);
            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, document.PageSize.GetBottom(50));
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 12);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();
        }
    }
}