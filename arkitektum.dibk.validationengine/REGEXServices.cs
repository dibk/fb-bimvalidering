using System;
using System.Collections.Generic;
using System.Linq;
using Xbim.Common;
using Xbim.MvdXml.DataManagement;
using Xbim.Ifc4.MeasureResource;
using Xbim.MvdXml;
using System.Text.RegularExpressions;
using arkitektum.dibk.mvdxml;
using arkitektum.dibk.validationengine.MvdXml;
using Serilog;
using Xbim.Ifc4.RepresentationResource;

namespace arkitektum.dibk.validationengine
{
    public class RegexServices
    {
        private readonly ILogger _logger = Log.ForContext<RegexServices>();
        /// <summary>
        /// See if the value of the entity need to be control with REGEX
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="requirementsRequirement"></param>
        /// <returns></returns>
        public Dictionary<string, ConceptTestResult> EntityValueRegexCheck(IPersistEntity entity, RequirementsRequirement requirementsRequirement)
        {
            var regexControlTestResults = new Dictionary<string, ConceptTestResult>();
            string rgxParameter = null;
            var applicableEntity = MvdxmlServices.GetApplicableRootEntity(requirementsRequirement);
            var parameter = MvdxmlServices.GetConceptParameter(requirementsRequirement.ParentConcept);

            switch (parameter.ToLower())
            {
                case "crsname":
                    rgxParameter = "CRSName";
                    break;
                case "landtitlenumber":
                    rgxParameter = "LandTitleNumber";
                    break;
                case "longname":
                    if (applicableEntity == "IfcBuildingStorey")
                        rgxParameter = "IfcBuildingStorey.LongName";
                    if (applicableEntity == "IfcZone")
                        rgxParameter = "IfcZone.LongName";
                    break;
                case "occupancytype":
                    rgxParameter = "IfcBuildin.OccupancyType";
                    break;
                default:
                    break;
            }

            if (rgxParameter == null)
            {
                return null;
                //rgxParameter = "notApplicable";
                //regexControlTestResults.Add(rgxParameter, ConceptTestResult.DoesNotApply);
            }
            else
            {
                regexControlTestResults.Add(rgxParameter, SetRegexAndValue(entity, rgxParameter));
            }
            return regexControlTestResults;
        }

        /// <summary>
        /// Set REGEX and Value to be checked for each entity that needs to be checked
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="parameter"></param>
        /// <param name="applicableEntity"></param>
        /// <returns></returns>
        private ConceptTestResult SetRegexAndValue(IPersistEntity entity, string parameter, string applicableEntity = "")
        {
            string rgxString = GetRegex(parameter, applicableEntity);
            var entityValue = string.Empty;
            try
            {
                switch (parameter)
                {
                    case "LandTitleNumber":
                        if (entity.Model.Header.SchemaVersion == "IFC2X3")
                        {
                            entityValue = ((Xbim.Ifc2x3.ProductExtension.IfcSite)entity).LandTitleNumber;
                        }
                        else entityValue = ((Xbim.Ifc4.ProductExtension.IfcSite)entity).LandTitleNumber;
                        break;
                    case "Name":
                        if (entity.Model.Header.SchemaVersion == "IFC2X3")
                        {
                            entityValue = ((Xbim.Ifc2x3.Kernel.IfcRoot)entity).Name;
                        }
                        else entityValue = ((Xbim.Ifc4.Kernel.IfcRoot)entity).Name;
                        if (rgxString == null)
                        {
                            return ConceptTestResult.Pass;
                        }
                        else return CheckEntityValueWithRegex(rgxString, entityValue);
                    case "CRSName":
                        if (entity.Model.Header.SchemaVersion == "IFC2X3")
                        {
                            entityValue = ((Xbim.Ifc2x3.Kernel.IfcRoot)entity).Name;
                        }
                        else
                        {
                            var ifcProject = (((Xbim.Ifc4.Kernel.IfcProject)entity));
                            var ifcCoordinateOperations = ifcProject.ModelContext.HasCoordinateOperation;
                            if (ifcCoordinateOperations != null)
                            {
                                var ifcProjectHasTargetCRS = ifcCoordinateOperations.Any(i => i.TargetCRS.Name != null);
                                if (ifcProjectHasTargetCRS)
                                {
                                    var ifcCoordinateOperation = ifcCoordinateOperations.First(i => i.TargetCRS.Name != null);
                                    entityValue = ifcCoordinateOperation.TargetCRS.Name.ToString();
                                }
                            }
                        }
                        break;
                    case "IfcBuildingStorey.LongName":

                        if (entity.Model.Header.SchemaVersion == "IFC2X3")
                        {
                            entityValue = (((Xbim.Ifc2x3.ProductExtension.IfcSpatialStructureElement)entity).LongName);
                        }
                        else entityValue = (((Xbim.Ifc4.ProductExtension.IfcSpatialElement)entity).LongName);
                        break;
                    case "IfcZone.LongName":
                        if (entity.Model.Header.SchemaVersion == "IFC2X3")
                        {
                            entityValue = ((Xbim.Ifc2x3.ProductExtension.IfcZone)entity).FriendlyName;
                        }
                        else entityValue = ((Xbim.Ifc4.ProductExtension.IfcZone)entity).LongName;
                        break;
                    case "IfcBuildin.OccupancyType":
                        //GetOccupancyType Check if IfcBuilding have Pset_BuildingCommon.OccupancyType and return the value
                        entityValue = GetOccupancyType(entity);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Debug($"Error - Can't get value from {parameter} : {ex.Message}");
            }

            if (string.IsNullOrEmpty(entityValue))
                return ConceptTestResult.DoesNotApply;

            return CheckEntityValueWithRegex(rgxString, entityValue);

        }

        /// <summary>
        /// Check Entity value with the REGEX "using System.Text.RegularExpressions"
        /// </summary>
        /// <param name="rgxString"></param>
        /// <param name="entityValue"></param>
        /// <returns>ConceptTestResult.Pass/Fail</returns>
        private ConceptTestResult CheckEntityValueWithRegex(string rgxString, IfcLabel? entityValue)
        {
            Regex rgx = new Regex(rgxString);
            if (entityValue.Value != null)
            {
                if (rgx.IsMatch(entityValue))
                {
                    return ConceptTestResult.Pass;
                }
            }
            return ConceptTestResult.Fail;
        }
        /// <summary>
        /// Get the value of the propertie ("Pset_BuildingCommon.OccupancyType") for IfcBuilding Entities
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>IfcBuilding.Pset_BuildingCommon.OccupancyType.value</returns>
        private IfcLabel GetOccupancyType(IPersistEntity entity)
        {
            string entityValue = null;
            if (entity.Model.Header.SchemaVersion == "IFC2X3")
            {
                var psetIfc2x3 = ((Xbim.Ifc2x3.Kernel.IfcObject)entity).GetPropertySingleValue("Pset_BuildingCommon", "OccupancyType");
                if (psetIfc2x3 != null)
                    entityValue = psetIfc2x3.NominalValue.ToString();

            }
            else
            {
                var pste = ((Xbim.Ifc4.Kernel.IfcObject)entity).GetPropertySingleValue("Pset_BuildingCommon", "OccupancyType");
                if (pste != null)
                    entityValue = pste.NominalValue.ToString();
            }
            return entityValue;
        }
        /// <summary>
        /// Get the right REGEX String for each Entity, value to chek
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="applicableEntity"></param>
        /// <returns></returns>
        public string GetRegex(string parameter, string applicableEntity = "")
        {
            string rgxString = "";
            switch (parameter)
            {
                case "LandTitleNumber":
                    return @"^(([0-9]{1,4})-([0-9]{1,4})\/([0-9]{1,4})\/([0-9]{1,4})\/([0-9]{1,4}))$";

                case "IfcBuildingStorey.LongName":
                    return "^([H-h,K-k,L-l,U-u,M-m,K-k,T-t,S-s](([0-9][1-9]))$)";
                case "IfcZone.LongName":
                    return "^([H-h,K-k,L-l,U-u,M-m,K-k,T-t,S-s](([0-9][1-9][0-9][1-9]))$)";
                case "Name":
                    return GetRegexForName(applicableEntity);
                case "IfcBuildin.OccupancyType":
                    return "(^[1-8][1-9]{2})$";
                case "CRSName":
                    return @"^(\w*EPSG:\w*|\w*epsg:\w*)?[0-9][0-9][0-9][0-9]$";
                default: return null;
            }
        }

        /// <summary>
        /// Get REGEX for IfcName entity
        /// </summary>
        /// <param name="applicableEntity"></param>
        /// <returns>RegexString</returns>
        public string GetRegexForName(string applicableEntity)
        {
            switch (applicableEntity)
            {
                case "IfcBuildingStorey":
                    return @"^(([1-9])|([0-9][1-9])|([0-9][0-9][1-9]))?";
                case "IfcChimney":
                    return @"(^27[1|2])([\w\W]{1,})?";
                case "IfcColumn":
                    return @"(^222)([\w\W]{1,})?";
                case "IfcCovering":
                    return @"(^2((18)|2[5,6,8]|38|4[4,6]|5[5-8]|6[2,6,8]|87))([\w\W]{1,})?";
                case "IfcDoor":
                    return @"(^2[3|4]4)([\w\W]{1,})?";
                case "IfcRailing":
                    return @"(^28[4|7])([\w\W]{1,})?";
                case "IfcRamp":
                    return @"(^283)([\w\W]{1,})?";
                case "IfcRoof":
                    return @"(^2((6[1,3,4,7])|86))([\w\W]{1,})?";
                case "IfcSpace":
                    return @"(^290)([\w\W]{1,})?";
                case "IfcStair":
                    return @"(^28[1|2|9])([\w\W]{1,})?";
                case "IfcWall":
                    return @"(^2(14|3[1,2,5,6]|4[1-5]|6[5,8]|84))([\w\W]{1,})?";
                case "IfcCurtainWall":
                    return @"(^2[3,4]3)([\w\W]{1,})?";
                case "IfcWindow":
                    return @"(^2(([3|4]4)|63))([\w\W]{1,})?";
                case "IfcSlab":
                    return @"(^2(16|5[1-4]|61|8[4,5,9]))([\w\W]{1,})?";
                case "IfcTransportElement":
                    return @"(^62[1-7])([[\w\W]{1,})?";
                default: return null;
            }
        }

    }
}
