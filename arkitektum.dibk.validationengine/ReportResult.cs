﻿using System.Runtime.CompilerServices;
using Xbim.Common;
using Xbim.Ifc4.Interfaces;
using Xbim.MvdXml;
using Xbim.MvdXml.DataManagement;

namespace arkitektum.dibk.validationengine
{
    public class ReportResult
    {
        public Concept Concept;
        public ModelViewExchangeRequirement Requirement;
        public IPersistEntity Entity;
        public ConceptTestResult TestResult;

        public ReportResult(Concept cpt, IPersistEntity selectedEntity, ConceptTestResult result, ModelViewExchangeRequirement requirement)
        {
            Concept = cpt;
            Entity = selectedEntity;
            TestResult = result;
            Requirement = requirement;
        }

        public string EntityLabel => $"#{Entity.EntityLabel}";
        public string EntityIfcGuid
        {
            get
            {
                var asRoot = Entity as IIfcRoot;
                if (asRoot == null)
                    return Entity.EntityLabel.ToString();
                return asRoot.GlobalId.ToString();
            }
        }

        private string GetExpressTypeName()
        {
            var asRoot = this.Entity as IIfcRoot;
            if (asRoot == null)
            {
                if (Entity != null)
                {
                    return Entity.ToString();
                }
            }
            var expressType = asRoot?.ExpressType;

            string name = expressType != null ? expressType.Name : asRoot?.Name?.ToString();
            return name;
        }
        public string EntityName
        {
            get
            {
                int? entityLabel = null;

                var asRoot = Entity as IIfcRoot;
                if (asRoot == null)
                    return Entity.ToString();

                var expressType = asRoot.ExpressType;
                

                //When u
                string name = expressType != null ? expressType.Name : asRoot?.Name?.ToString();

                entityLabel = asRoot.EntityLabel;

                return !string.IsNullOrEmpty(name) ? $"[#{entityLabel}] {name}"
                    : $"[#{asRoot.EntityLabel}] - ";
            }
        }

        public string InvolvedRequirement => Requirement != null
            ? Requirement.name
            : "undefined";

        public string ResultSummary
        {
            get
            {
                switch (TestResult)
                {
                    case ConceptTestResult.Pass:
                        return "Godkjent";
                    case ConceptTestResult.Fail:
                        return "Feil";
                    case ConceptTestResult.DoesNotApply:
                        return "Feil"; //Filter advarsel
                    case ConceptTestResult.Warning:
                        return "Advarsel";
                    default:
                        return TestResult.ToString();
                }
            }
        }

        public string ConceptName
        {
            get
            {
                if (Concept != null)
                {
                    if (!string.IsNullOrEmpty(Concept.name))
                    {
                        return Concept.name;
                    }
                }

                var name = GetExpressTypeName();

                if (string.IsNullOrEmpty(name))
                {
                    return "Undefined";
                }

                return name;
            }
        }
        //public string ConceptName => Concept != null
        //    ? Concept.name
        //    : "undefined"; 

    }
}
